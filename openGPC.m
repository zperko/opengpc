function [GPC,IntegrationData,ModelDetails]=openGPC(GPCSettings,ModelDetails)
%openGPC: An open source Generalized Polynomial Chaos Expansion toolbox.
%openGPC makes a Generalized Polynomial Chaos Expansion of a model using 
%Non-Intrusive Spectral Projection
% 1. INPUT:
%   - GPCSettings: Structure with the settings for constructing the GPC:
%       - variableNames: Name of the random variables to take into account
%       (1 x N, cell of strings).
%       - polType: Name of polynomials corresponding to random variable 
%       distributions, can be 'hermite', 'legendre' or 'laguarre' (1 x N,
%       cell of strings).
%       - means: The means of the distributions (1 x N, double)
%       - stds: The standard deviations/half widths of the distributions
%       (1 x N, double) 
%       - maxPolOrder: The maximum allowed polynomial order in the GPC
%       (integer).
%       - normType: The norm used to trim the GPC basis, (double, (0,1])
%       - trim: Flag for hyperbolic trimming (logical).
%       - calcMethod: calculation method ('projection' or 'regression').
%       - samplingMethod: The sampling method for choosing training points 
%       ('sparse','full','random','lhs'). Can be sparse grid, full grid, 
%       random or latin hypercube sampling.
%       - maxGridLevel: The maximum allowed integration order (integer).
%       Only used in projection.
%       - nExtraLevels: The number of extra levels for extended grids
%       (integer). Only used in projection.
%       - nTrainingPoints: The number of training points to be used
%       (integer). Only used in regression.
%       - cutoffValue: The value below which responses are neglected when
%       constructing the GPC (double).
%   - ModelDetails: Structure with the details of the model for which the
%   GPC is built. Needed entries are:
%       - modelCase: the case number of the model (integer, 0 for user
%       model).
%       - externalModel: flag for signalling non-Matlab model (logical)
%       - parallelMode: indicator for parellel model run (string, 'serial',
%       'matlabParallel', 'modelParallel')
%       - modelDetailsFile: filename for a json format file containing all
%       additional model details.
%       - statusUpdateInterval: time interval in seconds between requesting 
%       status updates if model is run externally (double, default=60)
%       - externalRetryInterval: time interval in seconds between trying to
%       run external code until successful run (double, default =15)
% 2. OUTPUT:
%   - GPC: GPCClass object for the GPC of the responses from the model.
%       - polType: Name of polynomials corresponding to random variable 
%       distributions, can be 'hermite' or 'legendre' (1 x N,
%       cell of strings).
%       - basis: The multi-indices of the PC basis vectors (P x N, integer)
%       - basisNorm: The norm of the different PC basis vectors (P x 1,
%       double).
%       - BasisVectors: MultiDimensionalOrthogonalPolynom objects for
%       the different PC basis vectors
%       - coeffs: The coefficients corresponding to the different basis
%       vectors for the different responses (P x nResponses)
%       - Details: A structure with some details on the construction of
%       the PCE and the input variables:
%           - modelID: Some ID for the model (string)
%           - comment: Entry for comments (string)
%           - polType: Name of polynomials corresponding to random variable 
%           distributions, can be 'hermite', 'legendre' or 'laguarre' (1 x N,
%           cell of strings).
%           - means: The means of the distributions (1 x N, double)
%           - stds: The standard deviations/half widths of the distributions
%           (1 x N, double) 
%           - maxPolOrder: The maximum allowed polynomial order in the GPC
%           (integer).
%           - normType: The norm used to trim the GPC basis, (double, (0,1])
%           - trim: Flag for hyperbolic trimming (logical).
%           - maxGridOrder: The maximum allowed integration order (integer).
%           - nExtraLevels: The number of extra levels for extended grids
%           (integer).
%           - cutoffValue: The value below which responses are neglected when
%           constructing the GPC (double).
%           - masked: Flag to indicate responses are masked in GPC
%           (logical)
%           - responseMask: The response mask to indicate which responses
%           are included in the GPC
%           - nSamples: Number of sample points used in the calculation
%           (integer)
%           - date: String with the starting date as YYYY-MM-DD-HH:MM:SS
%           (string)
%           - calcTimeSecond: Total time used for the calculation in
%           seconds (double)
%  - IntegrationData: A structure with the raw data used for 
%           constructing the PCE. Entries:
%           FinalCubature: A CubatureClass object representing the final
%               sparse grid used in the numerical integration
%           Cubatures: An array of CubatureClass objects representing the
%               individual sparse grids making up the FinalCubature
%           ErrorScenarios: Structure with two SampleClass objects, one for 
%               the planning scenarios containing all responses, one
%               for the scenarios in the cubature containing only the
%               selected responses (above cutoffValue)
%               Planning: The SampleClass object with the planning scenarios
%               All: The SampleClass object with all scenarios in the cubature
%                   Both contain:
%                   abscissas: The coordinates of the points
%                   responses: The value of the responses corresponding to
%                       the points
%                   nSamples: The number of sample points
%                   calcIDs: The file ID numbers corresponding to the points
%                   statusFlags: The status of the points and the corresponding data
%                       files

%% COPYRIGHT: Zoltan Perko, TU Delft
% Date: 2018.10.11.

%% FUNCTION DEFINITION

if nargin<2
    error('openGPC:NotEnoughtInputArguments',...
    'Not enough input arguments: GCP_Settings and ModelDetails needed.');
end

setPaths(ModelDetails);

% First open the modelDetailsFile if provided
if isfield(ModelDetails,'modelDetailsFile')
    ExternalModelDetails = loadModelDetails(modelDetailsFile);
    externalFields=fieldnames(ExternalModelDetails);
    for i=1:length(externalFields)
        ModelDetails.(externalFields(i)) = ExternalModelDetails.(externalFields(i));
    end
end

% Check the input structure
GPCSettings = checkGPCSettings(GPCSettings);
ModelDetails = checkModelDetails(ModelDetails,GPCSettings);

% If necessary, arrange external authentication
ModelDetails = authenticate(ModelDetails);

% Generate the sample points for the integration or regression
IntegrationData = generateSamplePoints(GPCSettings);

% Initialize the GPC
if ismember(GPCSettings.samplingMethod,{'full','sparse'})
    GPC = GPCClass(GPCSettings,IntegrationData.FinalCubature.multiIndex);
else
    GPC = GPCClass(GPCSettings,[]);
end
GPCSettings.normType = GPC.Details.normType; % Just to keep the two structures in sync

% Get the response mask for the responses to be included in the GPC and get 
% rid of the unnecessary responses
[ModelDetails,IntegrationData,GPC]=obtainMask(GPCSettings,IntegrationData,GPC,ModelDetails);

% Calculate the responses for all cubature points
indicesToCalculate = 1:IntegrationData.ErrorScenarios.All.nSamples;
indicesToCalculate = indicesToCalculate(~strcmp('fetched',...
    IntegrationData.ErrorScenarios.All.status));
IntegrationData.ErrorScenarios.All = ...
    IntegrationData.ErrorScenarios.All.computeResponses...
    (ModelDetails,indicesToCalculate);

% Then construct the PCE
switch GPCSettings.calcMethod
    case 'projection'
        GPC = GPC.updateCoeffsWithGridContribution(IntegrationData.FinalCubature,...
            IntegrationData.ErrorScenarios.All.responses);
    case 'regression'
        GPC = GPC.calcCoeffsWithRegression(IntegrationData.ErrorScenarios.All.abscissas,...
            IntegrationData.ErrorScenarios.All.responses);
end

end



function varargout=setPaths(ModelDetails)
%setPaths sets the necessary Matlab and Python paths needed by openGPC
% 1. INPUT:
%   - ModelDetails: Structure with the details of the model for which the
%   GPC is built. Needed entries are:
%       - externalModel: flag for signalling non-Matlab model (logical)
% 2. OUTPUT:
%   - matlabPath: Updated Matlab path (optional)
%   - pythonPath: Updated Python path (optional)
% 3. DESCRIPTION: Function adds the current working directory and all
% subdirectories to Matlab path, and the src subdirectory to the Python
% path. 
% 4. TODO: 
%   - Proper check to determine openGPC directory

%% COPYRIGHT: Zoltan Perko, TU Delft
% Date: 2019.07.22.

%% FUNCTION DEFINITION:
if contains(pwd,'opengpc') && (exist('src','dir')==7)
    matlabPath = addpath(genpath(pwd));
else
    error('openGPC:openGPCNotWorkingDirectory',...
        'openGPC should be run from main openGPC folder as the working directory.')
end

if ModelDetails.externalModel
    pythonPath = py.sys.path;
    pythonPath.insert(int64(1),fullfile(pwd,'src'));
    ExtCoupling = py.importlib.import_module("externalCoupling");
end

switch nargout
    case 0
    case 1
        varargout{1} = matlabPath;
    case 2
        varargout{1} = matlabPath;
        varargout{2} = pythonPath;
    otherwise
        error('openGPC:setPaths:TooManyOutputsRequested',...
            'Only Matlab and Python path can be requested');
end
        
end