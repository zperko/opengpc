function [GPC,IntegrationData,ModelDetails,EvaluationScenarios,EvaluationSettings,DVHD,GammaSample] = ...
    performRobustnessAnalysis(GPCSettings,ModelDetails,EvaluationSettings)
%performRobustnessAnalysis performs GPC based robustness analysis of proton
%plans using Astroid and the Thinknode environment.
% 1. INPUT:
%   - GPCSettings: The GPCSettings structure of openGPC (structure)
%   - ModelDetails: The ModelDetails structure of openGPC (structure).
%   Necessary entries in addition to normal ModelDetails entries:
%       - externalModel = 1
%       - plann_iss_id = Astroid plan ISS ID
%       - requestedStructureMasks: Cell with the names (string) of
%       structures for which DVH distributions should be created.
%       - getNominalDetails = true (by default causes nominal dose
%       distribution and all structure masks to be downloaded.
%   - EvaluationSettings: Structure containing the evaluation settings to
%   be used with entries:
%       - confidenceLevels: The confidence levels for constructing the
%       confidence ellipsoids from which the evaluation scenarios will be
%       sampled (array, [0 1)]).
%       - nEvalScenarios: The number of sampled evaluation scenarios per
%       confidence level (integer)
%       - nDVHSamples: The number of GPC samples used to generate DVH
%       distributions (integer)
%       - nDVHDPoints: The number of dose points in the DVH distributions
%       (integer)
%       - probLevels: The probability levels (percentiles) for the DVH 
%       distributions (array, [0 100])
%       - gammaFlag: logical flag to indicate if gamma evaluation should be
%       done on the evaluation scenarios (logical)
%       - Tolerances: Structure to indicate the tolerances used for the
%       gamma evaluation, with entries:
%           - distanceToAgreement: distance to agreement tolerance in mm.
%           - doseTolerance: dose tolerance.
% 2. OUTPUT:
%   - GPC: The GPC object for the dose distribution and errors (GPCClass)
%   - IntegrationData: Structure with the raw data used for constructing
%   the GPC.
%   - ModelDetails: The updated ModelDetails structure of openGPC
%   - EvaluationScenarios: Dose distributions corresponding to the
%   different evaluation scenarios (cell of SampleClass objects)
%   - EvaluationSettings: Structure with the evaltuation settings.
%   - DVHD: The dose volume histogram distributions for the requested
%   structures
%   - GammaSample: SampleClass object containing the gamma values for the
%   evaluation scenarios.
% 3. TODO:
%   - Implement fractionation, for now everything is for 1 fraction
%   treatment

%% COPYRIGHT
% Zoltan Perko, TU Delft
% 2020.04.03.

%% FUNCTION DEFINITION
EvaluationSettings = checkInput(EvaluationSettings);

% Construct GPC
[GPC,IntegrationData,ModelDetails]=openGPC(GPCSettings,ModelDetails);

% Make evaluation scenarios and calculate the corresponding true dose
% distributions
EvaluationScenarios = makeEvaluationErrorScenarios(EvaluationSettings.confidenceLevels,...
    EvaluationSettings.nEvalScenarios,GPC.Details,'slice');
for i=1:numel(EvaluationScenarios)
    EvaluationScenarios{i}.computeResponses(ModelDetails,1:EvaluationSettings.nEvalScenarios);
end

% Compare true and GPC dose distributions
ModelDetails = convertResponseMasks(GPC,ModelDetails);
GammaSample = checkEvaluationErrorScenarios(EvaluationScenarios,GPC,ModelDetails,...
    EvaluationSettings.confidenceLevels,EvaluationSettings.gammaFlag,EvaluationSettings.Tolerances);

% Produce dose volume histogram distributions
DVHD = GPC.makeResponseHistogramDistribution(ModelDetails,EvaluationSettings.structureIndices,...
    EvaluationSettings.nDVHSamples,EvaluationSettings.probLevels,EvaluationSettings.nDVHDPoints);

end


function EvaluationSettings = checkInput(EvaluationSettings)
%checkInput is an input parser checks the EvaluationSettings structure
%input to PerformRobustnessAnalysis
p = inputParser;
p.FunctionName='performRobustnessAnalysis';
p.CaseSensitive = false;
p.StructExpand = true;
p.KeepUnmatched = true;
p.addParameter('confidenceLevels',[0.99 0.98 0.95 0.90],@(x)validateattributes(x,{'numeric'},...
    {'nonempty','vector','nonnegative','<=',1},'performRobustnessAnalysis:InvalidConfidenceLevels',...
    'confidenceLevels'));
p.addParameter('nEvalScenarios',50,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative','integer'},'performRobustnessAnalysis:InvalidNEvalScenarios',...
    'nEvalScenarios'));
p.addParameter('nDVHSamples',10000,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative','integer'},'performRobustnessAnalysis:InvalidNDVHSamples',...
    'nDVHSamples'));
p.addParameter('nDVHDPoints',100,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative','integer'},'performRobustnessAnalysis:InvalidNDVHDPoints',...
    'nDVHDPoints'));
p.addParameter('probLevels',[95 75 25],@(x)validateattributes(x,{'numeric'},...
    {'nonempty','vector','nonnegative'},'performRobustnessAnalysis:InvalidProbLevels',...
    'ProbLevels'));
p.addParameter('gammaFlag',true,@(x)validateattributes(x,{'logical'},...
    {'nonempty','scalar'},'performRobustnessAnalysis:gammaFlag',...
    'gammaFlag'));
p.addParameter('Tolerances',struct('distanceToAgreement',1,'doseTolerance',0.5),@(x)assert(...
    isstruct(x) && all(ismember({'distanceToAgreement','doseTolerance'},fieldnames(x))) && ...
    isscalar(x.distanceToAgreement) && isnumeric(x.distanceToAgreement) && ...
    isscalar(x.doseTolerance) && isnumeric(x.doseTolerance),...
    'performRobustnessAnalysis:InvalidTolerances',...
    'Tolerances must be a structure with entries doseTolerance and distanceToAgreement.'));
p.addParameter('structureIndices',1,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','vector','nonnegative','integer'},'performRobustnessAnalysis:InvalidStructureIndices',...
    'structureIndices'));

p.parse(EvaluationSettings);
EvaluationSettings = p.Results;

end