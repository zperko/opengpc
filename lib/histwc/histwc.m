function [histogramWeights, valueBoundaries] = histwc(values, weights, nBins)
%histwc calculates a histogram based on weights for a number of bins
% 1. INPUT: 
%   - values: values to be histogrammed (double, nSamples X nResponses)
%   - weights: weight values for each response (double, 1 X nResponses)
%   - nBins: Number of bins (Integer)
% 2. OUTPUT:
%   - histogramWeights: weighted histograms for all samples (nSamples X
%   nBins)
%   - valueBoundaries: the values of the bin boundaries;
% 3. DESCRIPTION:
% This function generates a vector of cumulative weights for data
% histogram. Equal number of bins will be considered using minimum and 
% maximum values of the data. Weights will be summed in the given bin.
% Usage: [histWeights, vBoundaries] = histwc(values, weights, nBins)
%
% See also: HISTC, HISTWCV
% Author:
% mehmet.suzen physics org
% BSD License
% July 2013


  minV  = min(values(:));
  maxV  = max(values(:))+eps;
  valueBoundaries = linspace(minV, maxV, nBins);
  nSamples = length(values(:,1));
  histogramWeights = zeros(nSamples,nBins);
  for i=2:length(valueBoundaries)
      ind = find(values>=valueBoundaries(i) | values<valueBoundaries(i-1));
      if ~isempty(ind)
          weightSample = ones(nSamples,1)*weights;
          weightSample(ind)=0;
          histogramWeights(:,i-1) = sum(weightSample,2);
      end
  end
end