# OpenGPC
OpenGPC is an Open Source Generalized Polynomial Chaos Expansion Toolbox
Released under the EUPL licence (see licence.txt).

Copyright: Zoltan Perko and Sebastian van der Voort

If used please always cite:
- Z. Perko, L. Gilli, D. Lathouwers and J.L. Kloosterman, "Grid and basis adaptive polynomial chaos techniques for sensitivity and uncertainty analysis", Journal of Computational Physics, 260, pp. 54-84, 2014.

If used in medical physics context, please also cite:
- Z. Perko, S.R. van der Voort, S. van de Water, C.M.H. Hartman, M. Hoogeman, D. Lathouwers, "Fast and Accurate Sensitivity Analysis of IMPT Treatment Plans Using Polynomial Chaos Expansion", Physics in Medicine and Biology, 61(12), pp. 4646, 2016.

If used in engineering context, please also cite:
- Z. Perko, D. Lathouwers, J.L. Kloosterman, T. van der Hagen, "Large scale applicability of a Fully Adaptive Non-Intrusive Spectral Projection technique: Sensitivity and uncertainty analysis of a transient", Annals of Nuclear Energy, 71, pp. 272-292, 2014.

## Coding convention and notation
The toolbox mainly follows the coding style of [Richard Johnson, "MATLAB Style Guidelines 2.0", Version 2, March, 2014](http://www.datatool.com/downloads/MatlabStyle2%20book.pdf). 

### Notation
The following notation is used:
1. Variables: Written in camel case, starting with a lower case letter without underscores. E.g. order, quadratureRule, nBasisVectors.
1. Constants: All uppercase, separated by underscores, e.g. PI.
1. Structures and objects: Written in camel case, starting with upper case letter, without underscores. E.g. Options, Quadrature, Cubature.
1. Functions: Written in camel case, starting with lower case, without underscores.
1. Prefixes: For notational convenience, certain prefixes are used to indicate commonalities between variables:
	- n: Number of objects.
	- u: Unique objects in an object set.
	- i: Iterator for an object set.
1. GPC: GPC is an exception from the above notation rules, it is always capitalized and is followed by an underscore. The rest is as above, e.g. GPC_Test, GPC_Results, GPC_ErrorEstimator.

### Coding convention
As coding convention the following rules are used:
1. Functions:
	- Header written before the function, explaining purpose, use, inputs and outputs.
	- set, get: reserved for objects.
	- compute/comp: reserved for functions computing something.
	- find: reserved for looking up things without computing.
	- initialize/init: reserved for initializing an object or variable.
	- is: reserved for boolean checks.
	- Complement operations: get/set, add/remove, create/destroy, start/stop, insert/delete, increment/decrement, old/new, begin/end, firt/last, up/down, min/max, next/previous, open/close, show/hid, suspend/resume.
	- Functions should ideally be limited to 2 screens in length.
	- Functions should take structures and objects as inputs, give similar outputs.
1. General:
	- Units as suffixes if needed, e.g. lengthMeter, weightGram.

## Dependencies
The code uses:
jsonlab v1.9
Kron_Fast