function [QuadratureSets] = loadNeededQuadratureSets(...
    quadratureRules,maxGridLevel,nExtraLevels)
%loadNeededQuadratureSets loads all necessary quadarture sets for the given
%types up to the requred grid levels
% 1. INPUT:
%   - quadratureRules: Required quadrature types (1 X N, cell of strings)
%   - maxGridLevel: The maximum grid level (integer)
%   - nExtraLevels: The number of extra grid levels (integer)
% 2. OUTPUT:
%   - QuadratureSets: a matrix of QuadratureClass objects (M X N)
% 3. DESCRIPTION:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.04.

%% FUNCTION DEFINITION
Families={'gauss_hermite','gauss_legendre','gauss_laguerre'; 0,0,0};
for i=1:length(quadratureRules)
    if (find(strcmpi(quadratureRules(i),Families(1,:))))
        Families(2,strcmpi(quadratureRules(i),Families(1,:))) = {1};
    end
end
Families=Families(:,[Families{2,:}]==1);
QuadratureSets = QuadratureClass('Initial',[maxGridLevel, length(Families(1,:))]);
for i=1:length(QuadratureSets(1,:))
    for j=1:length(QuadratureSets(:,1))
        QuadratureSets(j,i)=set(QuadratureSets(j,i),'type',Families{1,i});
    end
    QuadratureSets(:,i)=loadQuadratureSet(QuadratureSets(:,i),maxGridLevel);
    QuadratureSets(:,i)=makeDifferenceQuadratureSet(QuadratureSets(:,i),nExtraLevels);
end

end