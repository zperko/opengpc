function y=legendr(x,o)
%legendr calculates the value of the oth Legendre polynomial at abscissas
% x from the recursion rule (n+1)P_{n+1}(x)=(2*n+1)*x*P_n(x)-n*P_{n-1}(x)
% 1. INPUT: 
%   - x: vector of abscissas (N x 1, double)
%   - o: Legendre polynomial orders (N x 1, integer)
% 2. OUTPUT: 
%   - y: vector of values of L_o(x)
% 3. DESCRIPTION:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

N = length(x);

switch (max(o))
    case 0
        p = ones(N,1);
    case 1
        p = ones(N,2);
        p(:,2) = x;
    otherwise
        p=zeros(N,max(o)+1);
        p(:,1)=ones(length(x),1);
        p(:,2)=x;
        for i=3:max(o)+1
            p(:,i)=(2*(i-2)+1)*p(:,2).*p(:,i-1)/(i-1)-(i-2)*p(:,i-2)/(i-1); 
        end
end

y=p(:,o+1);

end