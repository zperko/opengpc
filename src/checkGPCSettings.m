function [GPCSettings]=checkGPCSettings(GPCSettings)
%checkGPCSettings performs some basic checks on the provided input
%options to openGPC
% 1. INPUT: 
%   - GPCSettings: The structure with the settings for constructing the
%   GPC:
%       - variableNames: Name of the random variables to take into account
%       (1 x N, cell of strings).
%       - polType: Name of polynomials corresponding to random variable 
%       distributions, can be 'hermite' or 'legendre' (1 x N,
%       cell of strings).
%       - means: The means of the distributions (1 x N, double)
%       - stds: The standard deviations/half widths of the distributions
%       (1 x N, double) 
%       - maxPolOrder: The maximum allowed polynomial order in the GPC
%       (integer).
%       - normType: The norm used to trim the GPC basis, (double, (0,1])
%       - trim: Flag for hyperbolic trimming (logical).
%       - calcMethod: calculation method ('projection' or 'regression').
%       - samplingMethod: The sampling method for choosing training points 
%       ('sparse','full','random','lhs'). Can be sparse grid, full grid, 
%       random or latin hypercube sampling.
%       - maxGridLevel: The maximum allowed integration level (integer).
%       Only used in projection.
%       - nExtraLevels: The number of extra levels for extended grids
%       (integer). Only used in projection.
%       - nTrainingPoints: The number of training points to be used
%       (integer). Only used in regression.
%       - cutoffValue: The value below which responses are neglected when
%       constructing the GPC
% 2. OUTPUT:
%   - GPC_Settings: The structure with potentially changed settings

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.10.25.

%% FUNCTION DEFINITION
p = inputParser;
p.FunctionName = 'checkGPCSettings';
p.CaseSensitive = false;
p.StructExpand = true;
p.KeepUnmatched = true;

%% Required arguments
expectedPolTypes = {'hermite','legendre'};
requiredParameters = {'calcMethod','samplingMethod','maxPolOrder','polType','means','stds'};

foundParams = ismember(lower(requiredParameters),lower(fieldnames(GPCSettings)));
assert(all(foundParams),'openGPC:checkGPCSettings:RequiredSettingsMissing',...
    sprintf('Setting(s) %s missing.',strjoin(requiredParameters(~foundParams),', ')))
validateattributes(GPCSettings.polType,{'cell'},{'row'},...
    'openGCP:checkGPCSettings','polType');
nDim = length(GPCSettings.polType);

% Check calculation method, positional argument 1
p.addRequired('calcMethod',@(x)assert(ischar(x) && ismember(x,{'projection','regression'}),...
    'openGPC:checkGPCSettings:InvalidCalcMethod',...
    'calcMethod must be ''projection'' or ''regression''.'));

% Check sampling method for training points, positional argument 2
p.addRequired('samplingMethod',@(x)assert(ischar(x) && ismember(x,{'sparse','full','random','lhs'}),...
    'openGPC:checkGPCSettings:InvalidSamplingMethod',['samplingMethod must ',...
    'be ''full'', ''sparse'', ''random'' or ''lhs''.']));
p.addParameter('maxGridLevel',2,@(x)validateattributes(x,{'numeric'},...
    {'scalar','positive','integer'},'openGPC:checkGPCSettings:InvalidMaxGridLevel',...
    'maxGridLevel'));
p.addParameter('nExtraLevels',0,@(x)validateattributes(x,{'numeric'},...
    {'scalar','integer','nonnegative'},'openGPC:checkGPCSettings:InvalidExtraLevels',...
    'nExtraLevels'));
p.addParameter('nTrainingPoints',200,@(x)validateattributes(x,{'numeric'},...
    {'scalar','integer','positive'},'openGPC:checkGPCSettings:InvalidNumberOfTrainingPoints',...
    'nTrainingPoints'));

% Check polynomial order, positional argument 3
p.addRequired('maxPolOrder',@(x)validateattributes(x,{'numeric'},...
    {'scalar','integer','positive'},'openGPC:checkGPCSettings:InvalidMaxPolOrder',...
    'maxPolOrder'));

% Check polynomial types, positional argument 4
p.addRequired('polType',@(x)assert(all(ismember(lower(x),expectedPolTypes)),...
   'openGPC:checkGPCSettings:InvalidPolynomialType',...
   'polType must be a cell array with ''hermite'' or ''legendre'' entries.'));

% Check means, positional argument 5
p.addRequired('means',@(x)assert(isnumeric(x) && isrow(x) && length(x)==nDim,...
    'openGPC:checkGPCSettings:InvalidMeans',...
    'Variable means must be row vector of same size as polType.'));

% Check stds, positional argument 6
p.addRequired('stds',@(x)assert(isnumeric(x) && isrow(x)  && all(x>0) && length(x)==nDim,...
    'openGPC:checkGPCSettings:InvalidStds',...
    'Variable stds must be nonzero row vector of same size as polType.'));

%% Optional arguemnts
% Check variable names
default = cell(1,nDim);
for i=1:nDim, default{1,i} = ['Variable ',num2str(i)]; end
p.addParameter('variableNames',default,@(x)assert(iscell(x) && isrow(x) && length(x)==nDim,...
    'openGPC:checkGPCSettings:InvalidVariableNames',...
    'variableNames must be row cell array of strings'));

% Check normType
p.addParameter('normType',1,@(x)validateattributes(x,{'numeric'},...
    {'scalar','nonnegative','<=',1},'openGPC:checkGPCSettings:InvalidNormType',...
    'normType'));

% Check trim
p.addParameter('trim',false,@(x)validateattributes(x,{'logical'},{'nonempty','scalar'},...
    'openGPC:checkGPCSettings:InvalidGPCTrimOption','trim'));

% Check response cutoff value
p.addParameter('cutoffValue',0,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative'},...
    'openGPC:checkGPCSettings:InvalidCutoffValue',...
            'cutoffValue'));
p.addParameter('cutoffScenarioVariation',3,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative'},...
    'openGPC:checkGPCSettings:InvalidCutoffScenarioVariation','cutoffScenarioVariation'));

%% Parsing GPCSettings
p.parse(GPCSettings.calcMethod,GPCSettings.samplingMethod,GPCSettings.maxPolOrder,...
    GPCSettings.polType,GPCSettings.means,GPCSettings.stds,GPCSettings)
GPCSettings = p.Results;
GPCSettings.polType = lower(GPCSettings.polType);

if ~isempty(p.UsingDefaults)
    warning('openGPC:checkGPCSettings:DefaultSettingsUsed',...
        'Using default values for %s, consider whether these settings should be specified.',...
        strjoin(p.UsingDefaults,', '));
end

if ~isempty(fieldnames(p.Unmatched))
    warning('openGPC:checkGPCSettings:UnmatchedSetting',...
        'Settings %s do not match any valid GPC settings. Ignoring them',...
        strjoin(fieldnames(p.Unmatched),', '));
end

end