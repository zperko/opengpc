function Samples = uniformlySampleSurface(Phi,uLimits,N,samplingMethod)
%Function uniformlySampleSurface samples an m dimensional surface in an
% n dimensional space described by Phi, providing N samples
% 1. INPUTS:
%   - Phi: the analytical description of the n dimensional points on the
%     m dimensional surface as 
%           Phi = [x_1(u_1, ... , u_m), ... , x_n(u_1, ... , u_m) ], which
%           is a function handle
%   - u_Limits: The min and max limits of u
%   - N: The number of n dimensional samples wanted
%   - method: method for sampling, either 'rejection' (default) or
%   'slice'
% 2. OUTPUTS: 
%   - Samples: the N*n matrix containing the coordinates of the N samples
% 3. DESCRIPTION: Function based on N. P. Kopytov, E. A. Mityushov, "The
% method for uniform distribution of points on surfaces in
% multi-dimensional Euclidean space". The implementation uses the
% jacobianest function to quickly and numerically approximate the Jacobian
% for the calculation of the metric tensor. Both rejection sampling and 
% slice sampling can be used.
% 4. TODO:
%   - Implement some adaptivity instead of the fixed number of evaluations

%% FUNCTION DEFINITION
if nargin == 3
    samplingMethod = 'rejection';
end

% First we need the g metric g=det(g_{i,j}), where 
% g_{i,j} = \sum_k \pdiff{\Phi_k}{\u_i}\pdiff{\Phi_k}{\u_j}

Lambda = @(u)(jacobianest(Phi,u));
g = @(u)(sqrt(det(Lambda(reshape(u,[],1))'*Lambda(reshape(u,[],1)))));
disp('Starting sampling');
tic;
nMaxEvals = 1e4;

switch(samplingMethod)
    case 'rejection'
        r=ones(nMaxEvals,1)*uLimits(:,1)'+...
            rand(nMaxEvals,length(uLimits(:,1))).*(ones(nMaxEvals,1)*(uLimits(:,2)-uLimits(:,1))');
        gDist = zeros(nMaxEvals,1);
        for i=1:nMaxEvals
            gDist(i,1) = g(r(i,:)');
        end
        gMax = max(gDist);
        [~,fMaxOpt]=fmincon(@(u)(-g(u)),(uLimits(:,1)+uLimits(:,2))./2,[],[],[],[],uLimits(:,1),uLimits(:,2));
        gMax = max(gMax,-fMaxOpt);
        disp('Maximum found');
        toc;
        % Now we can start sampling
        finished = false;
        Samples = zeros(N,length(Phi(r(1,:)')));
        n=1;
        while ~finished
            r=ones(nMaxEvals,1)*uLimits(:,1)'+...
                rand(nMaxEvals,length(uLimits(:,1))).*(ones(nMaxEvals,1)*(uLimits(:,2)-uLimits(:,1))');
            fValues = zeros(nMaxEvals,1);
            for i=1:nMaxEvals
                fValues(i,1) = g(r(i,:)');
                if ~isreal(fValues(i,1))
                    fValues(i,1) = -inf;
                end
            end
            rAccept = rand(nMaxEvals,1);
            acceptedSamples = fValues > rAccept*gMax;
            disp(['Sampling efficiency: ',num2str(sum(acceptedSamples)/numel(acceptedSamples))]);
            Samples(n:n+sum(acceptedSamples)-1,:) = Phi(r(acceptedSamples,:)')';
            n=n+sum(acceptedSamples);
            if n>N
                finished = true;
                Samples = Samples(1:N,:);
            end
        end
    case 'slice'
        rAccepted = slicesample(sum(uLimits,2)/2,N,'pdf',g,'burnin',50,'thin',3);
        Samples = Phi(rAccepted')';
end
disp('Sampling finished');
toc;
end