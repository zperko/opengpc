# Author: Zoltan Perko
# Date: 2019.07.15.
# Copyright: Delft University of Technology, RST
# Description:

import sys, os
import json
import subprocess
import time
import puma
import numpy as np
# from connect import *

def submitCalculations(SampleStructure,ModelDetails,responseMask=None):
    print("Submitting calculations started at ",time.asctime())

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."

    SampleStructure["newIDs"] = list()
    SampleStructure["newStatus"] = list()
    SampleStructure["newSubProcesses"] = list()
    SampleStructure["newFileNames"] = list()

    for i in range(SampleStructure['nSamples']):
        SampleStructure['newStatus'].append(None)
        SampleStructure['newIDs'].append(None)
        SampleStructure['newSubProcesses'].append(None)
        SampleStructure['newFileNames'].append(None)
        if SampleStructure['status'][i] =='posting':
            if ModelDetails['modelCase']==0:
                [SampleStructure['newIDs'][i], SampleStructure['newSubProcesses'][i],
                SampleStructure['newStatus'][i]] = userFunction(
                    SampleStructure['abscissas'][i],ModelDetails)
                time.sleep(ModelDetails['externalModelDelay'])
            elif ModelDetails['modelCase']>0:
                inputs = [str(x) for x in SampleStructure['abscissas'][i]]
                childProcess = subprocess.Popen(["python",os.path.join(ModelDetails['scriptDirectory'],"externalBlackboxModel.py"),
                    "-case",str(int(ModelDetails['modelCase'])),"-dir",ModelDetails['calcDirectory'],"--i"] + inputs,
                    stdout=subprocess.PIPE)
                SampleStructure['newSubProcesses'][i] = childProcess
                SampleStructure['newIDs'][i] = str(childProcess.pid)
                SampleStructure['newStatus'][i] = 'calculating'

            fileName = os.path.join(ModelDetails['calcDirectory'],"ModelCase_"+str(int(ModelDetails['modelCase']))+
                "_PID_"+str(SampleStructure['newIDs'][i]+".json"))
            SampleStructure['newFileNames'][i] = fileName
        else:
            SampleStructure['newIDs'][i] = SampleStructure['IDs'][i]
            SampleStructure['newStatus'][i] = SampleStructure['Status'][i]
            SampleStructure['newSubProcesses'][i] = SampleStructure['subProcesses'][i]
            SampleStructure['newFileNames'][i] = SampleStructure['fileNames'][i]

    print("Submitting calculations finished at ",time.asctime())
    return SampleStructure

def requestStatusUpdate(SampleStructure,ModelDetails):
    print("Status update started at ",time.asctime())

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."

    SampleStructure['newStatus']=list()
    # Check for login in errors, etc.
    for i in range(SampleStructure['nSamples']):
        SampleStructure['newStatus'].append(SampleStructure['status'][i])
        if SampleStructure['status'][i] in 'calculating':
            if ModelDetails['modelCase']==0:
                status = pumaStatusUpdate(SampleStructure['IDs'][i],
                    ModelDetails['maxExternalRetryNumber'])
                time.sleep(ModelDetails['externalModelDelay'])

                if 'completed' in status:
                    SampleStructure['newStatus'][i] = 'completed'
                elif status in ['calculating','uploading','queued']:
                    SampleStructure['newStatus'][i] = 'calculating'
                elif status in ['error','failed']:
                    SampleStructure['newStatus'][i] = 'failed'
                else:
                    SampleStructure['newStatus'][i] = status
            elif ModelDetails['modelCase']>0:
                status = str(SampleStructure['subProcesses'][i].poll())
                if  status == "None":
                    SampleStructure['newStatus'][i] = 'calculating'
                elif status == SampleStructure['IDs'][i]:
                    SampleStructure['newStatus'][i] = 'completed'
                else:
                    SampleStructure['newStatus'][i] = 'failed'

            if SampleStructure['newStatus'][i] == 'failed':
                print("Calculation ",SampleStructure['IDs'][i]," failed.")
                # getErrorDetails()

    print("Status update finished at ",time.asctime())
    return SampleStructure

def readResults(SampleStructure,ModelDetails):
    print("Reading results started at ",time.asctime())

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."
    
    SampleStructure['newResponses']=list()
    SampleStructure['newStatus']=list()
    for i in range(SampleStructure['nSamples']):
        SampleStructure['newResponses'].append(SampleStructure['responses'][i])
        SampleStructure['newStatus'].append(SampleStructure['status'][i])
        if SampleStructure['status'][i] == 'completed':
            print("Reading results for ",SampleStructure['IDs'][i])
            if ModelDetails['modelCase']==0:
                successFlag = False
                nTries = 0
                while (not successFlag) and (nTries < ModelDetails['maxExternalRetryNumber']):
                    try:
                        doseImage3D = puma.iss.get(SampleStructure['IDs'][i])
                        time.sleep(ModelDetails['externalModelDelay'])
                        successFlag = True
                        if SampleStructure['Details']['masked'] == True:
                            mask = SampleStructure['Details']['mask'].tolist()[0]
                            SampleStructure['newResponses'][i] = list(np.array(
                                doseImage3D['pixels'])[mask])
                        else:
                            SampleStructure['newResponses'][i] = doseImage3D['pixels']
                        SampleStructure['newStatus'][i] = 'fetched'
                    except:
                        print("Getting results failed. Trying again in 1 s.")
                        time.sleep(1)
                        nTries = nTries + 1
                if nTries>=ModelDetails['maxExternalRetryNumber']:
                    print("Getting results failed permanently for ",
                        SampleStructure['IDs'][i])
                
            elif ModelDetails['modelCase']>0:
                with open(SampleStructure['fileNames'][i]) as fh:
                    if SampleStructure['Details']['masked'] == True:
                        mask = SampleStructure['Details']['mask'].tolist()[0]
                        SampleStructure['newResponses'][i] = list(np.array(json.load(fh))[mask])
                    else:
                        SampleStructure['newResponses'][i] = json.load(fh)
                SampleStructure['newStatus'][i] = 'fetched'

    print("Reading results finished at ",time.asctime())
    return SampleStructure

def writeResults(SampleStructure,ModelDetails):
    print("Writing results started at ",time.asctime())

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."

    SampleStructure['newStatus']=list()
    SampleStructure['newIDs']=list()
    for i in range(SampleStructure['nSamples']):
        SampleStructure['newStatus'].append(SampleStructure['status'][i])
        SampleStructure['newIDs'].append(SampleStructure['IDs'][i])
        if SampleStructure['status'][i] == 'writing':
            print("Writing results for ",SampleStructure['IDs'][i])
            if ModelDetails['modelCase']==0:
                successFlag = False
                nTries = 0
                schema = 'named/mgh/dosimetry/unboxed_image_3d'
                obj = ModelDetails['nominalModelDetails']['doseUnboxedImage3D']

                if SampleStructure['Details']['masked'] == True:
                    mask = SampleStructure['Details']['mask'].tolist()[0]
                    obj['pixels'] = np.array(obj['pixels'])
                    obj['pixels'][mask] = SampleStructure['responses'][i]
                    obj['pixels'] = list(obj['pixels'])
                else:
                    obj['pixels'] = list(SampleStructure['responses'][i])

                while (not successFlag) and (nTries < ModelDetails['maxExternalRetryNumber']):
                    try:
                        SampleStructure['newIDs'][i] = puma.iss.post(schema,obj)
                        time.sleep(ModelDetails['externalModelDelay'])
                        SampleStructure['newStatus'][i] = 'written'
                        successFlag = True
                    except:
                        print("Posting results failed. Trying again in 1 s.")
                        time.sleep(1)
                        nTries += 1

                if nTries>=ModelDetails['maxExternalRetryNumber']:
                    print("Writing results failed permanently for ", SampleStructure['IDs'][i])
            elif ModelDetails['modelCase']>0:
                print("Needs to be coded down")

    print("Writing results finished at ",time.asctime())
    return SampleStructure

def submitComparisonCalculations(SampleStructure,ModelDetails,refDoseIDs,Tolerances):
    print("Submitting response comparions calculations started at ",time.asctime())
    doseTolerance = Tolerances['doseTolerance']
    distanceToAgreement = Tolerances['distanceToAgreement']

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."

    SampleStructure["newIDs"] = list()
    SampleStructure["newStatus"] = list()
    for i in range(SampleStructure['nSamples']):
        SampleStructure['newIDs'].append(SampleStructure['IDs'][i])
        SampleStructure['newStatus'].append(SampleStructure['status'][i])
        if SampleStructure['status'][i] == 'comparing':
            if ModelDetails['modelCase']==0:
                successFlag = False
                nTries = 0
                
                doseCalc = {
                    "function": {
                        "account": "mgh",
                        "app": "dosimetry",
                        "name": "box_image_3d",
                        "args": [{"reference":SampleStructure['IDs'][i]}]
                            }
                    }
                refDoseCalc = {
                    "function": {
                        "account": "mgh",
                        "app": "dosimetry",
                        "name": "box_image_3d",
                        "args": [{"reference":refDoseIDs[i]}]
                            }
                    }
                gammaCalc = {
                    "function": {
                        "account": "mgh",
                        "app": "dosimetry",
                        "name": "compute_gamma_index_values_3d",
                        "args": [
                            doseCalc,
                            refDoseCalc,
                            {"value": doseTolerance},
                            {"value": distanceToAgreement}
                            ]
                        }
                    }

                # Post gamma calculation
                successFlag = False
                nTries = 0
                while (not successFlag) and (nTries < ModelDetails['maxExternalRetryNumber']):
                    try:
                        time.sleep(ModelDetails['externalModelDelay'])
                        ID = puma.calc.post({"function": 
                            {
                                "account":"mgh", 
                                "app":"dosimetry", 
                                "name": "unbox_image_3d", 
                                "args": [gammaCalc]
                            }
                        })
                        SampleStructure['newIDs'][i] = ID
                        successFlag = True
                    except:
                        print("Puma gamma calculation submission failed. Tyring again in 1 s.")
                        time.sleep(1)
                        nTries = nTries + 1
                # Check submitted calculation
                status = pumaStatusUpdate(ID,ModelDetails['maxExternalRetryNumber'])

                if status in 'uploading' or 'queued' or 'calculating':
                    SampleStructure['newStatus'][i] = 'calculating'
                else:
                    SampleStructure['newStatus'][i] = status

                if nTries>=ModelDetails['maxExternalRetryNumber']:
                    print("Writing results failed permanently for ", SampleStructure['IDs'][i])

            elif ModelDetails['modelCase']>0:
                print("Needs to be coded down")
    
    print("Submitting response comparisons finished at ",time.asctime())

    return SampleStructure

def userFunction(abscissas,ModelDetails):
    """ userFunction performs the external model calculation for the given abscissa values """
    print("Submitting dose recalculation started at ",time.asctime())
    Errors = {
        'gantry_angle_shift': 0,
        'couch_angle_shift': 0,
        'rsp_scale_factor': 0,
        'snapshot': None,
        'spots': None}

    acceptedErrors = {
        'beam_shift_X':0,
        'beam_shift_Y':0,
        'beam_shift_Z':0,
        'patient_shift_X':0,
        'patient_shift_Y':0,
        'patient_shift_Z':0}
    
    for index in range(len(ModelDetails['variableNames'])):
        if ModelDetails['variableNames'][index] in Errors.keys():
            Errors[ModelDetails['variableNames'][index]] = abscissas[index]
        elif ModelDetails['variableNames'][index] in acceptedErrors.keys():
            acceptedErrors[ModelDetails['variableNames'][index]] = abscissas[index]
        else:
            print("Unknown error type %s. Ignoring it.",ModelDetails['variableNames'][index])
    Errors['beam_shift'] = [
        acceptedErrors['beam_shift_X'],
        acceptedErrors['beam_shift_Y'],
        acceptedErrors['beam_shift_Z']
        ]
    Errors['patient_shift'] = [
        acceptedErrors['patient_shift_X'],
        acceptedErrors['patient_shift_Y'],
        acceptedErrors['patient_shift_Z']
        ]
    if Errors['snapshot'] == None:
        Errors.pop('snapshot')
    if Errors['spots'] == None:
        Errors.pop('spots')

    # Post dose recalc
    successFlag = False
    nTries = 0
    while (not successFlag) and (nTries < ModelDetails['maxExternalRetryNumber']):
        try:
            (dose_sum_id,beam_doses) = puma.recalc.recompute_plan_dose(ModelDetails['plan_iss_id'],
                Errors)
            successFlag = True
        except:
            print("Puma dose recomputation submission failed. Tyring again in 1 s.")
            time.sleep(1)
            nTries = nTries + 1

    # Post dose unboxing
    successFlag = False
    nTries = 0
    while (not successFlag) and (nTries < ModelDetails['maxExternalRetryNumber']):
        try:
            ID = puma.calc.post({"function": 
                    {
                        "account":"mgh", 
                        "app":"dosimetry", 
                        "name": "unbox_image_3d", 
                        "args": [
                            {
                                "reference":dose_sum_id
                            }
                        ]
                    }
                })
            successFlag = True
        except:
            print("Puma dose unboxing submission failed. Tyring again in 1 s.")
            time.sleep(1)
            nTries = nTries + 1

    # Check submitted calculation
    status = pumaStatusUpdate(ID,ModelDetails['maxExternalRetryNumber'])

    if status in 'uploading' or 'queued' or 'calculating':
        status = 'calculating'
        subProcess = beam_doses
    else:
        subProcess = []

    print("Submitting dose recalculation finished at ",time.asctime())
    # import puma.import_
    # Then import_ functions are usable as usual puma.import_.function
    # patient model need to be added to CT+structure
    return [ID, subProcess, status]

# def userFunctionRS(abscissas,ModelDetails):
#     """ userFunction performs the external model calculation for the given abscissa values """
#     print("Submitting dose recalculation started at ",time.asctime())
#     Errors = {
#         'gantry_angle_shift': 0,
#         'couch_angle_shift': 0,
#         'rsp_scale_factor': 0,
#         'snapshot': None,
#         'spots': None}

#     acceptedErrors = {
#         'beam_shift_X':0,
#         'beam_shift_Y':0,
#         'beam_shift_Z':0,
#         'patient_shift_X':0,
#         'patient_shift_Y':0,
#         'patient_shift_Z':0}
    
#     for index in range(len(ModelDetails['variableNames'])):
#         if ModelDetails['variableNames'][index] in Errors.keys():
#             Errors[ModelDetails['variableNames'][index]] = abscissas[index]
#         elif ModelDetails['variableNames'][index] in acceptedErrors.keys():
#             acceptedErrors[ModelDetails['variableNames'][index]] = abscissas[index]
#         else:
#             print("Unknown error type %s. Ignoring it.",ModelDetails['variableNames'][index])
#     Errors['beam_shift'] = [
#         acceptedErrors['beam_shift_X'],
#         acceptedErrors['beam_shift_Y'],
#         acceptedErrors['beam_shift_Z']
#         ]
#     Errors['patient_shift'] = [
#         acceptedErrors['patient_shift_X'],
#         acceptedErrors['patient_shift_Y'],
#         acceptedErrors['patient_shift_Z']
#         ]
#     if Errors['snapshot'] == None:
#         Errors.pop('snapshot')
#     if Errors['spots'] == None:
#         Errors.pop('spots')
    
#    clinicDB = get_current('ClinicDB')
#    patientDB = get_current('PatientDB')

#    patientInfo = patientDB.QueryPatientInfo(Filter={'ID':ModelDetails['plan_iss_id']})
#    if len(patientInfo)==1:
#        patient = patientDB.LoadPatient(PatientInfo=patientInfo[0])
#    else:
#        raise Exception("Patient info not found or more than one patient found.")

 #   patient = get_current('Patient')
 #   caseName = ModelDetails['caseName']
 #   try:
 #       case = patient.Cases[caseName]
 #   except:
 #       raise Exception("No case named {0} found for current patient.".format(caseName))
 #   planName = ModelDetails['planeName']
 #   try:
 #       plan = case.TreatmentPlans[planName]
 #   except:
 #       raise Exception("No plan named {0} found for current case.".format(planName))
    
 #   plan = get_current('Plan')

 #   QueryPatientInfo
#     root.LoadAggregate(LockMode="Write",ReadBinaryData=False,CollectionName=None,AggregateUuid=000)

#     BeamSet = get_current("BeamSet")

#     BeamSet.ComputePerturbedDose(DensityPerturbation = Errors['rsp_scle_factor'], 
#         IsocenterShift={'x':Errors['patient_shift_X'],'y':Errors['patient_shift_Y'],'z':Errors['patient_shift_Z']}, 
#         AllowGridExpansion=False, ComputeBeamDoses=True, OnlyOneDosePerImageSet=False, FractionNumbers=[0])
#     patient.Cases['Case 1'].TreatmentPlans

#     return [ID, subProcess, status]





def getNominalModelDetails(ModelDetails):
    print("Getting nominal model details started at ",time.asctime())

    if ModelDetails['modelCase']==0:
        cradleStatus = getCradleStatus()
        assert ('running' in cradleStatus),"Cradle could not be started."

    newModelDetails = dict()
    newModelDetails['doseUnboxedImage3D'] = puma.iss.get(ModelDetails['nominalResponseID'][0])
    if "requestedStructureMasks" in ModelDetails.keys():
        newModelDetails['structureMasks'] = getStructureMasks(ModelDetails['plan_iss_id'],ModelDetails['requestedStructureMasks'])

    return newModelDetails

def getStructureMasks(planID,requestedStructureMasks):
    planSummary = puma.plan.summary(planID)
    doseImageID = puma.plan.dose_id(planID)
    doseImageRequest = {"reference":doseImageID}
    gridRequest = {
        "function": {
            "account": "mgh",
            "app": "dosimetry",
            "name": "get_image_grid_3d",
            "args": [doseImageRequest]
            }
        }
    structureMasks = list()
    for index, structure in enumerate(planSummary["structures"]):
        label = puma.utilities.strip_styling(structure["label"])
        print(label)
        if label in requestedStructureMasks: 
            geometryID = puma.plan.resolve_results_query("generate_structure_geometry_request",
                planID,[index])
            structureVoxelsRequest={
                "property": {
                    "field": {
                        "value": "cells_inside"
                    },
                    "object": {
                        "function": {
                            "account": "mgh",
                            "app": "dosimetry",
                            "name": "compute_grid_cells_in_structure",
                            "args": [
                                gridRequest,
                                {
                                    "reference": geometryID
                                },
                            ],
                        }
                    },
                    "schema": {
                        "array_type": {
                            "element_schema": {
                                "named_type": {
                                    "account": "mgh",
                                    "app": "dosimetry",
                                    "name": "weighted_grid_index",
                                }
                            }
                        }
                    }
                }
            }
            maskCalcID = puma.calc.post(structureVoxelsRequest)
            maskCalcStatus = 'calculating'
            structureMasks.append({'ID':maskCalcID,'status':maskCalcStatus,'mask':None,'label':label})
        
    finished = False
    while not finished:
        finished = True
        time.sleep(15)
        for index in range(len(structureMasks)):
            if structureMasks[index]['status']=='calculating':
                structureMasks[index]['status'] = pumaStatusUpdate(structureMasks[index]['ID'])
                time.sleep(0.2)
                if structureMasks[index]['status'] in ['uploading','queued','calculating']:
                    structureMasks[index]['status'] = 'calculating'
                    finished = False

    for index in range(len(structureMasks)):
        if structureMasks[index]['status'] == 'completed':
            structureMasks[index]['mask']=puma.iss.get(structureMasks[index]['ID'])

    return structureMasks

def getCradleStatus(maxRetryNumber=10,retryInterval=1):
    cradleStatus = subprocess.check_output('puma cradle status').decode("utf-8")
    nTries = 0
    while ((cradleStatus is None) or ('running' not in cradleStatus)) and (nTries < maxRetryNumber):
            nTries += 1
            print("Cradle is not running. Restarting it.")
            time.sleep(retryInterval)
            subprocess.call('puma cradle stop',shell=True)
            subprocess.call('puma cradle start',shell=True)
            cradleStatus = subprocess.check_output('puma cradle status').decode("utf-8")

    return cradleStatus

def pumaStatusUpdate(ID,maxRetryNumber=10,retryInterval=1):
    successFlag = False
    nTries = 0
    while (not successFlag) and (nTries < maxRetryNumber):
        try:
            status = puma.calc.status(ID)
            status = str.lower([*status.keys()][0])
            successFlag = True
        except:
            print("Puma status update failed. Trying again in ",retryInterval, " s.")
            status = ""
            time.sleep(retryInterval)
            nTries = nTries + 1
    return status