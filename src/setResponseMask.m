function modelDetails = setResponseMask(modelDetails,GPC)
% Function Set_Dose_Mask writes out the plan details file with the dose
% mask information included, uploads this dose grid to thinknode, obtains
% the TN ID and sets the next dose grid ID to this.
% Inputs:
%       Plan_Details: The Plan_Details structure containing the patient and
%           plan details
%       PCE: The PCE object containing the dose mask
% Outputs:
%       Plan_Details: The Plan_Details structure containing the patient and
%           plan details, updated with the new dose grid ID.

% Save the response mask
savejson('',GPC.details.mask,'FileName',[modelDetails.modelDirectory,'Calculation_Files',...
    filesep,'responseMask',modelDetails.modelDirectory,'.json'],'SingletCell',1,'SingletArray',0);

% Upload the dose mask to thinknode and get the ID for it
status = 1;
while (status ~= 0)
    [status,cmdout] = system(['python ',modelDetails.scriptDirectory, 'uploadResponseMask.py ',...
        modelDetails.modelDetailsFile,' ',...
        modelDetails.modelDirectory,'calculationFiles',filesep,'responseMask',...
        modelDetails.modelDirectory,'.json']);
    disp(cmdout);
    if (status~=0)
        warning('GPC:obtainResponseMask:setResponseMask:ResponseMaskUploadFailed',...
            'Uploading response mask to Thinknode failed with code %d.',status);
    end
end

% Read back the updated plan details file with the new dose grid ID
modelDetails = loadjson(modelDetails.modelDetailsFile);

end