function y=legendrDer(x,o)
%legendrDer returns the value of the derivative of the o-th order
% legendre polynomial
% 1. INPUT: 
%   - x: vector of abscissas (N x 1, double)
%   - o: vector of Legendre polynomial orders (N x 1, integer)
% 2. OUTPUT:
%   - y: matrix of the derivative values at x for orders in o
% 3. DESCRIPTION:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

if (~iscolumn(x))
    x=x';
end
N = length(x);

switch max(o)
    case 0
        p = zeros(N,1);
    case 1
        p = [zeros(N,1) ones(N,1)];
    otherwise
        p = zeros(N,max(o)+1);
        p(:,2) = 1;
        p(:,3:end) = ((x*ones(1,max(o)-1)).*legendr(x,2:max(o))-legendr(x,(2:max(o))-1)).*...
            (1./(x.^2-1)*(2:max(o)));
end

y = p(:,o+1);

end