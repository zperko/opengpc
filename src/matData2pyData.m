function pyData = matData2pyData(matData)

pyData = mat2py(matData);

end

function pyData = mat2py(matData)

switch class(matData)
    case 'cell'
        for i = 1:numel(matData)
            matData{i} = mat2py(matData{i});
        end
    case 'struct'
        for i = 1:numel(matData)
            fields = fieldnames(matData(i));
            for j=1:numel(fields)
                matData(i).(fields{j}) = mat2py(matData(i).(fields{j}));
            end
        end
end
pyData = mat2pyConversion(matData);

end

function pyData = mat2pyConversion(matData)

switch class(matData)
    case 'char'
        if isrow(matData)
            pyData = py.str(matData);
        else
            pyData = py.str(matData');
        end
    case {'double','single','int8','int16','int32','int64','uint8','uint16','uint32','uint64','cell'}
        dataSize = size(matData);
        if numel(dataSize)>2
            error('matData2pyData:mat2pyConversion:multiDimensionalArra',...
                'Only 1 and 2 dimensional arrays allowed');
        elseif all(dataSize>1)
            pyData = py.list(cellfun(@py.list,num2cell(matData,2),...
                'UniformOutput',false)');
        elseif any(dataSize>1)
            if iscolumn(matData)
                pyData = py.list(matData');
            else
                pyData = py.list(matData);
            end
        else
            switch class(matData)
                case {'double','single'}
                    pyData = py.float(matData);
                case {'int8','int16','int32','int64','uint8','uint16','uint32','uint64'}
                    pyData = py.int(matData);
                case 'cell'
                    pyData = py.list(matData);
            end
        end
    case 'struct'
        dataSize = size(matData);
        if numel(dataSize)>2
            error('matData2pyData:mat2pyConversion:multiDimensionalArra',...
                'Only 1 and 2 dimensional arrays allowed');
        elseif all(dataSize)>1
            matData = num2cell(matData,2);
            for i = 1:numel(matData)
                matData{i} = num2cell(matData{i},1);
                matData{i} = cellfun(@py.dict,matData{i},'UniformOutput',false);
            end
            pyData = py.list(cellfun(@py.list,matData,'UniformOutput',false)');
        elseif any(dataSize)>1
            if iscolumn(matData)
                matData = cellfun(@py.dict,num2cell(matData',1),'UniformOutput',false);
            else
                matData = cellfun(@py.dict,num2cell(matData,1),'UniformOutput',false);
            end
            pyData = py.list(matData);
        else
            pyData = py.dict(matData);
        end
    otherwise
        pyData = matData;
end
end