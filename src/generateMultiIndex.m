function multiIndex=generateMultiIndex(dim,order,flag)
%generateMultiIndex generates a matrix of multi-indices for N
% variables with a maximum order of order. 
% 1. INPUT:
%   - dim: The number of variables (integer)
%   - order: The maximum order (integer)
%   - flag: 'all' for all indices, 'highest' for only the highest level
%   (string)
% 2. OUTPUT:
%   - multiIndex: The matrix of multi-indices (matrix, integer)
% 3. DESCRIPTION:
%   The function uses a loopping that is the fastest way I have found. All
%   more elegant ways are slower

%% COPYRIGHT:
% Zoltan Perko & Luca Gilli, TU Delft
% Date:2018.11.04.

%% FUNCTION DEFINITION
% Compute the number of terms 
P=(factorial(dim+order)/(factorial(order)*factorial(dim)))-1;
P=round(P);

multiIndex=zeros(P+1,dim);
MI2=multiIndex;
if (order>0)
    % N first order
    multiIndex(2:dim+1,:)=eye(dim);
    MI2(2:dim+1,:)=eye(dim);
    
    P=dim+1;
    r=zeros(dim,order);
    r(:,1)=ones(dim,1);
    
    for i=2:order
        L=P;
        r(:,i)=cumsum(r(end:-1:1,i-1));
        r(:,i)=r(end:-1:1,i);
        for j=1:dim
            MI2(P+1:P+r(j,i),:) = MI2(L-r(j,i)+1:L,:) + ...
                [zeros(r(j,i),j-1) ones(r(j,i),1) zeros(r(j,i),dim-j)];
            for k=L-r(j,i)+1:L
                P=P+1;
                multiIndex(P,:)=multiIndex(k,:);
                multiIndex(P,j)=multiIndex(P,j)+1;
            end
        end
    end
    
    switch(flag)
        case 'all'
        case 'highest'
            multiIndex=multiIndex(end-nchoosek(dim+order-1,order)+1:end,:);
        otherwise
            error('openGPC:generateMultiIndex:IncorrectFlag',...
                ['Incorrect flag selected! flag must be either ''all'' or ',...
                '''highest''!']);
    end
end

end