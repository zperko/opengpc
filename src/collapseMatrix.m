function R=collapseMatrix(R,dim)
%collapseMatrix removes identical lines from matrix R, while sums the dim+1
%column. 
% 1. INPUT:
%   - R: Matrix (M X N, double)
%   - dim: The column number up to which the comparison should be done
%   (integer)
% 2. OUTPUT:
%   - R: The matrix with removed duplicate lines and summed dim+1 column 
% 3. DESCRIPTION:
%   The rows of R are compared for 1:dim, if rows j and j+1 are identical, 
%   R(j,dim+1) becomes R(j,dim+1)+R(j+1,dim+1)

%% COPYRIGHT:
% Zoltan Perko & Luca Gilli, TU Delft
% Date: 2018.11.04.

%% FUNCTION DEFINITION

j=1;
for i=1:length(R(:,1))-1 
   if all(R(j,1:dim)==R(j+1,1:dim))
       R(j,dim+1)=R(j,dim+1)+R(j+1,dim+1);
       R(j+1,:)=[];
   else
       j=j+1;        
   end
end

end