function ModelDetails = convertResponseMasks(GPC,ModelDetails)
%convertResponseMasks converts response masks to masked GPC masks
% 1. INPUT:
%   - GPC: The GPC structure containing the dose mask on the full grid
%   - ModelDetails: The ModelDetails structure containing the response
%   masks on the full grid
% 2. OUTPUT:
%   - ModelDetails: The updated ModelDetails structure containing the
%   response masks on the masked grid

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2019.09.18.

%% FUNCTION DEFINITION

if isfield(ModelDetails.nominalModelDetails,'structureMasks')   
    for i=1:numel(ModelDetails.nominalModelDetails.structureMasks)        
        structureMask = zeros(size(GPC.Details.mask));
        structureMask(GPC.Details.mask) = 1:sum(GPC.Details.mask);
        ModelDetails.nominalModelDetails.structureMasks{i}.maskGPCIndices = ...
            structureMask(ModelDetails.nominalModelDetails.structureMasks{i}.maskIndices);
        if any(ModelDetails.nominalModelDetails.structureMasks{i}.maskGPCIndices==0)
            warning('openGPC:convertResponseMasks:SomeStructureIndicesCutInGPC',...
                ['Some structure indices are not present in GPC due to cutoff ',...
                '(response below cutoff value in all planning scenarios']);
            missingIndices = ModelDetails.nominalModelDetails.structureMasks{i}.maskGPCIndices == 0;
            ModelDetails.nominalModelDetails.structureMasks{i}.maskGPCIndices(missingIndices) = [];
            ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights(missingIndices) = [];
        end
        ModelDetails.nominalModelDetails.structureMasks{i}.cutoffWeight = ...
            ModelDetails.nominalModelDetails.structureMasks{i}.totalWeight - ...
            sum(ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights);
    end
else
    warning('openGPC:convertResponseMasks:NoMaskPresent',...
        'Response mask not present in ModelDetails.');
end

end
