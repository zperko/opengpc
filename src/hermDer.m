function y=hermDer(x,o)
%hermDer calculates the value of the derivative of the oth Hermite
% polynomial at abscissas x: He_o'(x)
% 1. INPUT:
%   - x: vector of abscissas (N x 1, double)
%   - o: vector of Hermite polynomial orders (N x 1, integer)
% 2. OUTPUT:
%   - y: matrix with the derivative of polynomials at x for orders in o
% 3. Description:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

N=length(x);

switch max(o)
    case 0
        p=zeros(N,1);
    case 1
        p=[zeros(N,1) ones(N,1)];
    otherwise
        p=zeros(N,max(o)+1);
        p(:,2) = 1;
        p(:,3:end) = herm(x,(2:max(o))-1).*(ones(N,1)*(2:max(o)));
end

y=p(:,o+1);

end