function Combinations = makeCombinations(variableCell)
%makeCombinations returns all combinations of the elements in variableCell.
% 1. INPUT:
%   - variableCell: A cell containing arrays with the possible values 
%           of the individual variables.
% 2. OUTPUT:
%   - combinations: A matrix with all possbile combinations for the
%       values in variableCell
% 3. DESCRIPTION:

%% COPYRIGHT
% This is modified version of allcomb version 2.1 (feb 2011) (c) Jos van 
% der Geest, email: jos@jasen.nl. 
% Modified by: Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

dim = length(variableCell);
% To make the last variable change the fastest
indexOrder = dim:-1:1;
[Combinations{indexOrder}]=ndgrid(variableCell{indexOrder});
Combinations = reshape(cat(dim+1,Combinations{:}),[],dim);
end