classdef OrthogonalPolynomClass < handle
    properties (SetAccess = protected, GetAccess = public)
        type
        order
    end
    methods
        function Polynom = OrthogonalPolynomClass(type,order)
            % constructs an orthogonal polynom class of order
            if (nargin == 2) && (length(order)>1)
                Polynom(length(order),1) = OrthogonalPolynomClass(type,order(end));
                if ~iscell(type)
                    type = {type};
                end
                for i=1:length(order)
                    Polynom(i,1).order = order(i);
                    Polynom(i,1).type = type;
                end
            end
        end
        
        function Y = value(Pol,x)
            % calculates the polynom value at x
            switch(Pol.type{1})
                case 'hermite'
                    Y = herm(x,Pol.order);
                case 'legendre'
                    Y = legendr(x,Pol.order);
                case 'laguerre'
                    Y = laguerre(x,Pol.order);
                otherwise
                    error('openGPC:OrthogonalPolynom:value:InvalidPolType',...
                        'Invalid polynomial type.');
            end
        end
        
        function Y = derivativeValue(Pol,x)
            % calculate the value of the derivative at x
            switch(Pol.type{1})
                case 'hermite'
                    Y = hermDer(x,Pol.order);
                case 'legendre'
                    Y = legendrDer(x,Pol.order);
                case 'laguarre'
                    Y = laguarreDer(x,Pol.order);
                otherwise
                     error('openGPC:OrthogonalPolynom:derivativeValue:InvalidPolType',...
                        'Invalid polynomial type.');
            end
        end
    end
    
end