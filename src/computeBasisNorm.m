function [I] = computeBasisNorm(multiIndex,polType)
%computeBasisNorm calculates the norm of the basis elements of a GPC
% 1. INPUT: 
%   - multiIndex: the multi-indexes describing the basis elements
%   - polType: a cell containing the type of the polynomials in 
%        the different directions
% 2. OUTPUT: I: a vector containing the norms
% 3. DESCRIPTION

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

if isempty(polType)
    I=1;
else
    dim=length(multiIndex(1,:));
    I=ones(length(multiIndex(:,1)),1);
    
    for i=1:length(I)
        for j=1:dim
            switch lower(polType{j})
                case 'hermite'
                    I(i)=I(i)*factorial(double(multiIndex(i,j)));
                case 'legendre'
                    I(i)=I(i)*2/(2*double(multiIndex(i,j))+1)/2;
                case 'laguerre'
                    I(i)=I(i)*1;
            end
        end
    end
end

end