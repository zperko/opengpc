classdef CubatureClass < QuadratureClass
    properties (SetAccess = protected, GetAccess = public)
        multiIndex  % The multi-index for the cubature
    end
    methods
        function Cubature = CubatureClass(type,QuadratureSet,multiIndices)
            if nargin<3 && length(dbstack) == 1
                error('openGPC:CubatureClass:NotEnoughtInputs',...
                    ['Not enough input arguments, quadrature types, ',...
                    'QuadratureSet and multiIndices are needed.']);
            elseif nargin == 3
                Cubature(length(multiIndices(:,1)),1) = CubatureClass(type,QuadratureSet);
                
                cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));
                quadratureTypeIndices = zeros(1,length(Cubature(1,1).type));
                for i=1:length(type)
                    logicalCells = cellfun(cellfind(type{i}),[QuadratureSet(1,:).type]);
                    quadratureTypeIndices(1,i)=find(logicalCells == 1);
                end
                
                for i=1:length(multiIndices(:,1))
                    quadratureSetIndices = sub2ind(size(QuadratureSet),multiIndices(i,:),...
                        quadratureTypeIndices);
                    Cubature(i,1).type = type;
                    Cubature(i,1).comment = cell(1,length(Cubature(1,1).type));
                    for j=1:length(quadratureSetIndices)
                        Cubature(i,1).type(j) = QuadratureSet(quadratureSetIndices(j)).type;
                        Cubature(i,1).comment(j) = QuadratureSet(quadratureSetIndices(j)).comment;
                    end
                    
                    Cubature(i,1).level = sum(multiIndices(i,:)) - length(multiIndices(i,:)) + 1;
                    Cubature(i,1).multiIndex = multiIndices(i,:);
                    abscissaCell = cell(length(multiIndices(i,:)),1);
                    weightCell = cell(length(multiIndices(i,:)),1);
                    for j=1:length(multiIndices(1,:))
                        abscissaCell{j}=QuadratureSet(quadratureSetIndices(j)).abscissas;
                        weightCell{j} = QuadratureSet(quadratureSetIndices(j)).weights;
                    end
                    Cubature(i,1).abscissas = makeCombinations(abscissaCell);
                    Cubature(i,1).weights = prod(makeCombinations(weightCell),2);
                end
                
            end
        end
               
        function SumCubature = plus(C1,C2)
            for i=1:length(C1.type)
                if strcmp(C1.type{i},C2.type{i})~=1
                    error('openGPC:CubatureClass:TypeMismatch',...
                        'Cubature types do not match.');
                end
            end
            SumCubature = CubatureClass(C1.type);
            SumCubature.type = C1.type;
            SumCubature.comment = cell(1,length(C1.type));
            for i=1:length(C1.type)
                SumCubature.comment(i) = {['(',C1.comment{i},')+(',C2.comment{i},')']};
            end
            
            SumCubature.level = max(C1.level,C2.level);
            abscissas = [C1.abscissas; C2.abscissas];
            weights = [C1.weights; C2.weights];
            for i=1:length(C1.type)
                [abscissas, rowIndices] = sortrows(abscissas,i);
                weights = weights(rowIndices);
            end
            R = collapseMatrix([abscissas, weights],length(C1.type));
            R(R(:,end)==0,:)=[];
            SumCubature.abscissas = R(:,1:end-1);
            SumCubature.weights = R(:,end);
            SumCubature.multiIndex = [C1.multiIndex; C2.multiIndex];
        end
    end
    
    
end