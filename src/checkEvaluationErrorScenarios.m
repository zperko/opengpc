function GammaEvaluations = checkEvaluationErrorScenarios(ErrorScenarios,GPC,ModelDetails,alpha,gammaFlag,...
    Tolerances)
%checkEvaluationErrorScenarios makes some plots to check the accuracy of
%GPC for randomly sampled ErrorScenarios corresponding to confidence
%intervals alpha
% 1. INPUTS
%   - ErrorScenarios: cell with SampleClass objects containing the error
%   scenarios
%   - GPC: GPC class object for which the accuracy is evaluated.
%   - ModelDetails: The ModelDetails structure of openGPC
%   - alpha: vector of confidence levels
%   - gammaFlag: flag to indicate if gamma evaluation should be done
%   instead of pure dose comparison (logical, true)
%   - Tolerances: structure with doseTolerance (double, 0.1) and 
%       distanceToAgreement (double, 1) criterion in gamma evaluation
% 2. OUTPUTS:
%   - GammaEvaluations: A cell with SampleClass objects containing the
%   gamma evaluations for the error scnearios compared to the GPC
% 3. DESCRIPTION:

%% COPYRIGHT
% Zoltan Perko, TU Delft

%% FUNCTION DEFINITION
switch nargin
    case 4
        gammaFlag = true;
        Tolerances.doseTolerance=0.1;
        Tolerances.distanceToAgreement = 1;
    case 5
        Tolerances.doseTolerance = 0.1;
        Tolerances.distanceToAgreement = 1;
    case 6
    otherwise
        error('checkEvaluationErrorScenarios:NotEnoughInputs',...
            'Provide ErrorScenarios, GPC, ModelDetails and alpha values at least');
end

nResponses = ErrorScenarios{1}.nResponses;
nSamples = ErrorScenarios{1}.nSamples;

% Compute the dose values for the evaluation scenarios if they are
% not present
for i=1:numel(ErrorScenarios)
    if any(~contains(ErrorScenarios{i}.status,'fetched'))
        ErrorScenarios{i}.computeResponses(ModelDetails,1:nSamples);
    end
end

figure
for i=1:numel(ErrorScenarios)
    physicalAbscissas = ones(nSamples,1)*GPC.Details.means + (ones(nSamples,1)*GPC.Details.stds).*...
        ErrorScenarios{i}.abscissas;
    GPCresponses = GPC.value(physicalAbscissas);
    GPCresponses(GPCresponses<0) = 0;
    [a,b]=ecdf(sum(abs(GPCresponses - ErrorScenarios{i}.responses)<Tolerances.doseTolerance,2)./nResponses);
    plot(b,100*(1-a));
    hold on
end
set(get(gca,'ylabel'),'string','Scenario fraction');
set(get(gca,'xlabel'),'string',['Accepted voxel fraction ($\Delta D<',...
    num2str(Tolerances.doseTolerance), '$ Gy)'],'interpreter','Latex');
legend(num2str(convert2vector(alpha)'));

if isfield(ModelDetails.nominalModelDetails,'structureMasks')
    Colors = {'r','g','b','c','m','y','k','w'};
    for i=1:numel(ErrorScenarios)
        physicalAbscissas = ones(nSamples,1)*GPC.Details.means + (ones(nSamples,1)*GPC.Details.stds).*...
            ErrorScenarios{i}.abscissas;
        GPCresponses = GPC.value(physicalAbscissas);
        GPCresponses(GPCresponses<0) = 0;
        responses = ErrorScenarios{i}.responses;
        for j=1:nSamples
            if mod(j-1,5)==0
                figure
                hVisibility = 'on';
            else
                hVisibility = 'off';
            end
            legendText = cell(numel(ModelDetails.nominalModelDetails.structureMasks),1);
            for k=1:numel(ModelDetails.nominalModelDetails.structureMasks)
                if ~isempty(ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices)
                    [a,b]=histwc(responses(j,ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices),...
                        ModelDetails.nominalModelDetails.structureMasks{k}.maskWeights,250);
                else
                    b = [GPC.Details.cutoffValue max(responses(:))];
                    a=[1 0];
                end
                h = plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{k}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{k}.totalWeight,'linestyle','-',...
                    'color',Colors{1+mod(k-1,numel(Colors))},'marker','none','HandleVisibility',hVisibility);
                hold on
                if ~isempty(ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices)
                [a,b]=histwc(GPCresponses(j,ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices),...
                    ModelDetails.nominalModelDetails.structureMasks{k}.maskWeights,250);
                else
                    b = [GPC.Details.cutoffValue max(responses(:))];
                    a=[1 0];
                end
                plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{k}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{k}.totalWeight,'marker','.',...
                    'color',get(h,'color'),'linestyle','none','HandleVisibility',hVisibility);
                legendText{2*k-1,1}=ModelDetails.nominalModelDetails.structureMasks{k}.label;
                legendText{2*k,1}=[ModelDetails.nominalModelDetails.structureMasks{k}.label,'-GPC'];
            end
            if mod(j-1,5)==0
                legend(legendText);
            end
        end
    end
end

% Compute and plot the gamma values if requested
switch gammaFlag
    case true
        figure
        GammaEvaluations = cell(size(ErrorScenarios));
        for i=1:numel(ErrorScenarios)
            GammaEvaluations{i} = ErrorScenarios{i}.compareResponses(ModelDetails,...
                1:nSamples,GPC,Tolerances);
            [a,b] = ecdf(sum(GammaEvaluations{i}.responses<1,2)./nResponses);
            plot(b,100*(1-a));
            hold on
        end
        set(get(gca,'ylabel'),'string','Scenario fraction');
        set(get(gca,'xlabel'),'string',['Accepted voxel fraction ($\Delta D=',...
            num2str(Tolerances.doseTolerance), '$ Gy, $\Delta d=',num2str(Tolerances.distanceToAgreement)',...
            '$ mm)'],'interpreter','Latex');
        legend(num2str(convert2vector(alpha)'));
    case false
        GammaEvaluations = {};
end

end