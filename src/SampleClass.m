classdef SampleClass < handle
    properties (SetAccess = protected, GetAccess = public)
        abscissas    % The abscissas to calculate (nSamples X dim, double)
        responses    % The model outputs (nSamples X nResponses, double)
        nSamples     % The number of calculations (integer)
        nResponses   % The number of responses (integer)
        Details      % Additional details (structure)
        IDs          % Unique IDs for the calculations (nSamples, cell)
        status       % status flags for the calculations (nSamples, cell)
        subProcesses % Python objects for started subprocesses (nSamples, cell)
        fileNames    % Full path of files containing responses (nSamples, cell)
    end
    
    methods
        function Sample = SampleClass(abscissas,Details)
            % Class constructor
            Sample.abscissas = abscissas;
            Sample.nSamples = length(abscissas(:,1));
            Sample.responses = zeros(Sample.nSamples,1);
            Sample.nResponses = 1;
            Sample.Details = Details;
            Sample.IDs = cell(Sample.nSamples,1);
            Sample.IDs(1:Sample.nSamples,1)={''};
            Sample.status = cell(Sample.nSamples,1);
            Sample.status(1:Sample.nSamples,1)={'created'};
            Sample.subProcesses = cell(Sample.nSamples,1);
            Sample.fileNames = cell(Sample.nSamples,1);
        end
        
        function Sample = set(Sample,property,value,varargin)
            % sets the requested Sample property to value, potentially only
            % for certain calculations
            if isprop(Sample,property)
                if nargin==3
                    Sample.(property) = value;
                else
                    Sample.(property)(varargin{1},:) = value;
                end
            else
                warning('SampleClass:Set:PropertyNotFound',...
                    ['Requested property (',property,') not found in ',...
                    'SampleClass. Properties not changed.']);
            end
        end
        
        function value = get(Sample,property,varargin)
            % returns the requested Sample properties, potentially only for
            % certain calculations
            if isprop(Sample,property)
                if nargin == 2
                    value = Sample.(property);
                else
                    value = Sample.(property)(varargin{1},:);
                end
            else
                warning('openGPC:SampleClass:set:PropertyNotFound',...
                    ['Requested field (',property,') not found in Sample class.']);
                value = [];
            end
        end
                
        function Sample = enterResponses(Sample,responses,indices)
            % Fills the responses data field in Sample at indices. The
            % nResponses field is updated as needed
            if Sample.Details.masked
                % If masked, the provided responses can be the already
                % masked response (if blackboxModel or external model
                % provides it masked or in readResults it is masked to 
                % decrease memory costs), but also can be the still 
                % unmasked responses (if blackboxModel or external model 
                % does not mask or readResults only reads without masking)
                if length(responses(1,:)) == Sample.nResponses
                    Sample.responses(indices,1:Sample.nResponses) = responses;
                elseif length(responses(1,:)) == length(Sample.Details.mask)
                    Sample.responses(indices,1:Sample.nResponses) = ...
                        responses(:,Sample.Details.mask);
                else
                    error('openGPC:SampleClass:enterResponses:incompatibleResonseSizes',...
                        'Number of responses differs from both mask size and nResponses.');
                end
            elseif length(responses(1,:)) >= Sample.nResponses
                if length(responses(1,:)) > Sample.nResponses
                    Sample.nResponses=length(responses(1,:));
                end
                Sample.responses(indices,1:Sample.nResponses) = responses;
            elseif length(responses(1,:)) < Sample.nResponses
                error('openGPC:SampleClass:enterResponses:InvalidnResponsesChange',...
                    'Number of responses cannot decrease.');
            end
            
        end
        
        function Sample = submit(Sample,ModelDetails,indices)
            % submits calculations as a batch. We convert the abscissa
            % values to their proper physical values before 
            Sample.status(indices) = {'posting'};
            
            % For external calculations the data is passed through calling python scripts directly
            indicesToSubmit = indices;
            SampleStructure.abscissas = ones(length(indicesToSubmit),1)*Sample.Details.means + ...
                Sample.abscissas(indicesToSubmit,:).*(ones(length(indicesToSubmit),1)*Sample.Details.stds);
            SampleStructure.IDs = Sample.IDs(indicesToSubmit);
            SampleStructure.status = Sample.status(indicesToSubmit);
            SampleStructure.nSamples = int64(length(indicesToSubmit));
            SampleStructure.variableNames = Sample.Details.variableNames;
            
            % Submission done by directly calling python functions from the
            % externalCoupling.py module. From version Matlab 2018a can
            % only pass 1 by N vectors, so we convert every matrix into
            % tuples. 2019 can already handle arrays so this could be
            % changed in future versions or based on Matlab version.
            
            SampleStructure.abscissas = py.tuple(num2cell(...
                SampleStructure.abscissas,2)');
            SampleStructure.IDs = SampleStructure.IDs';
            SampleStructure.status = SampleStructure.status';
%             SampleStructure = matData2pyData(SampleStructure);
            successFlag = false;
            nTries = 0;
            while successFlag~=true && nTries < ModelDetails.maxExternalRetryNumber
                nTries = nTries + 1;
                pythonReturn = py.externalCoupling.submitCalculations(SampleStructure,ModelDetails);                
                SampleStructure = struct(pythonReturn);
                SampleStructure.nSamples = double(SampleStructure.nSamples);
                SampleStructure.IDs = cell(SampleStructure.newIDs)';
                SampleStructure.status = cell(SampleStructure.newStatus)';
                SampleStructure.subProcesses = cell(SampleStructure.newSubProcesses)';
                SampleStructure.fileNames = cell(SampleStructure.newFileNames)';
                for i=1:SampleStructure.nSamples
                    SampleStructure.IDs{i} = char(SampleStructure.IDs{i});
                    SampleStructure.status{i} = char(SampleStructure.status{i});
                    SampleStructure.fileNames{i} = char(SampleStructure.fileNames{i});
                end
%                 SampleStructure = pyData2matData(pythonReturn);
                Sample.set('IDs',SampleStructure.IDs,indicesToSubmit);
                Sample.set('status',SampleStructure.status,indicesToSubmit);
                Sample.set('subProcesses',SampleStructure.subProcesses,indicesToSubmit);
                Sample.set('fileNames',SampleStructure.fileNames,indicesToSubmit);
                
                failed = find(~contains(SampleStructure.status,'calculating'));
                if isempty(failed)
                    successFlag = 1;
                else
                    warning('openGPC:SampleClass:submitCalculations:submitFailed',...
                        ['Submitting calculations failed for some indices with the codes below. Trying again in'...
                        ' %d seconds.'],ModelDetails.externalRetryInterval);
                    for i=1:length(failed)
                        fprintf('Index: %d, status: %s\n',indicesToSubmit(failed(i)),...
                            Sample.status{indicesToSubmit(failed(i))});
                    end
                    pause(ModelDetails.externalRetryInterval);
                    indicesToSubmit = indicesToSubmit(failed);
                    SampleStructure.IDs = SampleStructure.IDs(failed)';
                    SampleStructure.status = SampleStructure.status(failed)';
                    SampleStructure.subProcesses = SampleStructure.subProcesses(failed)';
                    SampleStructure.fileNames = SampleStructure.fileNames(failed)';
                    SampleStructure.nSamples = int64(length(indicesToSubmit));
%                     SampleStructure = matData2pyData(SampleStructure);
                    continue
                end
            end
            if nTries == ModelDetails.maxExternalRetryNumber
                error('openGPC:SampleClass:submit:maxExternalRetryNumberReached',...
                    'Maximum external retry number reached, submitting calculations failed');
            end
        end
        
        function Sample = requestStatusUpdate(Sample,ModelDetails,indices)
            % requests and updates the status of the calculations with
            % indices
            indicesToUpdate = indices;
            SampleStructure.nSamples = int64(length(indicesToUpdate));
            SampleStructure.IDs = Sample.IDs(indicesToUpdate)';
            SampleStructure.status = Sample.status(indicesToUpdate)';
            SampleStructure.subProcesses = Sample.subProcesses(indicesToUpdate)';

            successFlag = false;
            nTries = 0;
            while ~successFlag && nTries < ModelDetails.maxExternalRetryNumber
                nTries = nTries + 1;
                pythonReturn = py.externalCoupling.requestStatusUpdate(...
                    SampleStructure,ModelDetails);
                SampleStructure = struct(pythonReturn);
                SampleStructure.nSamples = double(SampleStructure.nSamples);
                SampleStructure.IDs = cell(SampleStructure.IDs)';
                SampleStructure.status = cell(SampleStructure.newStatus)';
                SampleStructure.subProcesses = cell(SampleStructure.subProcesses)';
                for i=1:SampleStructure.nSamples
                    SampleStructure.IDs{i} = char(SampleStructure.IDs{i});
                    SampleStructure.status{i} = char(SampleStructure.status{i});
                end
                Sample.set('status',SampleStructure.status,indicesToUpdate);
                
                failed = find(~(contains(SampleStructure.status,'calculating') | ...
                        contains(SampleStructure.status,'completed') | ...
                        contains(SampleStructure.status,'failed')));
                if isempty(failed)
                    successFlag = 1;
                else
                    warning('openGPC:SampleClass:requestStatusUpdate:statusUpdateFailed',...
                        ['Status update failed for some indices with the codes below. Trying again in'...
                        ' %d seconds.'],ModelDetails.externalRetryInterval);
                    for i=1:length(failed)
                        fprintf('Index: %d, status: %s\n',indicesToUpdate(failed(i)),...
                            Sample.status{indicesToUpdate(failed(i))});
                    end
                    pause(ModelDetails.externalRetryInterval);
                    indicesToUpdate = indicesToUpdate(failed);
                    SampleStructure.nSamples = int64(length(indicesToUpdate));
                    SampleStructure.IDs = SampleStructure.IDs(failed)';
                    SampleStructure.status = SampleStructure.status(failed)';
                    SampleStructure.subProcesses = SampleStructure.subProcesses(failed)';
                    continue
                end
                
            end
            if nTries == ModelDetails.maxExternalRetryNumber
                error('openGPC:SampleClass:requestStatusUpdate:maxExternalRetryNumberReached',...
                    'Maximum external retry number reached, requesting status update failed');
            end
        end
        
        function Sample = readResults(Sample,ModelDetails,indices)
            % collects the results of the calculations with the specific
            % indices
            indicesToRead = indices;
            SampleStructure.IDs = Sample.IDs(indicesToRead)';
            SampleStructure.status = Sample.status(indicesToRead)';
            SampleStructure.nSamples = int64(length(indicesToRead));
            SampleStructure.fileNames = Sample.fileNames(indicesToRead)';
            SampleStructure.responses = num2cell(Sample.responses(indicesToRead,:),2)';
            SampleStructure.Details = Sample.Details;
            
            successFlag = 0;
            nTries = 0;
            while ~successFlag && nTries < ModelDetails.maxExternalRetryNumber
                nTries = nTries + 1;
                pythonReturn = py.externalCoupling.readResults(...
                    SampleStructure,ModelDetails);
                SampleStructure = struct(pythonReturn);
                SampleStructure.nSamples = double(SampleStructure.nSamples);
                SampleStructure.status = cell(SampleStructure.newStatus)';
                SampleStructure.responses = zeros(SampleStructure.nSamples,Sample.nResponses);
                SampleStructure.responses = cell(SampleStructure.newResponses)';
                newResponses = zeros(SampleStructure.nSamples,length(SampleStructure.responses{1}));
                for i=1:SampleStructure.nSamples
                    SampleStructure.status{i} = char(SampleStructure.status{i});
                    if strcmp(SampleStructure.status{i},'fetched')
                        newResponses(i,:) = cell2mat(cell(SampleStructure.responses{i}));
                    end
                end
                Sample.set('status',SampleStructure.status,indicesToRead);
                Sample.enterResponses(newResponses,indicesToRead);
                failed = find(contains(SampleStructure.status,'completed'));
                if isempty(failed)
                    successFlag = 1;
                else
                    warning('openGpC:SampleClass:readResults:readResultsFailed',...
                        ['Reading results failed for some indices. Trying again in ',...
                        '%d seconds.'],ModelDetails.externalRetryInterval);
                    for i=1:length(failed)
                        fprintf('Index: %d, status: %s\n',indicesToRead(failed(i)),...
                            Sample.status{indicesToRead(failed(i))});
                    end
                    pause(ModelDetails.externalRetryInterval);
                    indicesToRead = indicesToRead(failed);
                    SampleStructure.IDs = SampleStructure.IDs(failed)';
                    SampleStructure.status = SampleStructure.status(failed)';
                    SampleStructure.fileNames = SampleStructure.fileNames(failed)';
                    SampleStructure.nSamples = int64(length(indicesToRead));
                    SampleStructure.Details = Sample.Details;
                    SampleStructure=rmfield(SampleStructure,'newResponses');
                    SampleStructure=rmfield(SampleStructure,'newStatus');
                    continue
                end  
            end
            if nTries == ModelDetails.maxExternalRetryNumber
                error('openGPC:SampleClass:readResults:maxExternalRetryNumberReached',...
                    'Maximum external retry number reached, reading results failed');
            end
        end
        
        function Sample = writeResults(Sample,ModelDetails,indices)
            % writes the results of the calculations with the specific
            % indices
            Sample.set('status',cellstr(char(ones(numel(indices),1)*'writing')),indices);
            indicesToWrite = indices;
            SampleStructure.IDs = Sample.IDs(indicesToWrite)';
            SampleStructure.status = Sample.status(indicesToWrite)';
            SampleStructure.nSamples = int64(length(indicesToWrite));
            SampleStructure.fileNames = Sample.fileNames(indicesToWrite)';
            SampleStructure.responses = num2cell(Sample.responses(indicesToWrite,:),2)';
            SampleStructure.Details = Sample.Details;
            
            successFlag = 0;
            nTries = 0;
            while ~successFlag && nTries < ModelDetails.maxExternalRetryNumber
                nTries = nTries + 1;
                pythonReturn = py.externalCoupling.writeResults(...
                    SampleStructure,ModelDetails);
                SampleStructure = struct(pythonReturn);
                SampleStructure.nSamples = double(SampleStructure.nSamples);
                SampleStructure.status = cell(SampleStructure.newStatus)';
                SampleStructure.IDs = cell(SampleStructure.newIDs)';
                for i=1:SampleStructure.nSamples
                    SampleStructure.IDs{i} = char(SampleStructure.IDs{i});
                    SampleStructure.status{i} = char(SampleStructure.status{i});
                end
                Sample.set('status',SampleStructure.status,indicesToWrite);
                Sample.set('IDs',SampleStructure.IDs,indicesToWrite);
                
                failed = find(contains(SampleStructure.status,'writing'));
                if isempty(failed)
                    successFlag = 1;
                else
                    warning('openGpC:SampleClass:writeResults:writeResultsFailed',...
                        ['Writing results failed for some indices. Trying again in ',...
                        '%d seconds.'],ModelDetails.externalRetryInterval);
                    for i=1:length(failed)
                        fprintf('Index: %d, status: %s\n',indicesToWrite(failed(i)),...
                            Sample.status{indicesToWrite(failed(i))});
                    end
                    pause(ModelDetails.externalRetryInterval);
                    indicesToWrite = indicesToWrite(failed);
                    SampleStructure.nSamples = int64(length(indicesToWrite));
                    SampleStructure.IDs = SampleStructure.IDs(indicesToWrite)';
                    SampleStructure.status = SampleStructure.status(indicesToWrite)';
                    SampleStructure.fileNames = SampleStructure.fileNames(indicesToWrite)';
                    SampleStructure.responses = num2cell(Sample.responses(indicesToWrite,:),2)';
                    SampleStructure=rmfield(SampleStructure,'newStatus');
                    SampleStructure=rmfield(SampleStructure,'newIDs');
                    continue
                 end
            end
            
        end
        
        function Sample = computeResponses(Sample,ModelDetails,indices)
            % computes the model responses for the requested indices
            if (~isempty(indices))
                if (ModelDetails.externalModel)
                    switch (ModelDetails.parallelMode)
                        case 'serial'                           
                            for i=1:length(indices)
                                Sample.submit(ModelDetails,indices(i));
                                finished = false;
                                while ~finished
                                    pause(ModelDetails.statusUpdateInterval);
                                    Sample.requestStatusUpdate(ModelDetails,indices(i));
                                    if strcmp('completed',Sample.status(indices(i)))
                                        Sample.readResults(indices(i));
                                        finished = true;
                                    end
                                end
                            end
                        case 'modelParallel'
                            % submit the calculations and get the calculation IDs
                            Sample.submit(ModelDetails,indices);
                            % Wait till all calculations are done and read results
                            % as they arrive
                            finished = false;
                            runningIndices = indices;
                            while ~finished
                                pause(ModelDetails.statusUpdateInterval);
                                % Request a status update for all calculations that are still running
                                Sample.requestStatusUpdate(ModelDetails,runningIndices);
                                % Check for failed calculations, modify
                                % their abscissas and resubmit them.
                                % Incredibly ugly but necessary for
                                % Thinknode to work properly.
                                failedIndices = runningIndices(strcmp('failed',Sample.status(runningIndices)));
                                if ~isempty(failedIndices)
                                    failedAbscissas = Sample.get('abscissas',failedIndices);
                                    for i=1:numel(failedAbscissas(:,1))
                                        dims = find(abs(failedAbscissas(i,:))~=0);
                                        failedAbscissas(i,dims(1)) = failedAbscissas(i,dims(1))*1.00000001;
                                    end
                                    Sample.set('abscissas',failedAbscissas,failedIndices);
                                    Sample.submit(ModelDetails,failedIndices);
                                end
                                % Read the results for calculations that have finished since the last
                                % update
                                indicesToRead=runningIndices(strcmp('completed',Sample.status(runningIndices)));
                                if ~isempty(indicesToRead)
                                    Sample.readResults(ModelDetails,indicesToRead);
                                    % Delete the indices of calculations that have successfully been read
                                    [membership,location]=ismember(indicesToRead(...
                                        strcmp('fetched',Sample.status(indicesToRead))),runningIndices);
                                    runningIndices(location(membership))=[];
                                end
                                
                                if (isempty(runningIndices))
                                    finished = true;
                                end
                            end
                        otherwise
                            error('openGPC:SampleClass:InvalidParallelMode',...
                                ['parallelMode invalid, only ''serial'', ',...
                                '''matlabParallel'' and ''modelParallel'' ',...
                                'allowed.']);
                    end
                else
                    switch (ModelDetails.parallelMode)
                        case 'serial'
                            for i=1:length(indices)
                                Sample.status(indices(i)) = {'calculating'};
                                [newResponses,Sample.IDs(indices(i)),...
                                    Sample.status(indices(i))] = blackboxModel(...
                                    Sample.abscissas(indices(i),:),ModelDetails);
                                Sample.enterResponses(newResponses,indices(i));
                            end
                            
                        case 'modelParallel'
                            [newResponses,Sample.IDs(indices),...
                                Sample.status(indices)]=blackboxModel(...
                                Sample.abscissas(indices,:),ModelDetails);
                            Sample.enterResponses(newResponses,indices);
                            
                        case 'matlabParallel'
                            newResponses = cell(length(indices),1);
                            newIDs = cell(length(indices),1);
                            newStatus = cell(length(indices),1);
                            newAbscissas = Sample.abscissas(indices,:);
                            parfor i=1:length(indices)
                                [newResponses{i,1},newIDs(i),newStatus(i)] = ...
                                    blackboxModel(newAbscissas(i,:),ModelDetails);
                            end
                            Sample.enterResponses(cell2mat(newResponses),indices);
                            Sample.IDs(indices)=newIDs;
                            Sample.status(indices)=newStatus;
                        otherwise
                            error('openGPC:SampleClass:InvalidParallelMode',...
                                ['parallelFlag invalid, only ''serial'', ',...
                                '''matlabParallel'' and ''modelParallel'' ',...
                                'allowed.']);
                    end
                end 
            end
        end
        
        function GPCSample = compareResponses(Sample,ModelDetails,indices,GPC,Tolerances)
            % compares the GPC responses to the true values by external
            % comparison method
            if (~isempty(indices))
                GPCSample = SampleClass(Sample.abscissas,Sample.Details);
                GPCSample.set('nResponses',Sample.nResponses);
                physicalAbscissas = ones(length(indices),1)*GPC.Details.means + ...
                    (ones(length(indices),1)*GPC.Details.stds).*Sample.abscissas;
                GPCResponses = GPC.value(physicalAbscissas);
                GPCResponses(GPCResponses<0) = 0;
                GPCSample.enterResponses(GPCResponses,indices);
                if (ModelDetails.externalModel)
                    switch (ModelDetails.parallelMode)
                        case 'serial'
                            for i=1:length(indices)
                                GPCSample.writeResults(ModelDetails,indices(i));
                                GPCSample.submitComparisonCalculation(ModelDetails,indices(i),...
                                    Sample.IDs(indices(i)),Tolerances);
                                finished = false;
                                while ~finished
                                    pause(ModelDetails.statusUpdateInterval);
                                    GPCSample.requestStatusUpdate(ModelDetails,indices(i));
                                    if strcmp('completed',GPCSample.status(indices(i)))
                                        GPCSample.readResults(indices(i));
                                        finished = true;
                                    end
                                end
                            end
                        case 'parallel'
                            case 'modelParallel'
                                GPCSample.writeResults(ModelDetails,indices);
                                GPCSample.submitComparisonCalculation(ModelDetails,indices,...
                                    Sample.IDs(indices),Tolerances);
                                finished = false;
                                runningIndices = indices;
                                while ~finished
                                    pause(ModelDetails.statusUpdateInterval);
                                    GPCSample.requestStatusUpdate(ModelDetails,runningIndices);
                                    % Check for failed calculations, modify
                                    % their tolerance and resubmit them.
                                    % Need to reupload GPC dose then
                                    % resubmit comparison calculation
                                    failedIndices = runningIndices(strcmp('failed',GPCSample.status(runningIndices)));
                                    if ~isempty(failedIndices)
                                        Tolerances.doseTolerance = Tolerances.doseTolerance*1.00000001;
                                        disp(['Gamma evaluation calculations failed for indices ',failedIndices]);
                                        disp('Resubmitting them with modified tolerances:');
                                        disp(Tolerances);
                                        physicalAbscissas = ones(length(failedIndices),1)*GPC.Details.means + ...
                                            (ones(length(failedIndices),1)*GPC.Details.stds).*Sample.abscissas;
                                        GPCResponses = GPC.value(physicalAbscissas);
                                        GPCResponses(GPCResponses<0) = 0;
                                        GPCSample.enterResponses(GPCResponses,failedIndices);
                                        GPCSample.writeResults(ModelDetails,failedIndices);
                                        GPCSample.submitComparisonCalculation(ModelDetails,failedIndices,...
                                            Sample.IDs(failedIndices),Tolerances);
                                        disp('Gamma calculation resubmission finished.');
                                    end
                                
                                    indicesToRead=runningIndices(strcmp('completed',GPCSample.status(runningIndices)));
                                    if ~isempty(indicesToRead)
                                        GPCSample.readResults(ModelDetails,indicesToRead);
                                        % Delete the indices of calculations that have successfully been read
                                        [membership,location]=ismember(indicesToRead(...
                                            strcmp('fetched',GPCSample.status(indicesToRead))),runningIndices);
                                        runningIndices(location(membership))=[];
                                    end

                                    if (isempty(runningIndices))
                                        finished = true;
                                    end
                                end
                        otherwise
                            error('openGPC:SampleClass:InvalidParallelMode',...
                                ['parallelMode invalid, only ''serial'', ',...
                                '''matlabParallel'' and ''modelParallel'' ',...
                                'allowed.']);
                    end
                else
%                     switch (ModelDetails.parallelMode)
%                         case 'serial'
%                             for i=1:length(indices)
%                                 Sample.status(indices(i)) = {'calculating'};
%                                 [newResponses,Sample.IDs(indices(i)),...
%                                     Sample.status(indices(i))] = blackboxModel(...
%                                     Sample.abscissas(indices(i),:),ModelDetails);
%                                 Sample.enterResponses(newResponses,indices(i));
%                             end
%                             
%                         case 'modelParallel'
%                             [newResponses,Sample.IDs(indices),...
%                                 Sample.status(indices)]=blackboxModel(...
%                                 Sample.abscissas(indices,:),ModelDetails);
%                             Sample.enterResponses(newResponses,indices);
%                             
%                         case 'matlabParallel'
%                             newResponses = cell(length(indices),1);
%                             newIDs = cell(length(indices),1);
%                             newStatus = cell(length(indices),1);
%                             newAbscissas = Sample.abscissas(indices,:);
%                             parfor i=1:length(indices)
%                                 [newResponses{i,1},newIDs(i),newStatus(i)] = ...
%                                     blackboxModel(newAbscissas(i,:),ModelDetails);
%                             end
%                             Sample.enterResponses(cell2mat(newResponses),indices);
%                             Sample.IDs(indices)=newIDs;
%                             Sample.status(indices)=newStatus;
%                         otherwise
%                             error('openGPC:SampleClass:InvalidParallelMode',...
%                                 ['parallelFlag invalid, only ''serial'', ',...
%                                 '''matlabParallel'' and ''modelParallel'' ',...
%                                 'allowed.']);
%                     end
                end
            end
        end
        
        function submitComparisonCalculation(Sample,ModelDetails,indices,refIDs,Tolerances)
            % submits the calculations necessary to compare GPC responses
            % to true responses using external comparison method
            Sample.status(indices) = {'comparing'};
            
            % For external calculations the data is passed through calling python scripts directly
            indicesToCompare = indices;
            SampleStructure.IDs = Sample.IDs(indicesToCompare);
            SampleStructure.status = Sample.status(indicesToCompare);
            SampleStructure.nSamples = int64(length(indicesToCompare));            
            SampleStructure.IDs = SampleStructure.IDs';
            SampleStructure.status = SampleStructure.status';
            refIDs = refIDs';

            successFlag = false;
            nTries = 0;
            while successFlag~=true && nTries < ModelDetails.maxExternalRetryNumber
                nTries = nTries + 1;
                pythonReturn = py.externalCoupling.submitComparisonCalculations(SampleStructure,...
                    ModelDetails,refIDs,Tolerances);                
                SampleStructure = struct(pythonReturn);
                SampleStructure.nSamples = double(SampleStructure.nSamples);
                SampleStructure.IDs = cell(SampleStructure.newIDs)';
                SampleStructure.status = cell(SampleStructure.newStatus)';
                for i=1:SampleStructure.nSamples
                    SampleStructure.IDs{i} = char(SampleStructure.IDs{i});
                    SampleStructure.status{i} = char(SampleStructure.status{i});
                end
%                 SampleStructure = pyData2matData(pythonReturn);
                Sample.set('IDs',SampleStructure.IDs,indicesToCompare);
                Sample.set('status',SampleStructure.status,indicesToCompare);
                
                failed = find(~contains(SampleStructure.status,'calculating'));
                if isempty(failed)
                    successFlag = 1;
                else
                    warning('openGPC:SampleClass:submitCalculations:submitFailed',...
                        ['Submitting comparison calculations failed for some indices with the codes below. Trying again in'...
                        ' %d seconds.'],ModelDetails.externalRetryInterval);
                    for i=1:length(failed)
                        fprintf('Index: %d, status: %s\n',indicesToCompare(failed(i)),...
                            Sample.status{indicesToCompare(failed(i))});
                    end
                    pause(ModelDetails.externalRetryInterval);
                    indicesToCompare = indicesToCompare(failed);
                    SampleStructure.IDs = SampleStructure.IDs(failed)';
                    SampleStructure.status = SampleStructure.status(failed)';
                    SampleStructure.nSamples = int64(length(indicesToCompare));

                    continue
                end
            end
            if nTries == ModelDetails.maxExternalRetryNumber
                error('openGPC:SampleClass:submitComparisonCalculations:maxExternalRetryNumberReached',...
                    'Maximum external retry number reached, submitting calculations failed');
            end
        end
    end
end     