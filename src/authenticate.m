function ModelDetails = authenticate(ModelDetails)
%authenticate performs authenticate if necessary for running the external
%model
% 1. INPUT:
%   - ModelDetails: Structure with the details of the model for which the
%   GPC is built. Needed entries are:
%       - modelCase: the case number of the model (integer, 0 for user
%       model).
%       - externalModel: flag for signalling non-Matlab model (logical)
%       - Authenticity: Structure to store authenticity details:
%           - Valid: flag to signal authentication is not needed
%           - token: token needed to run external code
% 2. OUTPUT:
%   - modelDetails: The updated modelDetails structure containing the new
%   Authenticity.
% 3. DESCRIPTION: For now code is coupled to pumaCoupling
% 4. TODO:

%% COPYRIGHT: Zoltan Perko, TU Delft
% Date: 2019.07.23.

%% FUNCTION DEFINITION

if ModelDetails.externalModel
    if ~ModelDetails.Authenticity.valid
        activeToken = char(py.puma.token.get());
        if ~isempty(ModelDetails.Authenticity.token)
            if strcmp(ModelDetails.Authenticity.token,activeToken)
                ModelDetails.Authenticity.valid = true;
            else
                warning('openGPC:authenticate:TokenMismatch',...
                    'Given token %s differs from active token %s. Switching to active token.',...
                    ModelDetails.Authenticity.token,activeToken);
                ModelDetails.Authenticity.valid = true;
                ModelDetails.Authenticity.token = activeToken;
            end
        else
            try
                !puma token gen &
                disp('Waiting for 1 minute to arrange authentication. openGPC will continue automatically.')
                pause(60)
                disp('Continuing');
                ModelDetails.Authenticity.valid = true;
                ModelDetails.Authenticity.token = char(py.puma.token.get());
            catch
                error('openGPC:authenticate:AuthenticationFailed',...
                    'Authentication failed with error %s',token);
            end
        end
    end
else
    if ~ModelDetails.Authenticity.valid
        % Enter authentication method if needed
    end
end

assert(ModelDetails.Authenticity.valid,'openGPC:authenticate:InvalidAuthenticity',...
    'Authenticity could not be established. Check authentication in authenticate.m');
end 