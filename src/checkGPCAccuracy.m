function checkGPCAccuracy(GPC,IntegrationData,ModelDetails)
%checkGPCAccuracy performs some basic checks on the accuracy of the GPC
% 1. INPUT: 
%   - GPC: A GPC class object containined the GPC and its details
%   - IntegrationData: the IntegrationData structure of openGPC containing
%   the planning scenarios and all cubature points, with the corresponding
%   responses
%   - ModelDetails: The ModelDetails structure of openGPC
% 2. OUTPUT:
% 3. DESCRIPTION: Some rough estimates are made of the accuracy of the GPC
% by plotting: 
%   - The histogram of errors for all responses between the GPC and true
%   values among the scenarios used to build the GPC
%   - The histogram of maximum and mean response errors between the GPC and
%   the true values among the scenarios used to build the GPC.
%   - If samplingMethod is sparse or full, one and two dimensional 
%   dependences of some responses on the different variables and different 
%   pairs of variables together with the GPC predictions.
%   - If samplingMethod is lhs or random, predicted vs. true response
%   graphs of some responses.

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.10.25.

%% FUNCTION DEFINITION

if GPC.Details.masked
    mask = GPC.Details.mask;
else
    mask = true(size(GPC.coeffs(1,:)));
end
nSamples = IntegrationData.ErrorScenarios.All.nSamples;
nResponses = IntegrationData.ErrorScenarios.All.nResponses;
abscissas = IntegrationData.ErrorScenarios.All.abscissas;
physicalAbscissas = ones(nSamples,1)*GPC.Details.means + (ones(nSamples,1)*GPC.Details.stds).*...
    IntegrationData.ErrorScenarios.All.abscissas;
nDim = length(abscissas(1,:));

if nResponses > 100
    % Check planning scenario rough DVHs
    for i=1:length(IntegrationData.ErrorScenarios.Plan.abscissas(:,1))
        figure
        ecdf(IntegrationData.ErrorScenarios.Plan.responses(i,mask));
        hold on
        GPCresponses = GPC.value(GPC.Details.means + GPC.Details.stds.*IntegrationData.ErrorScenarios.Plan.abscissas(i,:));
        GPCresponses(GPCresponses<0)=0;
        ecdf(GPCresponses);
        if isfield(ModelDetails.nominalModelDetails,'structureMasks')
            for j=1:numel(ModelDetails.nominalModelDetails.structureMasks)
                responses = IntegrationData.ErrorScenarios.Plan.responses(i,mask);
                responses = responses(ModelDetails.nominalModelDetails.structureMasks{j}.maskGPCIndices);
                [a,b]=histwc(responses,ModelDetails.nominalModelDetails.structureMasks{j}.maskWeights,250);
                plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight,'-');
                [a,b]=histwc(GPCresponses(1,ModelDetails.nominalModelDetails.structureMasks{j}.maskGPCIndices),...
                    ModelDetails.nominalModelDetails.structureMasks{j}.maskWeights,250);
                plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight,'.');
            end
        end
    end
    
    % Check some cubature points
    if nSamples>=20
        indicesToCheck = 1+round(rand(20,1)*(nSamples-1));
    else
        indicesToCheck = 1:nSamples;
    end
    for i=1:length(indicesToCheck)
        figure
        ecdf(IntegrationData.ErrorScenarios.All.responses(indicesToCheck(i),:));
        hold on
        GPCresponses = GPC.value(GPC.Details.means + GPC.Details.stds.*...
            IntegrationData.ErrorScenarios.All.abscissas(indicesToCheck(i),:));
        GPCresponses(GPCresponses<0)=0;
        ecdf(GPCresponses);
        if isfield(ModelDetails.nominalModelDetails,'structureMasks')
            for j=1:numel(ModelDetails.nominalModelDetails.structureMasks)
                responses = IntegrationData.ErrorScenarios.All.responses(indicesToCheck(i),:);
                if ~isempty(ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices)
                    responses = responses(1,ModelDetails.nominalModelDetails.structureMasks{j}.maskGPCIndices);
                    [a,b]=histwc(responses,ModelDetails.nominalModelDetails.structureMasks{j}.maskWeights,250);
                else
                    b = [GPC.Details.cutoffValue max(responses(:))];
                    a = [1 0];
                end
                plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight,'-');
                if ~isempty(ModelDetails.nominalModelDetails.structureMasks{k}.maskGPCIndices)
                    [a,b]=histwc(GPCresponses(1,ModelDetails.nominalModelDetails.structureMasks{j}.maskGPCIndices),...
                        ModelDetails.nominalModelDetails.structureMasks{j}.maskWeights,250);
                else
                    b = [GPC.Details.cutoffValue max(responses(:))];
                    a = [1 0];
                end
                plot(b,1-((ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight-sum(a)+cumsum(a)))./...
                    ModelDetails.nominalModelDetails.structureMasks{j}.totalWeight,'.');
            end
        end
    end
end

% Check some statistics
GPCresponses = GPC.value(physicalAbscissas);
GPCresponses(GPCresponses<0) = 0;
responseError = abs(GPCresponses - IntegrationData.ErrorScenarios.All.responses);
figure
subplot(2,1,1)
histogram(responseError,100);
set(get(gca,'xlabel'),'string','Response error','fontsize',14);
set(get(gca,'ylabel'),'string','Histogram','fontsize',14);
title({'Distribution of errors for all responses between true values';'and GPC predictions among training points used to build GPC'});
subplot(2,1,2)
histogram(responseError./GPCresponses,100);
set(get(gca,'xlabel'),'string','Relative response error','fontsize',14);
set(get(gca,'ylabel'),'string','Histogram','fontsize',14);

% Average and maximum error histograms across scenarios
figure
subplot(2,1,1);
histogram(sum(responseError,1)/nSamples,100);
set(get(gca,'xlabel'),'string','Average abs. response error across scenarios');
set(get(gca,'ylabel'),'string','Histogram across responses');
title({'Distribution of maximum and mean response errors between true values';'and GPC predictions among training points used to build GPC'});
subplot(2,1,2);
histogram(max(responseError,[],1),100);
set(get(gca,'xlabel'),'string','Maximum abs. response error across scenarios');
set(get(gca,'ylabel'),'string','Histogram across responses');

% Some response dependences
if nResponses > 10
    [~,maxErrorResponse]=max(mean(responseError,1));
    responsesToCheck = [maxErrorResponse; 1+round(rand(10,1)*(nResponses-1))];
else
    responsesToCheck = 1:nResponses;
end

for j=1:length(responsesToCheck)
    for i=1:nDim
        figure
        if ismember(GPC.Details.samplingMethod,{'full','sparse'})
            indices = sum(abscissas(:,[1:(i-1) i+1:end])==0,2)==nDim-1;
        else
            indices = 1:nSamples;
        end
        scatter(physicalAbscissas(indices,i),IntegrationData.ErrorScenarios.All.responses(...
            indices,responsesToCheck(j)));
        hold on
        scatter(physicalAbscissas(indices,i),GPCresponses(indices,responsesToCheck(j)),'r.');
        set(get(gca,'xlabel'),'string',GPC.Details.variableNames{i},'fontsize',14);
        set(get(gca,'ylabel'),'string',['Response ',num2str(responsesToCheck(j))],'fontsize',14);
        legend({'True','GPC'});
        title(['Dependence of response ',num2str(responsesToCheck(j)),' on ',GPC.Details.variableNames{i}]);
    end
    
    dimCombinations = combnk(1:nDim,2);
    for i=1:length(dimCombinations(:,1))
        dims = 1:nDim;
        dims(dimCombinations(i,:))=[];
        if ismember(GPC.Details.samplingMethod,{'full','sparse'})
            indices = sum(abscissas(:,dims)==0,2)==nDim-2;
        else
            indices = 1:nSamples;
        end
        figure
        scatter3(physicalAbscissas(indices,dimCombinations(i,1)),physicalAbscissas(indices,dimCombinations(i,2)),...
            IntegrationData.ErrorScenarios.All.responses(indices,responsesToCheck(j)));
        hold on
        %     limits = minmax(physicalAbscissas(indices,dimCombinations(i,:))');
        %     [xGrid,yGrid] = meshgrid(linspace(limits(1,1),limits(1,2),500),linspace(limits(2,1),limits(2,2),500));
        %     grid = ones(numel(xGrid),1)*GPC.Details.means;
        %     grid(:,dimCombinations(i,:)) = [xGrid(:) yGrid(:)];
        %     zGrid = GPC.value(grid,responsesToCheck(j));
        %     surf(xGrid,yGrid,reshape(zGrid,size(xGrid)));
        scatter3(physicalAbscissas(indices,dimCombinations(i,1)),physicalAbscissas(indices,dimCombinations(i,2)),...
            GPCresponses(indices,responsesToCheck(j)),'r.');
        set(get(gca,'xlabel'),'string',GPC.Details.variableNames{dimCombinations(i,1)},'fontsize',14)
        set(get(gca,'ylabel'),'string',GPC.Details.variableNames{dimCombinations(i,2)},'fontsize',14)
        set(get(gca,'zlabel'),'string',['Response ',num2str(responsesToCheck(j))],'fontsize',14);
        legend({'True','GPC'});
        title(['Dependence of response ',num2str(responsesToCheck(j)),' on ',GPC.Details.variableNames{dimCombinations(i,1)},...
            ' and ',GPC.Details.variableNames{dimCombinations(i,2)}]);
    end
    
    % Predicted vs. true responses
    figure
    scatter(IntegrationData.ErrorScenarios.All.responses(:,responsesToCheck(j)),GPCresponses(:,responsesToCheck(j)));
    hold on
    maxResponse = max(IntegrationData.ErrorScenarios.All.responses(:,responsesToCheck(j)));
    plot([0 maxResponse],[0 maxResponse],'r');
    set(get(gca,'xlabel'),'string','True response values','fontsize',14)
    set(get(gca,'ylabel'),'string','GPC predicted response values','fontsize',14)
    title(['Predicted vs. true values for response ',num2str(responsesToCheck(j))]);
end

end
