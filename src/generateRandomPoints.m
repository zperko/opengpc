function IntegrationData = generateRandomPoints(GPCSettings)
%generateRandomPoints generates points randomly with either direct Monte
%Carlo or Latin Hypercube Sampling
% 1. INPUT:
%   - GPCSettings: Structure with the settings for sampling the points
% 2. OUTPUT:
%   - IntegrationData: A structure with the following entries:
%       - ErrorScenarios: Structure with all points for the error scenarios:
%           - All: A SampleClass object with the points of the sampling in
%           it and the corresponding response values.
% 3. DESCRIPTION: The code uses standard Monte Carlo sampling or Latin
% Hypercube Sampling

%% COPYRIGHT
% Zoltan Perko, TU Deflt
% Date: 2020.03.18.

%% FUNCTION DEFINITION
dirHermite=strcmp(GPCSettings.polType,'hermite');
dirLegendr=strcmp(GPCSettings.polType,'legendre');
dirLaguerr=strcmp(GPCSettings.polType,'laguerre');
switch GPCSettings.samplingMethod
    case 'random'
        r = rand(GPCSettings.nTrainingPoints,length(GPCSettings.polType));
        r = 2^0.5*erfinv(2*r-1)*diag(dirHermite)+(2*r-1)*diag(dirLegendr)-...
            log(1-r)*diag(dirLaguerr);
    case 'lhs'
        r = zeros(GPCSettings.nTrainingPoints,length(GPCSettings.polType));
        if any(dirHermite)
            r(:,dirHermite) = lhsnorm(zeros(sum(dirHermite),1),eye(sum(dirHermite)),...
                GPCSettings.nTrainingPoints);
        end
        if any(dirLegendr)
            r(:,dirLegendr) = 2*lhsdesign(GPCSettings.nTrainingPoints,sum(dirLegendr))-1;
        end
        if any(dirLaguerr)
            r(:,dirLaguerr) = -log(1-lhsdesign(GPCSettings.nTrainingPoints,sum(dirLaguerr)));
        end
    otherwise
        error('openGPC:generateSamplePoints:generateRandomPoints:InvalidSamplingMethod',...
            'Invalid samplingMethod requested, check GPCSettings');
end

% Construct a SampleClass object and fill up the abscissa values
for i={'variableNames','means','stds','polType'}
    SampleDetails.(i{1}) = GPCSettings.(i{1});
end
SampleDetails.masked = false;
SamplePoints = SampleClass(r,SampleDetails);

IntegrationData.ErrorScenarios.All = SamplePoints;

end