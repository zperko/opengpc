function y=laguerre(x,o)
%laguerre calculates the value of the oth Laguarre polynomial at
% abscissas x: La_o(x)
% 1. INPUT: 
%   - x: vector of abscissas (N x 1, double)
%   - o: orders of the Laguerre polynomials (N x 1, integer)
% 2. OUTPUT: 
%       y: matrix with the polynomial values at x for orders in o

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

if (~iscolumn(x))
    x=x';
end
N=length(x);

switch (max(o))
    case 0
        p = ones(N,1);
    case 1
        p = ones(N,2);
        p(:,2) = -x+1;
    otherwise
        p = zeros(N,max(o)+1);
        p(:,1)=ones(N,1);
        p(:,2)=-x+1;
        for i=3:max(o)+1
            p(:,i)=1/(i-1)*((2*(i-2)+1-x).*p(:,i-1)-(i-2)*p(:,i-2));
        end
end

y=p(:,o+1);

end