# Author: Zoltan Perko
# Date: 2019.07.19.
# Copyright: Delft University of Technology, RST
# Description:

import sys, os
import json
import argparse
import math
import time

parser = argparse.ArgumentParser()
parser.add_argument("--modelCase","-case", type=int, help="Model ID for blackboxModel",default=1)
parser.add_argument("--i", help="Input variables", nargs="+", type=float)
parser.add_argument("--calcDirectory","-dir",help="Full model directory path", default=os.getcwd())
args = parser.parse_args()
ID = os.getpid()

if args.modelCase==1:
	response = [100*(args.i[1]-args.i[0]**2)**2 + (1-args.i[0])**2, 
	(100*(args.i[1]-args.i[0]**2)**2 + (1-args.i[0])**2)*1e4,
	(100*(args.i[1]-args.i[0]**2)**2 + (1-args.i[0])**2)/1e9]
elif args.modelCase==2:
	response = [1+math.sin(args.i[0])+7*(math.sin(args.i[1]))**2+0.1*(args.i[2])**4*math.sin(args.i[0]),
	1+math.sin(args.i[0])+7*(math.sin(args.i[1]))**2+0.1*(args.i[2])**4*math.sin(args.i[0])/1e6]

time.sleep(15)

with open(os.path.join(args.calcDirectory,"ModelCase_"+str(args.modelCase)+"_PID_"+str(ID)+".json"),"w") as fh:
	json.dump(response,fh,sort_keys=True, indent=4, separators=(',',':'))
	fh.flush()

sys.exit(ID)