function [normType,maxPolOrder] = computeRandomHyperbolicTrimParameters(...
    basis,normType,nTrainingPoints,stepSize)
%computeRandomHyperbolicTrimParameters calculates the normType and maxPolOrder
% values with which the basis set should be trimmed hyperbolically such 
% that only so many vectors remain for which the number of training points
% can be supposed to be accurate
% 1. INPUT:
%   - basis: Matrix of multi-indexes of the basis vectors of the GPC
%   - normType: The initial norm for the hyperbolic trim
%   - nTrainingPoints: The number or training points used for constructing
%   the GPC.
%   - stepSize: The initial step size with which the search should be
%           performed
% 2. OUTPUT:
%   - normType: The norm type in the hyperbolic trim
%   - maxPolOrder: The maximum polynomial order
% 3. DESCRIPTION: hyperbolic trimming is done in two steps: first only the
% trim parameter is decreased to cut out enough cross terms, then if that
% is insufficient, the total polynomial order is decreased step by step.

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2020.03.19

%% FUNCTION DEFINITION
% First check if trim is needed at all
maxPolOrder = max(max(basis,[],2));
basis = hyperbolicTrimMI(basis,maxPolOrder,normType);

originalBasis = basis;
originalNormType = normType;
maxPolOrder = maxPolOrder + 1;
while length(basis(:,1)) > nTrainingPoints/2
    normType = originalNormType;
    maxPolOrder = maxPolOrder -1;
    while normType > 0
        normType = max(normType - stepSize,0);
        basis = hyperbolicTrimMI (originalBasis,maxPolOrder,normType);
        if length(basis(:,1)) <= nTrainingPoints/2
            break
        end
    end
end