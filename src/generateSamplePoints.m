function IntegrationData = generateSamplePoints(GPCSettings)
%generateSamplePoints generates the SampleClass and potentially
%CubatureClass objects corresponding to the requested samplingMethod
% 1. INPUT:
%   - GPCSettings Structure with the settings for constructing the GPC
% 2. OUTPUT:
%   - IntegrationData: A structure with the following entries:
%       - ErrorScenarios: Structure with all points for the error scenarios
%           - All: A SampleClass object with the points of the final
%          cubature/sampling in it and the corresponding response values.
%   If samplingMethod is full or sparse IntegrationData also contains:
%       - FinalCubature: The final cubature rule used to build the GPC. It
%       is the sum of all generated cubatures, i.e. the final sparse grid.
%       - Cubatures: An array of CubatureClass objects, with each object
%       corresponding to one of the sparse grids
%   
% 3. DESCRIPTION:

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2020.03.18.

%% FUNCTION DEFINITION
switch GPCSettings.samplingMethod
    case 'sparse'
        % Smolyak Sparse grids
        IntegrationData = generateCubature(GPCSettings);
    case 'full'
        error('openGPC:generateSamplePoints:fullGridNotImplemented',...
            'Full grid sampling not yet implemented');
    case {'random','lhs'}
        IntegrationData = generateRandomPoints(GPCSettings);
    otherwise
        error('openGPC:generateSamplePoints:InvalidSamplingMethod',...
            'Invalid samplingMethod requested, check GPCSettings');
end

end