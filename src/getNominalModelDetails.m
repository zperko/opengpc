function ModelDetails = getNominalModelDetails(ModelDetails)
%getNominalModelDetails obtains some details from the nominal model that
%can be useful for later analysis of the results.
% 1. INPUT:
%       - ModelDetails: The ModelDetails structure of openGPC
% 2. OUTPUT:
%       - ModelDetails: The ModelDetails structure updated with the
%       nominalModelDetails
% 3. DESCRIPTION:

%% FUNCTION DEFINITION
switch ModelDetails.externalModel
    case true
        pyData = py.externalCoupling.getNominalModelDetails(ModelDetails);
        ModelDetails.nominalModelDetails = pyData2matData(pyData);
        if isfield(ModelDetails,'requestedStructureMasks')
            for i=1:numel(ModelDetails.nominalModelDetails.structureMasks)
                maskLength = numel(ModelDetails.nominalModelDetails.structureMasks{i}.mask);
                ModelDetails.nominalModelDetails.structureMasks{i}.maskIndices = zeros(1,maskLength);
                ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights = zeros(1,maskLength);
                for j=1:numel(ModelDetails.nominalModelDetails.structureMasks{i}.mask)
                    ModelDetails.nominalModelDetails.structureMasks{i}.maskIndices(1,j) = ...
                        ModelDetails.nominalModelDetails.structureMasks{i}.mask{j}.index;
                    ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights(1,j) = ...
                        ModelDetails.nominalModelDetails.structureMasks{i}.mask{j}.weight;
                end
                ModelDetails.nominalModelDetails.structureMasks{i}.totalWeight = sum(...
                    ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights);
            end
        end
    case false
        % If model details are required from Matlab code, that should be
        % coded here.
        ModelDetails.nominalModelDetails = [];
end

end
