function IntegrationData = generateCubature(GPCSettings)
%generateCubature generates the CubatureClass and SampleClass
% objects corresponding to the requested integration rule
% 1. INPUT:
%   - GPCSettings Structure with the settings for constructing the GPC
% 2. OUTPUT:
%   - IntegrationData: A structure with the following entries:
%       - FinalCubature: The final cubature rule used to build the GPC. It
%       is the sum of all generated cubatures, i.e. the final sparse grid.
%       - Cubatures: An array of CubatureClass objects, with each object
%       corresponding to one of the sparse grids
%       - ErrorScenarios: Structure with all points for the error scenarios
%           - All: A SampleClass object with the points of the final
%          cubature in it and the corresponding response values.
% 3. DESCRIPTION:

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.10.31.

%% FUNCTION DEFINITION
% Get the quadrature rules that should be used in the different directions.
% For now only Gaussian rules are allowed
quadratureRules=cell(1,length(GPCSettings.polType));
for i=1:length(quadratureRules)
    quadratureRules(i)={['gauss_',GPCSettings.polType{i}]};
end

% Load the needed quadrature families with the needed grid orders
QuadratureSets = loadNeededQuadratureSets...
    (quadratureRules,GPCSettings.maxGridLevel,GPCSettings.nExtraLevels);

% Generate the multi-indices of all sparse grids making up the final cubature
multiIndices = generateMultiIndex(length(quadratureRules),GPCSettings.maxGridLevel-1,'all')+1;

% Construct the array of CubatureClass objects containing the details of
% each cubature (sparse grid)
Cubatures = CubatureClass(quadratureRules,QuadratureSets,multiIndices);

% Construct the final cubature by simply adding up the sparse grids
FinalCubature = Cubatures(1);
for i=2:length(Cubatures)
    FinalCubature = FinalCubature + Cubatures(i);
end

% Construct a SampleClass object and fill up the abscissa values
for i={'variableNames','means','stds','polType'}
    SampleDetails.(i{1}) = GPCSettings.(i{1});
end
SampleDetails.masked = false;
SamplePoints = SampleClass(FinalCubature.abscissas,SampleDetails);

IntegrationData.FinalCubature = FinalCubature;
IntegrationData.Cubatures = Cubatures;
IntegrationData.ErrorScenarios.All = SamplePoints;

end