function [ModelDetails,IntegrationData,GPC]=obtainMask(GPCSettings,IntegrationData,GPC,ModelDetails)
%obtainMask obtains a response mask showing which responses should be 
% included in the GPC. It runs the nominal scenario as well as the worst 
% case scenarios in each dimensions and choses any response that has a 
% value higher than cutoffValue in any of the included scenarios.
% 1. INPUT:
%   - GPCSettings: The settings structure of openGPC
%   - IntegrationData: The structure with the raw data used for 
%       constructing the GPC.
%   - GPC: The GPCClass object of openGPC
%   - modelDetails: The modelDetails structure containing any model
%       details necessary for running the model.
% 2. OUTPUT:
%   - modelDetails: The updated modelDetails structure containing the 
%       response mask with the IDs of the included responses.
%   - IntegrationData: The raw data structure with the planning error
%       scenarios updated, containing all responses in the planning 
%       scenarios.
% 3. DESCRIPTION:
% 4. TODO:
%   - If no cutoff is requested, obtainMask should be skipped as a whole
%   - +-3*std will have to be adjusted for more complex distributions
%   (could result in non-physical parameter values).

%% COPYRIGHT: Zoltan Perko, TU Delft
% Date: 2018.10.15.

%% FUNCTION DEFINITION

% First we generate the abscissa values for the planning scenarios. If
% cutoff is requested, the nominal plus the individual perturbations are
% included. If no cutoff is requested, only the nominal scenario is run.
planningPoints=kron(eye(length(GPC.Details.stds)),[1; -1]);
dirHermite = find(contains(GPC.Details.polType,'hermite'));
dirLaguerr = find(contains(GPC.Details.polType,'laguerre'));
planningPoints(:,dirHermite) = planningPoints(:,dirHermite)*GPCSettings.cutoffScenarioVariation;
planningPoints(:,dirLaguerr) = (planningPoints(:,dirLaguerr)+1)/2*GPCSettings.cutoffScenarioVariation;
nominalScenario = zeros(1,length(GPC.Details.stds));
nominalScenario(dirLaguerr) = GPC.Details.stds(dirLaguerr);
if GPCSettings.cutoffValue>0
    planningPoints = [nominalScenario; planningPoints];
else
    planningPoints = nominalScenario;
end

% Then we generate a SampleClass object for the planning scenarios
for i={'variableNames','means','stds','polType'}
    sampleDetails.(i{1}) = GPC.Details.(i{1});
end
sampleDetails.masked = false;
planningSample = SampleClass(planningPoints,sampleDetails);

% Calculate the response values for all error points
planningSample.computeResponses(ModelDetails,1:planningSample.nSamples);
% Write the ID of the nominal response to ModelDetails, so that when a
% responseMask is used we know how to interpret the GPC results
ModelDetails.nominalResponseID = planningSample.IDs(1);
if ModelDetails.getNominalModelDetails
    ModelDetails = getNominalModelDetails(ModelDetails);
end
% Update ModelDetails file if present
% if isfield(ModelDetails,'modelDetailsFile')
%     savejson('',ModelDetails,'FileName',ModelDetails.modelDetailsFile,'SingletCell',1,'SingletArray',0);
% end

% Get the response mask and copy the selected responses. If no cutoff is
% requested all responses are retained. If all responses would be cut off,
% the maximum response is retained.
if GPCSettings.cutoffValue>0
    responseMask = false(1,planningSample.nResponses);
    for i=1:planningSample.nSamples
        responseMask = any([responseMask ; abs(planningSample.responses(i,:))>=GPCSettings.cutoffValue],1);
    end
    if all(~responseMask)
        warning('openGPC:obtainMask:responsesAllMasked',...
            'All responses are below the cutoff value. Maximum response retained');
        [~,indexMaxResponse] = max(max(abs(planningSample.responses),[],1));
        responseMask = false(1,planningSample.nResponses);
        responseMask(indexMaxResponse) = true;
    end
else
    responseMask = true(1,planningSample.nResponses);
end

[membership,location]=ismember(planningSample.abscissas,...
    IntegrationData.ErrorScenarios.All.abscissas,'rows','legacy');
if any(membership)
    IntegrationData.ErrorScenarios.All.set('status',...
        planningSample.status(membership),location(membership));
    IntegrationData.ErrorScenarios.All.set('IDs',...
        planningSample.IDs(membership),location(membership));
    IntegrationData.ErrorScenarios.All.set('subProcesses',...
        planningSample.subProcesses(membership),location(membership));
    IntegrationData.ErrorScenarios.All.set('fileNames',...
        planningSample.fileNames(membership),location(membership));
    IntegrationData.ErrorScenarios.All.enterResponses...
        (planningSample.responses(membership,responseMask), location(membership));
else
    IntegrationData.ErrorScenarios.All.enterResponses...
        (zeros(1,sum(responseMask==1)), 1);
end
IntegrationData.ErrorScenarios.Plan = planningSample;

% Set the number of responses in the GPC and store the mask if cutoff is
% requested.
GPC = GPC.set('coeffs',zeros(length(GPC.coeffs(:,1)),sum(responseMask==1)));

Details = GPC.get('Details');
SampleDetails = IntegrationData.ErrorScenarios.All.get('Details');
if GPCSettings.cutoffValue>0
    Details.masked = true;
    Details.mask = responseMask;
    SampleDetails.masked = true;
    SampleDetails.mask = responseMask;
else
    Details.masked = false;
    SampleDetails.masked = false;
end
GPC = GPC.set('Details',Details);
IntegrationData.ErrorScenarios.All.set('Details',SampleDetails);
    
% Finally we set the response mask in the model details file
% ModelDetails = setResponseMask(ModelDetails,GPC);

end