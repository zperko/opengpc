function ModelDetails = checkModelDetails(ModelDetails,GPCSettings)
%checkModelDetails performs some basic checks on the provided input options
%to openGPC for the model
% 1. INPUT:
%   - ModelDetails: Structure with the details of the model for which the
%   GPC is built. Needed entries are:
%       - modelCase: the case number of the model (integer, 0 for user
%       model).
%       - externalModel: flag for signalling non-Matlab model (logical)
%       - parallelMode: indicator for parellel model run (string, 'serial',
%       'matlabParallel', 'modelParallel')
%       - modelDetailsFile: filename for a json format file containing all
%       additional model details.
%       - statusUpdateInterval: time interval in seconds between requesting 
%       status updates if model is run externally (double, default=60)
%       - externalRetryInterval: time interval in seconds between trying to
%       run external code until successful run (double, default =15)
%       - externalModelDelay: delay time in seconds between external
%       submissions, reading, status update
%       - getNominalModelDetails: flag to indicate that some model details
%       should be obtained from the nominal run (logical, default = false)
% 2. OUTPUT:
%   - ModelDetails: The structure with potentially changed settings

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.10.25.

%% FUNCTION DEFINITION
% Delete details set by GPCSettings and openGPC in modelDetails
overwrittenModelDetails = {'means','stds','variableNames','nominalResponseID'};
for i = overwrittenModelDetails
     if isfield(ModelDetails,i{1})
         ModelDetails = rmfield(ModelDetails,i{1});
     end
end

allowedParallelModes = {'serial','modelParallel','matlabParallel'};
externalDirectories = {'modelDirectory','calcDirectory','scriptDirectory'};

p = inputParser;
p.FunctionName = 'checkModelDetails';
p.CaseSensitive = false;
p.StructExpand = true;
p.KeepUnmatched = true;

%% Required arguments
% Check external model directory, required argument 1
p.addRequired('externalModel',@(x)validateattributes(x,{'logical'},...
    {'nonempty','scalar'},'openGPC:checkModelDetails:InvalidModelDetails',...
    'externalModel'));

% Check modelCase, required argument 2
p.addRequired('modelCase',@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','integer','nonnegative'},'openGPC:checkModelDetails:InvalidModelCase',...
    'modelCase'));

% Check parallel mode
p.addParameter('parallelMode','serial',@(x)assert((ischar(x) || isstring(x)) && ...
    ismember(x,allowedParallelModes),'openGPC:checkModelDetails:InvalidParallelMode',...
    'Only modes %s are allowed.',strjoin(allowedParallelModes,', ')));

% Check auxillary folders for external coupling
p.addParameter('modelDirectory',fullfile(pwd,'modelDirectory'),...
    @(x)validateattributes(x,{'char','string'},{'nonempty','row'},...
    'openGPC:checkModelDetails:InvalidModelDirectory','modelDirectory'));

p.addParameter('calcDirectory',fullfile(pwd,'modelDirectory','calculationFiles'),...
    @(x)validateattributes(x,{'char','string'},{'nonempty','row'},...
    'openGPC:checkModelDetails:InvalidCalculationDirectory','calcDirectory'));

p.addParameter('scriptDirectory',fullfile(pwd,'src'),...
    @(x)assert(exist(x,'dir') && exist(fullfile(x,'externalCoupling.py'),'file'),...
    'openGPC:checkModelDetails:InvalidScriptDirectory',['scriptDirectory does not exist ',...
    'or does not contain externalCoupling.py coupling module.']));

% Check status update interval value
p.addParameter('statusUpdateInterval',60,@(x)validateattributes(x,{'numeric'},...
    {'positive','scalar','nonempty'},'openGPC:checkModelDetails:InvalidStatusUpdateInterval',...
    'statusUpdateInterval'));

% Check external code running retry interval
p.addParameter('externalRetryInterval',15,@(x)validateattributes(x,{'numeric'},...
    {'positive','scalar','nonempty'},'openGPC:checkModelDetails:InvalidExternalRetryInterval',...
    'externalRetryInterval'));
p.addParameter('maxExternalRetryNumber',10,@(x)validateattributes(x,{'numeric'},...
    {'positive','nonempty','scalar','integer'},'openGPC:checkModelDetails:InvalidMaxExternalRetryNumber',...
    'maxExternalRetryNumber'));
p.addParameter('externalModelDelay',0.0,@(x)validateattributes(x,{'numeric'},...
    {'nonempty','scalar','nonnegative'},'openGPC:checkModelDetails:InvalidExternalModelDelay',...
    'externalModelDelay'));

% Check nominal model details flag
p.addParameter('getNominalModelDetails',false,@(x)validateattributes(x,{'logical'},...
    {'nonempty','scalar'},'openGPC:checkModelDetails:InvalidGetNominalModelDetails',...
    'getNominalModelDetails'));

% Check authentication details
p.addParameter('Authenticity',struct('valid',true,'token',[]),@(x)assert(isstruct(x) && ...
    all(ismember({'valid','token'},fieldnames(x))) && islogical(x.valid),...
    'openGPC:checkModelDetails:InvalidAuthenticity',['Authenticity must be structure with ',...
    'entries valid (logical) and token (string).']));

% Parse input
p.parse(ModelDetails.externalModel,ModelDetails.modelCase,ModelDetails);
ModelDetails = p.Results;

if ModelDetails.externalModel == true
    for i=externalDirectories
        if ismember(i{1},p.UsingDefaults)
            warning(['openGPC:checkModelDetails:Default',i{1}],...
                'No %s given, set to default %s.',...
                i{1},ModelDetails.(i{1}));
        end
    end
    if ~exist(ModelDetails.modelDirectory,'dir')
        warning('openGPC:checkModelDetails:NonexistentModelDirectory',...
            'Given model directory %s does not exist, it is created now.',...
            ModelDetails.modelDirectory);
        mkdir(ModelDetails.modelDirectory);
    end
    
    if ~exist(ModelDetails.calcDirectory,'dir')
        warning('openGPC:checkModelDetails:NonexistentCalculationDirectory',...
            'Given calculation directory %s does not exist, it is created now.',...
            ModelDetails.calcDirectory);
        mkdir(ModelDetails.calcDirectory);
    end
end

if ~isempty(p.UsingDefaults(~ismember(p.UsingDefaults,externalDirectories)))
    warning('openGPC:checkModelDetails:DefaultSettingsUsed',...
        'Using default values for %s, consider specifying all settings.',...
        strjoin(p.UsingDefaults,', '));
end

if ~isempty(fieldnames(p.Unmatched))
    warning('openGPC:checkModelDetails:UnmatchedSettings',...
        'Settings %s do not match any valid ModelDetails settings. Copying them without checking.',...
        strjoin(fieldnames(p.Unmatched),', '));
    for field = fieldnames(p.Unmatched)'
        ModelDetails.(field{1}) = p.Unmatched.(field{1});
    end
end

% Copying means and standard deviations to ModelDetails
for i={'means','stds','variableNames'}
    ModelDetails.(i{1}) = GPCSettings.(i{1});
end

end