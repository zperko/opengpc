function ModelDetails = loadModelDetails(modelDetailsFile)
%loadModelDetails reads a model details file containing any additional
%information that is required to run the model for which the GPC is built.
% 1. INPUT:
%       - modelDetailsFile: A JSON file containing the required additional
%       data
% 2. OUTPUT:
%       - ModelDetails: A structure with all necessary information
% 3. DESCRIPTION:
% 4. TODO: 
%       - Check new jsonsave for cells
%       - Check need for directories (was needed for Thinknode coupling)
% 5. DEPENDENCIES:
%       - jsonlab-1.5

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

ModelDetails = loadjson(modelDetailsFile);
ModelDetails.modelDetailsFile=modelDetailsFile;

% Create directory for auxillary files
if ~isfield(ModelDetails,'modelDirectory')
    error('openGPC:loadModelDetails:modelDirectoryMissing',...
        'No modelDirectory found in %s.',modelDetailsFile);
else
    if (~exist([ModelDetails.modelDirectory,'calculationFiles'],'dir'))
        mkdir([ModelDetails.modelDirectory,'calculationFiles']);
    end
end

% Check if path is present for the coupling scripts
if (~isfield(ModelDetails,'scriptDirectory'))
    error('openGPC:loadModelDetails:scriptDirectoryMissing',...
        'Path to coupling script executable not set.');
elseif ModelDetails.scriptDirectory(end)~=filesep
    ModelDetails.scriptDirectory=[ModelDetails.scriptDirectory,filesep];
end

savejson('',ModelDetails,'FileName',modelDetailsFile,'SingletCell',1,'SingletArray',0);

end