function [responses,IDs,status]=blackboxModel(input,ModelDetails)
%blackboxModel is the problem for which PCE is built. It contains both a 
% userFunction, as well as a set of test models using simple analytical 
% and easy to solve numerical problems
% 1. USE:
%   [response,IDs,status] = blackboxModel(input,ModelDetails);
% 2. INPUT: 
%   - input: value of input random variables for the different realizations
%   (nSamples x nInputs, double)
%   - ModelDetails: Structure with the details of the model for which the
%   GPC is built.
% 3. OUTPUT: 
%   - response: the matrix of response values corresponding to the
%   different ralizations of the inputs (nSamples x nResponses, double)
%   - IDs: assigned IDs for the calculations. If userFunction is used, IDs
%   are assigned in it. If a test function is used, IDs are assigned based
%   on datetime + the index of the input combination.
%   - status: exit status of calculations

%% COPYRIGHT: Zoltan Perko, TU Delft
% 2018.11.29.

%% FUNCTION DEFINITION

IDs=cell(length(input(:,1)),1);
status=cell(length(input(:,1)),1);
physicalInput = ones(length(input(:,1)),1)*ModelDetails.means + ...
    input.*(ones(length(input(:,1)),1)*ModelDetails.stds);

switch (ModelDetails.modelCase)
    %% User defined function
    case (0)
        [responses, IDs, status]= userFunction(physicalInput,ModelDetails);
    
    %% Analytical test functions with increasing number of inputs
    case (1)
        % Rosenbrock function in 2D, from Evaluation of Non-Intrusive
        % Approaches for Wiener-Askey Generalized Polynomial Chaos, Eldred,
        % 2008. Correct answer is 402 - 2*He_1(x) - 200*He_1(y) +
        % 601*He_2(x) +100*He_2(y)- 200*He_2(x)*He_1(y) +100*He_4(x)
        responses(:,1)= 100*(physicalInput(:,2)-physicalInput(:,1).^2).^2+(1-physicalInput(:,1)).^2;
        responses(:,2)=(100*(physicalInput(:,2)-physicalInput(:,1).^2).^2+(1-physicalInput(:,1)).^2)*1e4;
        responses(:,3)=(100*(physicalInput(:,2)-physicalInput(:,1).^2).^2+(1-physicalInput(:,1)).^2)/1e9;
        
    case(2)
        % Ishigami function in 3D, from Adaptive sparse polynomial chaos
        % expansion based on least angle regression, Blatman, 2011
        responses(:,1)=1+sin(physicalInput(:,1))+7*(sin(physicalInput(:,2))).^2+...
            0.1*(physicalInput(:,3)).^4.*sin(physicalInput(:,1));
        responses(:,2)=(1+sin(physicalInput(:,1))+7*(sin(physicalInput(:,2))).^2+...
            0.1*(physicalInput(:,3)).^4.*sin(physicalInput(:,1)))/1e6;
    
    case (3)
        % Sobol' function in 8D, from Efficient computation of global
        % sensitivity indices using sparse polynomial chaos expansions,
        % Blatman, 2010.
        alpha=[1,2,5,10,20,50,100,500];
        responses(:,1)=prod((abs(4*physicalInput-2)+ones(length(physicalInput(:,1)),1)*alpha)./...
            (ones(length(physicalInput(:,1)),1)*(1+alpha)),2);
        responses(:,2)=(prod((abs(4*physicalInput-2)+ones(length(physicalInput(:,1)),1)*alpha)./...
            (ones(length(physicalInput(:,1)),1)*(1+alpha)),2))/1e6;
    
    case (4)
        % CONTINUOUS Genz function f_5 in 10 dimensions. Based on J. Foo,
        % G.E. Karniadakis, "Multi-element probablistic collocation method
        % in high dimensions", 2010, Journal of Computational Physics, Vol.
        % 229, p. 1536-1557
        u=0.5*ones(size(physicalInput));
        alpha=ones(length(physicalInput(:,1)),1)*(10./(2.^(1:length(physicalInput(1,:)))));
        responses(:,1) = exp(-sum(alpha.*abs(physicalInput-u),2));
        responses(:,2) = exp(-sum(alpha.*abs(physicalInput-u),2))/1e6;
        
    case (5)
        % Morris function in 20 dimensions, taken from Blatman 2012 (also
        % Saltelli SA book)
        beta1(1:10)=20;
        beta1(11:2:19)=-1;
        beta1(12:2:20)=1;
        beta2(1:6,1:6)=-15;
        beta2(7:2:19,1:2:19)=1;
        beta2(7:2:19,2:2:20)=-1;
        beta2(8:2:20,2:2:20)=1;
        beta2(8:2:20,1:2:19)=-1;
        beta2(1:2:5,7:2:19)=1;
        beta2(1:2:5,8:2:20)=-1;
        beta2(2:2:6,7:2:19)=-1;
        beta2(2:2:6,8:2:20)=1;
        beta3(1:5,1:5,1:5)=-10;
        beta3(20,20,20)=0;
        beta4(1:4,1:4,1:4,1:4)=5;
        beta4(20,20,20,20)=0;
        w=2*(physicalInput-0.5);
        w(:,3:2:7)=2*(1.1*physicalInput(:,3:2:7)./(physicalInput(:,3:2:7)+0.1)-0.5);
            
        responses=zeros(length(physicalInput(:,1)),1);
        for i=1:20
            responses=responses+beta1(i)*w(:,i);
            for j=i+1:20 %6
                responses=responses+beta2(i,j)*w(:,i).*w(:,j);
                for l=j+1:20 %5
                    responses=responses+beta3(i,j,l)*w(:,i).*w(:,j).*w(:,l);
                    for s=l+1:20 %4
                        responses=responses+beta4(i,j,l,s)*w(:,i).*w(:,j).*w(:,l).*w(:,s);
                    end
                end
            end
        end
        responses = [responses responses/1e6];
    
    %% Analytical test function with noise
    case (6)
        % Analytical test function, with and without noise in response
        noiseLevel=10;
        responses(:,1) = (physicalInput(:,2)-physicalInput(:,1).^2).^2+(1-physicalInput(:,1)).^2;
        responses(:,2) = (physicalInput(:,2)-physicalInput(:,1).^2).^2+(1-physicalInput(:,1)).^2 + ...
            noiseLevel*(heaviside(physicalInput(:,2)-1)+heaviside(physicalInput(:,2))+heaviside(physicalInput(:,2)+1));
        
    %% Numerical test problems
    case(7)
        % A transmission problem for radiation through shielding
        responses(:,1) = radiationShielding(physicalInput);
        responses(:,2) = radiationShielding(physicalInput)/1e6;
        
    case (8)
        % A 2/3 dimensional neutron diffusion criticality problem
        responses=zeros(length(physicalInput(:,1)),1);
        for i=1:length(physicalInput(:,1))
            [responses(i,1),~,~]=critical2N(physicalInput(i,:));
        end
        responses(:,2) = responses(:,1)/1e7;
    
    case (9)
        % 1 effective group point kinetics
        % Uncertain inputs: starting power, lambda, Lambda, beta, rho, time
        % of reactivity insertion
%         beta_i=[5.96e-5, 64.33e-5, 22.64e-5, 57.82e-5, 126.68e-5, 54.67e-5, 46.16e-5, 24.74e-5];
%         lambda_i=[0.0125, 0.0283, 0.0425, 0.133, 0.292, 0.666, 1.63, 3.55];
%         lambda=(1/sum(beta_i)*sum(beta_i./lambda_i))^-1;
%         s=[0.05, 0.05, 0.05, 0.05, 0.05, 0.05];
%         mu=[1, lambda, 0.478e-6, 0.00403, 0.00403*0.9047619, 6e-3];
%         input=(ones(length(input(:,1)),1)*mu).*(1+(ones(length(input(:,1)),1)*s).*input);

%         t=unique(sort([linspace(0,6,3e5)'; input(6)],'ascend'));
%         temp_index=find(t>input(6),1,'first');
%         rho=[ones(temp_index,1)*input(5);zeros(3e5-temp_index+1,1)];
%         Simplified one group PK
%         output(1,1)=input(1)*(input(4)/(input(4)-input(5))*exp(input(2)*input(5)/(input(4)-input(5))*input(6))-...
%             input(5)/(input(4)-input(5))*exp(-(input(4)-input(5))/input(3)*input(6)));
        omega= [ (-(physicalInput(:,2).*physicalInput(:,3) - ...
            physicalInput(:,5)+physicalInput(:,4))+...
            ((physicalInput(:,2).*physicalInput(:,3) - ...
            physicalInput(:,5)+physicalInput(:,4)).^2+...
            4*physicalInput(:,3).*physicalInput(:,5).*physicalInput(:,2)).^0.5)./...
            (2*physicalInput(:,3)) , ...
            (-(physicalInput(:,2).*physicalInput(:,3) - ...
            physicalInput(:,5)+physicalInput(:,4))-...
            ((physicalInput(:,2).*physicalInput(:,3) - ...
            physicalInput(:,5)+physicalInput(:,4)).^2+...
            4*physicalInput(:,3).*physicalInput(:,5).*physicalInput(:,2)).^0.5)./...
            (2*physicalInput(:,3)) ] ;
        responses(:,1)=physicalInput(:,1).*((omega(:,1)+physicalInput(:,2))./...
            physicalInput(:,2).*omega(:,2)./...
            (omega(:,2)-omega(:,1)).*exp(omega(:,1).*physicalInput(:,6))+...
            (omega(:,2)+physicalInput(:,2))./physicalInput(:,2).*omega(:,1)./...
            (omega(:,1)-omega(:,2)).*exp(omega(:,2).*physicalInput(:,6)));
        responses(:,2) = responses(:,1)/1e6;
%         [ P, C ] = One_Effective_Group_PK( t,rho,physicalInput(4),physicalInput(3),physicalInput(2), ...
%             physicalInput(1), physicalInput(4)/physicalInput(3)/physicalInput(2)*physicalInput(1) );
%         output(1,1)=max(P);
%         output(1,2)=P(end);
%         output(1,3)=max(C);
%         output(1,4)=C(end);
    
    case (10)
        % Two group diffusion for slab coupled with pure heat conduction,
        % same problem as used for coupled adjoint sensitivity article
        [Sigma_t,Sigma_f,Sigma_tr,khi,D,nu,Q,k,~,a,~,~,~,~,~,T_ref]=...
         Get_data_slab(0);
        Sigma_t_temp=zeros(2,2,length(physicalInput(:,1)));
        Sigma_t_temp(1,1,:)=permute(Sigma_t(1,1)*(1+physicalInput(:,1)),[3 2 1]);
        Sigma_t_temp(2,1,:)=permute(Sigma_t(2,1)*(1+physicalInput(:,2)),[3 2 1]);
        Sigma_t_temp(1,2,:)=permute(Sigma_t(1,2)*(1+physicalInput(:,3)),[3 2 1]);
        Sigma_f_temp=(Sigma_f*ones(1,length(physicalInput(:,1)))).*(1+physicalInput(:,4:5))';
        Sigma_tr_temp=zeros(2,2,length(physicalInput(:,1)));
        Sigma_tr_temp(1,1,:)=permute(Sigma_tr(1,1)*(1+physicalInput(:,6)),[3 2 1]);
        Sigma_tr_temp(1,2,:)=permute(Sigma_tr(1,2)*(1+physicalInput(:,7)),[3 2 1]);
        Sigma_tr_temp(2,2,:)=permute(Sigma_tr(2,2)*(1+physicalInput(:,8)),[3 2 1]);
        D=(ones(length(physicalInput(:,1)),1)*D).*(1+physicalInput(:,9:10));
        nu=(ones(length(physicalInput(:,1)),1)*nu).*(1+physicalInput(:,11:12));
        T_ref=T_ref*(1+physicalInput(:,13));
        k=k*(1+physicalInput(:,14));
        a=a*(1+physicalInput(:,15));
        
        responses=zeros(length(physicalInput(:,1)),13);
        for i=1:length(physicalInput(:,1))
            [Phi,T,T_avg,B1,B2,Pow,k_eff,eta,p,epsilon,P_NL_thermal,P_NL_fast]=...
                calculateSteadyStateSlab(Sigma_t_temp(:,:,i),Sigma_f_temp(:,i),Sigma_tr_temp(:,:,i),...
                khi,D(i,:),nu(i,:),Q,k(i),a(i),T_ref(i));
            responses(i,:)=[Phi,T,T_avg,B1,B2,Pow,k_eff,eta,p,epsilon,P_NL_thermal,P_NL_fast];
        end
        responses(:,end) = responses(:,end)/1e6;
    otherwise
        error('openGPC:blackboxModel:InvalidTestCase',...
            'Invalid testcase selected must be between 1-10.');

end

if ModelDetails.modelCase~=0
    for i=1:length(physicalInput(:,1))
        IDs(i,1)={['ID',datestr(datetime(datetime,'Format','yyyyMMdd-HHmmSSSSSSSSS'),'yyyymmdd-HHMMSSFFF'),...
            ' S',num2str(i)]};
        pause(1e-3);
        if all(~isnan(responses(i,:)))
            status(i,1)={'fetched'};
        else
            status(i,1)={'failed'};
        end
    end
end
