classdef MultiDimensionalOrthogonalPolynomClass < OrthogonalPolynomClass
    properties (SetAccess = protected, GetAccess = public)
        multiIndex
    end
    methods
        function MultiDimensionalPolynom = MultiDimensionalOrthogonalPolynomClass(type,MI)
            % constructs a multidimensional orthogonal polynom object
            if (nargin < 2) && length(dbstack) == 1
                error('openGPC:MultiDimensionalOrthogonalPolynomClass:InputError',...
                    ['Not enough input arguments ',...
                    ', type and multiIndex are needed.']);
            elseif nargin == 2
                MultiDimensionalPolynom(length(MI(:,1)),1) = ...
                    MultiDimensionalOrthogonalPolynomClass(type);
                
                for i=1:length(MI(:,1))
                    MultiDimensionalPolynom(i,1).type = type;
                    MultiDimensionalPolynom(i,1).order = sum(MI(i,:));
                    MultiDimensionalPolynom(i,1).multiIndex = MI(i,:);
                end
            end
        end
        
        function Y = value(MDPol,x)
            % calculates the value of the MD polynom at x
            Y = ones(length(x(:,1)),1);
            for i=1:length(x(1,:))
                switch MDPol.type{i}
                    case 'hermite'
                        Y=Y.*herm(x(:,i),MDPol.multiIndex(i));
                    case 'legendre'
                        Y=Y.*legendr(x(:,i),MDPol.multiIndex(i));
                    case 'laguerre'
                        Y=Y.*laguerre(x(:,i),MDPol.multiIndex(i));
                    otherwise
                        error('openGPC:MultiDImensionalOrthogonalPolynomClass:value:InvalidPolType',...
                            'Invalid polynomial type!');
                end
            end
            
        end
        
        function Y = derivativeValue(MDPol,x,derIndex)
            % calculate the value of the partial derivative of the MD
            % polynom at x in direction derIndex
            Y=ones(length(x(:,1)),1);
            for i=1:length(x(1,:))
                switch MDPol.type{i}
                    case 'hermite'
                        Y=Y.*(hermDer(x(:,i),MDPol.multiIndex(i))*(i==derIndex)+...
                            herm(x(:,i),MDPol.multiIndex(i))*(i~=derIndex));
                    case 'legendre'
                        Y=Y.*(legendrDer(x(:,i),MDPol.multiIndex(i))*(i==derIndex)+...
                            legendr(x(:,i),MDPol.multiIndex(i))*(i~=derIndex));
                    case 'laguerre'
                        Y=Y.*(laguarreDer(x(:,i),MDPol.multiIndex(i))*(i==derIndex)+...
                            laguarre(x(:,i),MDPol.multiIndex(i))*(i~=derIndex));
                    otherwise
                        error('openGPC:MultiDimensionalOrthogonalPolynomClass:derivativeValue:InvalidPolType',...
                            'Invalid polynomial type.');
                end
            end
        end
        
    end
end