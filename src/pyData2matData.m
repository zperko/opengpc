function matData = pyData2matData(pyData)

matData = py2mat(pyData);
end

function matData = py2mat(pyData)
    matData = py2matConversion(pyData);
    switch class(matData)
        case 'cell'
            for i = 1:numel(matData)
                matData{i} = py2mat(matData{i});
            end
        case 'struct'
            for i = 1:numel(matData)
                fields = fieldnames(matData(i));
                for j = 1:numel(fields)
                    matData(i).(fields{j}) = py2mat(matData(i).(fields{j}));
                end
            end
    end
end

function matData = py2matConversion(pyData)
switch class(pyData)
    case {'py.str','py.unicode'}
        matData = char(pyData);
    case 'py.bytes'
        matData = uint8(pyData);
    case {'py.int','py.long','py.array.array'}
        matData = double(pyData);
    case {'py.list','py.tuple'}
        matData = cell(pyData);
    case 'py.dict'
        matData = struct(pyData);
    otherwise
        matData = pyData;
end
end