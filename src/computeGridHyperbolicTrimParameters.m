function [normType,maxPolOrder] = computeGridHyperbolicTrimParameters(...
    basis,normType,MI,stepSize)
%computeGridHyperbolicTrimParameters calculates the normType and maxPolOrder
% values with which the basis set should be trimmed hyperbolically such 
% that only those vectors remain for which the integration scheme in MI 
% can be supposed to be accurate
% 1. INPUT:
%   - basis: Matrix of multi-indexes of the basis vectors of the GPC
%   - normType: The initial norm for the hyperbolic trim
%   - MI: The multi-indexes of the used grids
%   - stepSize: The initial step size with which the search should be
%           performed
% 2. OUTPUT:
%   - normType: The norm type in the hyperbolic trim
%   - maxPolOrder: The maximum polynomial order
% 3. DESCRIPTION:

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.11.15

%% FUNCTION DEFINITION

allowedPolOrders=2*MI-2;
if (all(max(allowedPolOrders,[],1)==max(allowedPolOrders(:,1))))
    maxAllowedPCOrder=max(allowedPolOrders(:,1));
else
    warning('opeGPC:computeHyperbolicTrimParameters:AnisortopicPCBasis',...
        ['The entered sparse grid is anisotropic, unidirectional vectors ',...
        'trimmed to the lowest order']);
    maxAllowedPCOrder=min(max(allowedPolOrders,[],1));
end
if (max(max(basis,[],2))>maxAllowedPCOrder)
    maxPolOrder=maxAllowedPCOrder;
else
    maxPolOrder=max(max(basis,[],2));
end

% First check if trim is needed at all
basis=hyperbolicTrimMI(basis,maxPolOrder,normType);
keptFlag=false(length(basis(:,1)),1);
for i=1:length(basis(:,1))
    if (any(all(ones(length(allowedPolOrders(:,1)),1)*double(basis(i,:))<=allowedPolOrders,2)))
        keptFlag(i,1)=true;
    else
        break
    end
end
originalBasis = basis;
if (any(~keptFlag)) 
    decrease=true;
    while (decrease)
        normType=normType-stepSize;
        basis=hyperbolicTrimMI(originalBasis,maxPolOrder,normType);
        keptFlag=false(length(basis(:,1)),1);
        for i=1:length(basis(:,1))
            if (any(all(ones(length(allowedPolOrders(:,1)),1)*double(basis(i,:)) <= ...
                    allowedPolOrders,2)))
                keptFlag(i,1)=true;
            else
                break
            end
        end
        if (all(keptFlag))
            decrease=false;
        end
    end
    
    while (1)
        stepSize=stepSize/2;
        previousBasis=basis;
        if (decrease)
            normType=normType-stepSize;
        else
            normType=normType+stepSize;
        end
        
        basis = hyperbolicTrimMI(originalBasis,maxPolOrder,normType);
        keptFlag=false(length(basis(:,1)),1);
        for i=1:length(basis(:,1))
            if (any(all(ones(length(allowedPolOrders(:,1)),1)*double(basis(i,:))<=allowedPolOrders,2)))
                keptFlag(i,1)=true;
            else
                break
            end
        end
              
        if ( all(keptFlag) && all(size(previousBasis)==size(basis)) && ...
                all(all(previousBasis==basis,2)) )
            break
        elseif (all(keptFlag))
            decrease = false;
        else
            decrease = true;
        end
    end
end

end

% This is a different trim, where simply all basis vectors are kept for
% which the accuracy can be assumed to be sufficient. The hyperbolic trim
% approach is more accurate, as in this case it can happen that
% many-dimensional basis vectors remain which won't be integrated
% accurately
%         Kept_Flag=false(length(PCE.Basis(:,1)),1);
%         for i=1:length(PCE.Basis(:,1))
%             if (any(all(ones(length(Allowed_PC_Orders(:,1)),1)*double(PCE.Basis(i,:))<=Allowed_PC_Orders,2)))
%                 Kept_Flag(i,1)=true;
%             end
%         end
%         PCE.Basis=PCE.Basis(Kept_Flag,:);