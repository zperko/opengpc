classdef GPCClass
    % GPC class for Generalized Polynomial Chaos Expansion of responses of 
    % interest
    properties (SetAccess = protected, GetAccess = public)
        Details % Structure with Details of the GPC
        basis % Matrix of multi-indexes representing the basis vectors of the GPC
        basisNorm % The norm of the basis vectors in the GPC
        BasisVectors % MultiDimensional orthogonal polynomial objects representing the basis vectors
        coeffs % Matrix of GPC coefficients
        polType % Cell of polynomial types in the GPC
    end
    methods
        function GPC = GPCClass(GPCSettings,MI)
            % Constructor
            GPC.Details = GPCSettings;
            GPC.polType = GPCSettings.polType;
            GPC.basis = generateMultiIndex(length(GPC.polType),...
                GPC.Details.maxPolOrder,'all');
            % If requested, perform a hyperbolic trim
            if GPC.Details.trim == true
                switch GPCSettings.samplingMethod
                    case {'sparse','full'}
                        % Adjust multi-index matrix for Extra integration levels
                        MI(MI == GPCSettings.maxGridLevel) = GPCSettings.maxGridLevel...
                            + GPCSettings.nExtraLevels;
                        [GPC.Details.normType,GPC.Details.maxPCOrder] = ...
                            computeGridHyperbolicTrimParameters(GPC.basis,GPCSettings.normType,MI,0.01);
                    case {'random','lhs'}
                        [GPC.Details.normType,GPC.Details.maxPCOrder] = ...
                            computeRandomHyperbolicTrimParameters(GPC.basis,GPCSettings.normType,...
                            GPCSettings.nTrainingPoints,0.01);
                end
                
                GPC.basis = hyperbolicTrimMI(GPC.basis,...
                    GPC.Details.maxPolOrder,GPC.Details.normType);
                fprintf(['PC Basis was trimmed hyperbolically with ',...
                    'q-norm=%f and order p=%f\n'],GPC.Details.normType,...
                    GPC.Details.maxPolOrder);
                fprintf('Number of remaining basis vectors: %d\n',...
                    length(GPC.basis(:,1)));
            end
            
            % Generate the basis vectors
            GPC.BasisVectors = MultiDimensionalOrthogonalPolynomClass...
                (GPC.polType,GPC.basis);
            
            % Calculating the basis norm
            GPC.basisNorm=computeBasisNorm(GPC.basis,GPC.polType);
            
            % Initialize the coefficients
            GPC.coeffs=zeros(length(GPC.basis(:,1)),1);
        end
        
        % Constructor methods
        function GPC = set(GPC,property,value)
            % sets the requested GPC property to value
            if isprop(GPC,property)
                GPC.(property) = value;
            else
                warning('openGPC:GPCClass:set:PropertyNotFound',...
                    ['Requested field (',property,') not found in GPC class. ',...
                    'Properties not changed.']);
            end
        end
        
        function value = get(GPC,property)
            % returns the requested GPC property
            if isprop(GPC,property)
                value = GPC.(property);
            else
                warning('openGPC:GPCClass:set:PropertyNotFound',...
                    ['Requested field (',property,') not found in GPC class.']);
                value = [];
            end
        end
        
        function [GPC, dGPCCoeffs] = updateCoeffsWithGridContribution(...
                GPC,Cubature,responses)
            % updates the GPC coefficients according to the cubature and
            % responses
            dGPCCoeffs=zeros(length(GPC.basisNorm),length(responses(1,:)));
            
            for c=1:length(dGPCCoeffs(:,1))
                dGPCCoeffs(c,:)=dGPCCoeffs(c,:)+1/GPC.basisNorm(c)*(Cubature.weights.*...
                    GPC.BasisVectors(c).value(Cubature.abscissas))'*responses;
            end
            GPC.coeffs = GPC.coeffs + dGPCCoeffs; 
        end
        
        function GPC = calcCoeffsWithRegression(GPC,abscissas,responses)
            % calculates the GPC coefficients using standard regression
            dataMx = zeros(length(responses(:,1)),length(GPC.basisNorm));
            for c=1:length(GPC.basisNorm)
                dataMx(:,c) = GPC.BasisVectors(c).value(abscissas);
            end
            GPC.coeffs = (dataMx'*dataMx)\(dataMx' * responses);
        end
        
        function GPC = changenResponses(GPC,indices)
            % expands the GPC responses or extracts from it
            if (length(indices)>1 && max(indices) > length(GPC.coeffs(1,:)))
                warning('openGPC:GPCClass:changenResponses:InconsistentIndexSet',...
                    ['More than 1 indice passed, but maximum index is out of range in GPC.coeffs. ',...
                    'No change done.']);
            else
                % To increase or decrease the number of responses in the GPC
                if (max(indices)<=length(GPC.coeffs(1,:)))
                    GPC.coeffs = GPC.coeffs(:,indices);
                else
                    GPC.coeffs(end,indices)=0;
                end
            end
        end
                   
        % Loading from raw data
        function GPC = loadFromData(GPC,Details,basis,coeffs,polType)
            GPC.Details = Details;
            GPC.basis = basis;
            GPC.polType = polType;
            GPC.Details.polType = polType;
            GPC.coeffs = coeffs;
            % Calculating the basis norm
            GPC.basisNorm=computeBasisNorm(GPC.basis,GPC.polType);
            % Generating basis vectors
            GPC.BasisVectors = MultiDimensionalOrthogonalPolynomClass(...
                GPC.polType,GPC.basis);
        end
        
        % Postprocessing methods
        function responses = value(GPC,x,varargin)
            % calculates the GPC values
            if (nargin<3)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            xi =(x-ones(length(x(:,1)),1)*GPC.Details.means)./...
                (ones(length(x(:,1)),1)*GPC.Details.stds); 
            responses = computeMDPolValues(GPC.BasisVectors,xi,false)*...
                GPC.coeffs(:,responseIDs);
        end
        
        function responseDerivatives = differentiate(GPC,x,direction,varargin)
             % calculates the derivative values of GPC
            if (nargin<4)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            indexSet = [1:direction-1 direction+1:length(GPC.basis(1,:))];
            xi = (x-ones(length(x(:,1)),1)*GPC.Details.means)./...
                (ones(length(x(:,1)),1)*GPC.Details.stds);
            
            derPol = MultiDimensionalOrthogonalPolynomClass(...
                GPC.polType(direction),GPC.basis(:,direction));
            if numel(GPC.polType)>1
                MDPol = MultiDimensionalOrthogonalPolynomClass(...
                    GPC.polType(indexSet),GPC.basis(:,indexSet));
                responseDerivatives = ...
                    (computeMDPolValues(MDPol,xi(:,indexSet),false).*...
                    computeMDPolValues(derPol,xi(:,direction),true))*...
                    GPC.coeffs(:,responseIDs)/GPC.Details.stds(direction);
            else
                responseDerivatives = computeMDPolValues(derPol,xi(:,direction),true)*...
                    GPC.coeffs(:,responseIDs)/GPC.Details.stds(direction);
            end
                
        end
        
        function [responses,r] = sample(GPC,nSamples,varargin)
            % randomly samples from the GPC nSamples
            derivativeFlag = false;
            responseIDs = 1:length(GPC.coeffs(1,:));
            switch (nargin)
                case 2
                    
                case 3
                    responseIDs=varargin{1};
                case {4,5}
                    responseIDs=varargin{1};
                    if (varargin{2})
                        derivativeFlag = true;
                        if (nargin<5)
                            error('openGPC:GPCClass:sample:NoDerivativeDirection',...
                                ['Direction of derivation has to be ',...
                                'entered for sampling derivatives.']);
                        else
                            direction = varargin{3};
                        end
                    end
                otherwise
                    warning('openGPC:GPCClass:sample:IncorrectNumberOfInputs',...
                        'Incorrect number of inputs entered,1,2 or 4 required.');
                    responses = NaN;
                    return
            end
                    
            dirHermite=strcmp(GPC.polType,'hermite');
            dirLegendr=strcmp(GPC.polType,'legendre');
            dirLaguerr=strcmp(GPC.polType,'laguerre');
            
            r=rand(nSamples,length(GPC.polType));
            r=2^0.5*erfinv(2*r-1)*diag(dirHermite)+(2*r-1)*diag(dirLegendr)-...
                log(1-r)*diag(dirLaguerr);
            if (~derivativeFlag)
                responses = computeMDPolValues(GPC.BasisVectors,r,derivativeFlag)*...
                    GPC.coeffs(:,responseIDs);
            else
                responses = GPC.differentiate(r.*(ones(length(r(:,1)),1)*GPC.Details.stds),direction,responseIDs);
            end
        end
        
        function meanValue = mean(GPC,varargin)
            % returns the means of the responses
            if (nargin<2)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            meanValue=GPC.coeffs(1,responseIDs);
        end
        
        function varianceValue = variance(GPC,varargin)
            % returns the variances of the responses
            if (nargin<2)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            varianceValue = GPC.basisNorm(2:end)'*GPC.coeffs(2:end,responseIDs).^2;
        end 
        
        function S = sensitivityMatrix(GPC,x,responseIDs,relativeFlag)
            % returns the sensitivity matrix of the responses, either in
            % absolute or relative terms.
            if (nargin<2)
                % nominal scenario used if no x is given)
                x=zeros(1,length(GPC.basis(1,:)));
                dirLaguerr = find(contains(GPC.Details.polType,'laguerre'));
                x(dirLaguerr) = GPC.Details.stds(dirLaguerr);
                % All responses included
                responseIDs = 1:length(GPC.coeffs(1,:));
                relativeFlag = false;
            elseif (nargin<3)
                responseIDs = 1:length(GPC.coeffs(1,:));
                relativeFlag = false;
            elseif (nargin<4)
                relativeFlag = false;
            end
            referenceResponses = GPC.value(x,responseIDs);
            
            S = zeros(length(GPC.basis(1,:)),length(responseIDs),length(x(:,1)));
            for i = 1:length(GPC.basis(1,:))
                if relativeFlag
                    normMx = (x(:,i)*ones(1,numel(responseIDs)))./referenceResponses;
                else
                    normMx = ones(size(referenceResponses));
                end
                S(i,:,:) = permute(normMx.*GPC.differentiate(x,i,responseIDs),[3 2 1]);
            end
        end
        
        function S = sobolIndices(GPC,maxInteractionOrder,responseIDs)
            % calculates the Sobol indices of the repsonses up to order
            % maxInteractionOrder
            if (nargin<2)
                maxInteractionOrder = 1;
                responseIDs = 1:length(GPC.coeffs(1,:));
            elseif (nargin<3)
                responseIDs = 1:length(GPC.coeffs(1,:));
            end
            
            dim=length(GPC.basis(1,:));
            % Preallocating sensitivity tensor
            interactions=size(combnk(1:dim,min(maxInteractionOrder,round(dim/2))));
            S=zeros(maxInteractionOrder+1,interactions(1,1),length(responseIDs));
            
            % Calculating the Sobol indices of the different interaction levels
            for i=1:maxInteractionOrder
                interactions=combnk(1:dim,i);
                for j=1:size(interactions(:,1))
                    nInteractions=1:dim;
                    nInteractions(interactions(j,:))=[];
                    indexSet=find(all([all(GPC.basis(:,interactions(j,:))>0,2) ...
                        all(GPC.basis(:,nInteractions)==0,2)],2));
                    S(i,j,:)=(GPC.basisNorm(indexSet)'*GPC.coeffs(indexSet,responseIDs).^2)./...
                        GPC.variance(responseIDs);
                end
            end
            
            % Finally the total sensitivity indices are calculated
            for i=1:dim
                indexSet=find(GPC.basis(:,i)>0);
                S(end,i,:)=(GPC.basisNorm(indexSet)'*GPC.coeffs(indexSet,responseIDs).^2)./...
                    GPC.variance(responseIDs);
            end
        end
        
        function interactions=identifySobolIndex(GPC,interactionOrder,index)
            % identifies which Sobol index the given index corresponds to
            interactions = combnk(1:length(GPC.basis(1,:)),interactionOrder);
            interactions = interactions(index,:);
        end  
        
        function responseHistDist = makeResponseHistogramDistribution(GPC,ModelDetails,...
                structureIndidces,nSamples,probabilityLevels,nBins) 
            % draws nSamples of the responses and produces probabilistic 
            % distributions of the response histograms in given spatial 
            % parts of the problem
            switch(nargin)
                case 4
                    probabilityLevels = [95 75 25];
                    nBins = 100;
                case 5
                    nBins = 100;
                case 6
                otherwise
                    error('openGPC:GPCClass:makeResponseHistogramDistribution:InvalidInput',...
                        '4-7 inputs are needed');
            end
            
            responseHistDist=cell(numel(structureIndidces),1);
            for i=1:numel(structureIndidces)
                responseSample = GPC.sample(nSamples,ModelDetails.nominalModelDetails.structureMasks{i}.maskGPCIndices);
                if ~isempty(responseSample)
                    responseSample(responseSample<0)=0;
                    [a,b] = histwc(responseSample,ModelDetails.nominalModelDetails.structureMasks{i}.maskWeights,...
                        nBins);
                    a = 1-((ModelDetails.nominalModelDetails.structureMasks{i}.totalWeight-...
                        sum(a,2)+cumsum(a,2)))./ModelDetails.nominalModelDetails.structureMasks{i}.totalWeight;
                else
                    b = linspace(GPC.Details.cutoffValue,max(GPC.mean),nBins);
                    a = [1 zeros(1,nBins-1)];
                end
                responseHistDist{i,1}.grid = b;
                responseHistDist{i,1}.lowerBound = zeros(numel(probabilityLevels),nBins);
                responseHistDist{i,1}.upperBound = zeros(numel(probabilityLevels),nBins);
                
                colors = jet(length(probabilityLevels));
                figure
                legendText = cell(1,length(probabilityLevels));
                responseHistDist{i,1}.expectedResponse = mean(a,1);
                for j=1:numel(probabilityLevels)
                    responseHistDist{i,1}.lowerBound(j,:) = prctile(a,(100-probabilityLevels(j))/2,1);
                    responseHistDist{i,1}.upperBound(j,:) = prctile(a,100-(100-probabilityLevels(j))/2,1);
                    fill([b b(end:-1:1)],[responseHistDist{i,1}.lowerBound(j,:) ...
                        responseHistDist{i,1}.upperBound(j,end:-1:1)],colors(j,:),...
                        'EdgeColor',colors(j,:));
                    hold on
                    legendText{j} = [num2str(probabilityLevels(j)),'% confidence'];
                end
                plot(b,responseHistDist{i,1}.expectedResponse);
                legendText{j+1} = 'Expected response';
                legend(legendText);
                set(get(gca,'ylabel'),'string','Volume fraction','fontsize',14);
                title(ModelDetails.nominalModelDetails.structureMasks{i}.label);
            end      
        end
        
        function LocalGPC = constructLocalGPC(GlobalGPC,index,stds,means,varargin)
            % constructs local GPC by expanding summative Gaussian
            % parameters
            LocalGPC = GlobalGPC;
            if (nargin<5)
                responseIDs = 1:length(GlobalGPC.coeffs(1,:));
            else
                responseIDs = varargin{1};
                LocalGPC = LocalGPC.set('coeffs',LocalGPC.coeffs(:,responseIDs));
            end
            
            if (length(index)>1)
                warning('openGPC:GPCClass:constructLocalGPC:TooLongIndex',...
                    'Index has to be a single integer. Original GPC returned, no conversion done.');
            elseif (~strcmpi(GlobalGPC.polType{index},'hermite'))
                warning('openGPC:GPCClass:ConstructLocalGPC:NotGaussianVariable',...
                    ['Indicated variable Index=',num2str(index),' is not Gaussian: ',...
                    GlobalGPC.polTpe{index},' polynomial indicated instead of Hermite!',...
                    ' Original GPC returned, no conversion done.']);
            else
                if (sum(stds.^2)~=GlobalGPC.Details.stds(index)^2)
                    warning('openGPC:GPCClass:construcLocalGPC:StandardDeviationsInconsistent',...
                        ['Global GPC variance is ',num2str(GlobalGPC.Details.stds(index)^2),', ',...
                        'local GPC variance is ',num2str(sum(stds.^2)),'. ',...
                        'Conversion is done, but accuracy is not guaranteed.']);
                end
                if (sum(means)~=GlobalGPC.Details.means(index))
                    warning('openGPC:GPCClass:construcLocalGPC:MeansInconsistent',...
                        ['Global GPC mean is ',num2str(GlobalGPC.Details.means(index)),', ',...
                        'local GPC mean is ',num2str(sum(means.^2)),'. ',...
                        'Conversion is done, but accuracy is not guaranteed.']);
                end
                % Insert extra Gaussian variable
                LocalGPC.polType={LocalGPC.polType{1:index},'hermite',...
                    LocalGPC.polType{index+1:end}};
                LocalGPC.Details.polType = LocalGPC.polType;
                % Convert the sigmas into the \sigma_x/\sigma_z and \sigma_y/\sigma_z
                originalStds = stds;
                stds=stds./(sum(stds.^2)^0.5);
                % Determine the maximum order that is needed and build up the
                % a_{k,m} matrix containing the coefficients for the global to local
                % conversion.
                maxOrder=max(GlobalGPC.basis(:,index));
                A=zeros(maxOrder+1,(maxOrder+1)*(maxOrder+2)/2);
                for i=1:maxOrder+1
                    A(i,(i-1)*i/2+1:i*(i+1)/2)=(stds(1).^((i-1)-(0:(i-1)))).*(stds(2).^(0:(i-1))).* ...
                        factorial(i-1)./factorial(0:(i-1))./factorial(i-1-(0:(i-1)));
                end
                % Build up the local GPC_Basis
                lengthLocalGPCBasis=0;
                for i=1:length(GlobalGPC.basis(:,1))
                    lengthLocalGPCBasis=lengthLocalGPCBasis+GlobalGPC.basis(i,index)+1;
                end
                LocalGPC.basis=zeros(lengthLocalGPCBasis,length(GlobalGPC.basis(1,:))+1);
                LocalGPC.coeffs=zeros(lengthLocalGPCBasis,length(GlobalGPC.coeffs(1,responseIDs)));
                j=1;
                for i=1:length(GlobalGPC.basis(:,1))
                    if (GlobalGPC.basis(i,index)>0)
                        newGPCBasisVectors=generateMultiIndex(2,...
                            GlobalGPC.basis(i,index),'highest');
                        LocalGPC.basis(j:j+length(newGPCBasisVectors(:,1))-1,:)=...
                            [ones(length(newGPCBasisVectors(:,1)),1)*...
                            GlobalGPC.basis(i,1:index-1) ...
                            newGPCBasisVectors ...
                            ones(length(newGPCBasisVectors(:,1)),1)*...
                            GlobalGPC.basis(i,index+1:end)];
                        LocalGPC.coeffs(j:j+length(newGPCBasisVectors(:,1))-1,:)=...
                            A(GlobalGPC.basis(i,index)+1,...
                            GlobalGPC.basis(i,index)*...
                            (GlobalGPC.basis(i,index)+1)/2+1:...
                            (GlobalGPC.basis(i,index)+1)*...
                            (GlobalGPC.basis(i,index)+2)/2)'*...
                            GlobalGPC.coeffs(i,responseIDs);
                        j=j+length(newGPCBasisVectors(:,1));
                    else
                        LocalGPC.basis(j,:)=[GlobalGPC.basis(i,1:index) 0 ...
                            GlobalGPC.basis(i,index+1:end)];
                        LocalGPC.coeffs(j,:)=GlobalGPC.coeffs(i,responseIDs);
                        j=j+1;
                    end
                end
                LocalGPC.BasisVectors = MultiDimensionalOrthogonalPolynomClass...
                    (LocalGPC.polType,LocalGPC.basis);
                LocalGPC.basisNorm = computeBasisNorm(LocalGPC.basis,...
                    LocalGPC.polType);
                LocalGPC.Details.variableNames={GlobalGPC.Details.variableNames{1:index-1},...
                    [GlobalGPC.Details.variableNames{index},'-local 1'],...
                    [GlobalGPC.Details.variableNames{index},'-local 2'],...
                    GlobalGPC.Details.variableNames{index+1:end}};
                LocalGPC.Details.means=...
                    [GlobalGPC.Details.means(1:index-1) means ...
                    GlobalGPC.Details.means(index+1:end)];
                LocalGPC.Details.stds=...
                    [GlobalGPC.Details.stds(1:index-1) originalStds ...
                    GlobalGPC.Details.stds(index+1:end)];
                
            end
        end
        
        function ReducedGPC = substitute(GPC,indexSet,x,varargin)
            % constructs a GPC object with decreased dimensionality by
            % substuting a given value for some of the inputs
            if length(indexSet)~=length(x(1,:))
                error('openGPC:GPCClass:substitute:InputInconsistentWithIndexSet',...
                    ['indexSet (',num2str(indexSet),') has to be the same length as the input (',...
                    num2str(x),')']);
            end
            if (nargin<4)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            % Make a multi-dimensional polynomial object array with only
            % the directions that are substituted for and calculate the
            % values of the PC coefficients after substitution
            MDPol = MultiDimensionalOrthogonalPolynomClass(GPC.polType(indexSet),...
                GPC.basis(:,indexSet));
            coeffValues = repmat(GPC.coeffs(:,responseIDs),[1 1 length(x(:,1))]).*...
                repmat(permute(computeMDPolValues(MDPol,x./(ones(length(x(:,1)),1)...
                *GPC.Details.stds(indexSet)),false)',[1 3 2]),...
                [1 length(GPC.coeffs(1,responseIDs)) 1]);
            % Identify the remaining unique basis vectors, sum up the
            % corresponding coefficients and make the new basis
            remainingIndices = true(1,length(GPC.basis(1,:)));
            remainingIndices(indexSet) = false;
            remainingBasis=GPC.basis(:,remainingIndices);
            [reducedBasis,~,ic]=unique(remainingBasis,'rows','legacy');
            reducedCoeffs = zeros(length(reducedBasis(:,1)),...
                length(GPC.coeffs(1,responseIDs)),length(x(:,1)));
            for i=1:length(reducedBasis(:,1))
                reducedCoeffs(i,:,:)=sum(coeffValues(ic==i,:,:),1);
            end
            
            GPCDetails = GPC.Details;
            GPCDetails.means = GPCDetails.means(remainingIndices);
            GPCDetails.stds = GPCDetails.stds(remainingIndices);
            GPCDetails.variableNames = GPCDetails.variableNames(remainingIndices);
            GPCDetails.polType = GPCDetails.polType(remainingIndices);
             
            ReducedGPC{length(x(:,1))}=GPCClass(GPCDetails);
            ReducedGPC{end}.Details = GPCDetails;
            ReducedGPC{end}.polType = GPCDetails.polType;
            ReducedGPC{end} = ReducedGPC{end}.set('basis',reducedBasis);
            ReducedGPC{end} = ReducedGPC{end}.set('BasisVectors',...
                MultiDimensionalOrthogonalPolynomClass(GPCDetails.polType,ReducedGPC{end}.basis));
            ReducedGPC{end} = ReducedGPC{end}.set('basisNorm',...
                computeBasisNorm(reducedBasis,GPCDetails.polType));
            ReducedGPC{end} = ReducedGPC{end}.set('coeffs',...
                reducedCoeffs(:,:,length(x(:,1))));
            for i=1:(length(x(:,1))-1)
                ReducedGPC{i} = GPCClass(GPCDetails);
                ReducedGPC{i}.Details = GPCDetails;
                ReducedGPC{i}.polType = GPCDetails.polType;
                ReducedGPC{i} = ReducedGPC{i}.set('basis',reducedBasis);
                ReducedGPC{i} = ReducedGPC{i}.set('BasisVectors',...
                    MultiDimensionalOrthogonalPolynomClass(GPCDetails.polType,ReducedGPC{i}.basis));
                ReducedGPC{i} = ReducedGPC{i}.set('basisNorm',...
                    computeBasisNorm(reducedBasis,GPCDetails.polType));
                ReducedGPC{i} = ReducedGPC{i}.set('coeffs',reducedCoeffs(:,:,i));
            end
            ReducedGPC = [ReducedGPC{:}];
        end
        
        function lambdaGPC = constructLambdaGPC(GPC,index,varargin)
            % constructs a lambda-GPC by integrating across one of the
            % dimensions to get a GPC of a mean dependent response
            if (length(index)>1)
                error('openGPC:GPCClass:constructLambdaGPC:IncorrectIndex',...
                    'Index has to be a single direction');
            end
            if (nargin<3)
                responseIDs = 1:length(GPC.coeffs(1,:));
            else
                responseIDs=varargin{1};
            end
            remainingIndices = true(1,length(GPC.basis(1,:)));
            remainingIndices(index) = false;
            reducedBasis = GPC.basis(GPC.basis(:,index)==0,remainingIndices);
            if isempty(reducedBasis)
                reducedBasis = 0;
            end
            basisNormForIndex = Calculate_basisNorm(0,GPC.polType(index));
            reducedCoeffs = GPC.coeffs(GPC.basis(:,index)==0,responseIDs)*...
                basisNormForIndex;
            
            GPCDetails = GPC.Details;
            GPCDetails.means = GPCDetails.means(remainingIndices);
            GPCDetails.stds = GPCDetails.stds(remainingIndices);
            GPCDetails.variableNames = GPCDetails.variableNames(remainingIndices);
            GPCDetails.polType = GPCDetails.polType(remainingIndices);
            
            lambdaGPC = GPCClass(GPCDetails);
            lambdaGPC = lambdaGPC.set('basis',reducedBasis);
            lambdaGPC = lambdaGPC.set('BasisVectors',...
                MultiDimensionalOrthogonalPolynomClass(GPCDetails.polType,...
                reducedBasis));
            lambdaGPC = lambdaGPC.set('basisNorm',ComputeBasisNorm(reducedBasis,...
                GPCDetails.polType));
            lambdaGPC = lambdaGPC.set('coeffs',reducedCoeffs); 
            lambdaGPC = lambdaGPC.set('polType',GPC.polType(remainingIndices));
        end
        
    end
end


function Y = computeMDPolValues(MDPol,x,derivativeFlag)

if (derivativeFlag && (length(MDPol(1).type)>1 || size([MDPol(1).multiIndex],2)>1))
    error('openGPC:GPCClass:computeMDPolValues:DerivativeNotObvious',...
        'Derivative value requested for a more than 1 dimensional polynomial.');
end

nScenarios=length(x(:,1));
Y = ones(nScenarios,length(MDPol));
% Identify the different polynomial families
[uPolType,~,uPolTypeIndex]=unique([MDPol(:).type]);
uPolTypeIndex = reshape(uPolTypeIndex,length(MDPol(1).type),length(MDPol(:)))';
% Get the multi-index set for the full basis
polOrders=reshape([MDPol(:).multiIndex],length(MDPol(1).type),[])';
% Loop through the different unique polynomial families
for i=1:length(uPolType)
    % Get the multi-indexes of the basis vectors having a polynomial of
    % this type, and get the unique polynomial orders
    uPolOrders=unique(polOrders(uPolTypeIndex==i));
    % Get the indices of the MDPols with this pol type, as well as the
    % dimensions
    polTypeDims = any(uPolTypeIndex==i,1);
    nPolTypeDims = sum(uPolTypeIndex==i,1);
    polTypeDimsIndex = find(polTypeDims>0);
    % Get the abscissas which are at a dimension with this polynomial type
    abscissas = x(:,polTypeDims);
    
    % Calculate the polynomial values for all abscissas for all unique
    % polynomial orders
    switch(uPolType{i})
        case 'hermite'
            if (~derivativeFlag)
                polynomialValues = herm(abscissas(:),0:max(uPolOrders));
            else
                polynomialValues = hermDer(abscissas(:),0:max(uPolOrders));
            end
        case 'legendre'
            if (~derivativeFlag)
                polynomialValues = legendr(abscissas(:),0:max(uPolOrders));
            else
                polynomialValues = legendrDer(abscissas(:),0:max(uPolOrders));
            end
        case 'laguerre'
            if (~derivativeFlag)
                polynomialValues = laguerre(abscissas(:),0:max(uPolOrders));
            else
                polynomialValues = laguerreDer(abscissas(:),0:max(uPolOrders));
            end
        otherwise
            error('openGPC:MuldiDImensionalOrthogonalPolynomClass:computeMDPolValues',...
                'Invalid polynomial type');
    end
    
    % Finally get the indexes. This looping over the dimensions can
    % probably be circumvented with clever indexing, but for now I do not
    % really see how
    shift=uint32(0);
    for j=1:length(polTypeDimsIndex)
        MDPolsToUpdate=uPolTypeIndex(:,polTypeDimsIndex(j))==i;
        YMult=ones(size(Y));
        YMult(:,MDPolsToUpdate) = reshape(polynomialValues(...
            Kron_Fast(ones(nPolTypeDims(polTypeDimsIndex(j)),1,'uint32'),uint32(transpose(1:nScenarios)))+shift+...
            (Kron_Fast(uint32(polOrders(MDPolsToUpdate,polTypeDimsIndex(j)))+uint32(1),ones(nScenarios,1,'uint32'))-uint32(1))*uint32(size(polynomialValues,1))),...
            [nScenarios nPolTypeDims(polTypeDimsIndex(j))]);
        Y=Y.*YMult;
        shift = shift + uint32(polTypeDims(polTypeDimsIndex(j))*nScenarios);
    end
   
end

end