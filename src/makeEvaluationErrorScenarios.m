function ErrorScenarios = makeEvaluationErrorScenarios(alpha,N,Details,samplingMethod)
% Function makeEvaluationErrorScenarios produces a cell array of sample
% class objects that contain N number of equally position error scenarios
% corresponding to the confidence levels in alpha, representing equally
% probably scenarios for a multivariate Gaussian distribution with
% covariance matrix Sigma
% Inputs:
%       Sigma: The covariance matrix of the multivariate Gaussian for which
%           the error scenarios are wanted
%       alpha: A vector of confidence interval levels, which determine the
%           equiprobable surfaces where the error scenarios will be 
%       N: The number of error scenarios per surface
%       Details: The Details structure of a GPC class object
%       samplingMethod: method for sampling
% Outputs:
%       Error_Scenarios: A cell array of SampleClass objects that contain
%           the abscissas of the points representing the error scenarios

if nargin ==3
    samplingMethod = 'rejection';
end

for i={'variableNames','means','stds','polType','masked'}
    SampleDetails.(i{1}) = Details.(i{1});
end
if isfield(Details,'mask')
    SampleDetails.('mask') = Details.mask;
end

nDim = numel(Details.means);
percentiles = chi2inv(alpha,nDim);
ErrorScenarios=cell(length(percentiles),1);
for i=1:length(percentiles)
    Sigma = diag(Details.stds.^2);
    abscissas = sampleConfidenceEllipsoid(Sigma,percentiles(i),N,samplingMethod);
    ErrorScenarios{i} = SampleClass(abscissas,SampleDetails);
    if Details.masked
        ErrorScenarios{i}.set('nResponses',sum(Details.mask));
    end
end

end


function abscissas = sampleConfidenceEllipsoid(Sigma,percentile,N,samplingMethod)
% First get the percentiles for the chi square distributions
nDim = length(Sigma(1,:));
if ( nDim<=1 )
    error('openGPC:makeEvaluationErrorScenarios:TooLowDimensions',...
        'For 1 dimension (1 error) there are only 2 points per percentile, no need to sample.');
end

% Set up the variables for nDim-1 spherical coordinates
uLimits = [zeros(nDim-1,1) [pi*ones(nDim-2,1); 2*pi]];
% For the percentile we need the semi-axis of the corresponding
% confidence ellipsoid
a = diag((Sigma*percentile).^0.5);
% Get the transformation from the spherical coordinates u to the points
% on the N-dimensional ellipsoid. Since we use the proper semi-axes, we
% need a standard ellipsoid
switch (nDim)
    case 2
        Phi = @(u)([cos(u(1,:)); sin(u(1,:))]./...
            (ones(2,1)*sqrt((cos(u(1,:))/a(1)).^2+(sin(u(1,:))/a(2)).^2)));
    otherwise
        Phi = @(u)([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            sqrt(sum(([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            (a*ones(1,length(u(1,:))))).^2)));
end
abscissas = uniformlySampleSurface(Phi,uLimits,N,samplingMethod);
% We have to store the normalized variables, hence we scale everything
% with the standard deviations
abscissas = abscissas./(ones(N,1)*diag(sqrt(Sigma))');
end