function y=laguerreDer(x,o)
%laguerreDer calculates the value of the derivative of the oth 
% Laguerre polynomial at abscissas x: La_o'(x)
% 1. INPUT:
%   - x: vector of abscissas (N x 1, double)
%   - o: vector of Laguerre polynomial orders (N x 1, integer)
% 2. OUTPUT:
%   - y: matrix with the polynomial derivative values at x for orers in o
% 3. DESCRIPTION:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

if (~iscolumn(x))
    x=x';
end
N=length(x);

switch max(o)
    case 0
        p = zeros(N,1);
    case 1
        p = [zeros(N,1) -ones(N,1)];
    otherwise
        p=zeros(N,max(o)+1);
        p(:,2) = -1;
        for i=3:max(o)+1
            p(:,i)=1/(i-1)*(-1*laguarre(x,i-2)+(2*(i-2)+1-x).*p(:,i-1)-(i-2)*p(:,i-2));
        end
end

y = p(:,o+1);

end