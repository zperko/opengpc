classdef QuadratureClass < handle
    properties (SetAccess = protected, GetAccess = public)
        type      % The type of quadrature (string)
        comment   % Comment on quadrature (string)
        level     % The level of the quadrature (integer)
        abscissas % The abscissas of the quadrature (1 X N, double)
        weights   % The weights of the quadrature (1 X N, double)
    end
    
    %% Constructor
    methods
        function Quadrature = QuadratureClass(quadratureType,quadSetSize)
            if nargin<2 && length(dbstack)==1
                error('openGPC:QuadratureClass:NotEnoughInputArguments',...
                    'Inputs quadraturetype and quadSetSize needed.');
            elseif nargin==2
                Quadrature(quadSetSize(1,1),quadSetSize(1,2)) = ...
                    QuadratureClass(quadratureType);
                for i=1:quadSetSize(1,1)
                    for j=1:quadSetSize(1,2)
                        if (~iscell(quadratureType))
                            Quadrature(i,j).type = {quadratureType};
                        else
                            Quadrature(i,j).type = quadratureType;
                        end
                        Quadrature(i,j).comment = {};
                        Quadrature(i,j).level = [];
                        Quadrature(i,j).weights = [];
                        Quadrature(i,j).abscissas = [];
                    end
                end
            end
        end
        
        %% Property handler functions
        function Quadrature = set(Quadrature,property,value)
            % sets the requested Quadrature property to value
            if isprop(Quadrature,property)
                Quadrature.(property) = value;
                if (strcmp(property,'type')==1) && ~iscell(value)
                    Quadrature.(property) = {value};
                end
            else
                warning('openGPC:QuadratureClass:set:PropertyNotFound',...
                    ['Requested field (',Name,') not found in GPC class. ',...
                    'Properties not changed.']);
            end
        end
        
        function value = get(Quadrature,property)
            % returns the requested Quadrature property
            if isprop(Quadrature,property)
                value=Quadrature.(property);
            else
                warning('openGPC:QuadratureClass:get:PropertyNotFound',...
                    ['Requested property (',property,') not fount in ',...
                    'GPC Class.']);
                value=[];
            end
        end
        
        function Quadrature = loadQuadrature(Quadrature,level)
            % loads the abscissas and weights of the requested quadrature
            if nargin<1
                error('openGPC:QuadratureClass:loadQuadrature:NotEnoughInputs',...
                    'Inputs type and level are needed.');
            elseif ~isnumeric(level)
                error('openGPC:GPCClass:loadQuadrature:NonIntegerLevel',...
                    'Requested level must be positive integer.');
            end
            
            Quadrature.comment = {['L',num2str(level)]};
            Quadrature.level = level;
            switch(lower(Quadrature.type{1}))
                case 'gauss_hermite'
                    [Quadrature.abscissas,Quadrature.weights] = ...
                        computeGaussQuadrature(6,level*2-1,0,1);
                    Quadrature.abscissas = ...
                        Quadrature.abscissas*2^0.5;
                    Quadrature.abscissas(level) = 0;
                    Quadrature.weights=Quadrature.weights/pi^0.5;
                case 'gauss_legendre'
                    [Quadrature.abscissas,Quadrature.weights] = ...
                        computeGaussQuadrature(1,level*2-1,-1,1);
                    Quadrature.abscissas(level) = 0;
                    Quadrature.weights=Quadrature.weights/2;
                case 'gauss_laguerre'
                    [Quadrature.abscissas,Quadrature.weights] = ...
                        computeGaussQuadrature(5,level*2-1,0,1);
                otherwise
                    error(['openGPC:QuadratureClass:loadQuadrature:Unknown ',...
                        'quadrature rule requested.']);
            end
        end
        
        function DifferenceQuadrature = minus(Q1,Q2)
            % returns the difference quadrature Q1-Q2
            if (strcmp(Q1.type{1},Q2.type{1})~=1)
                error('openGPC:QuadratureClass:minus:QuadratureTypeMismatch',...
                    'Quadrature types do not match.')
            elseif (Q1.level<=Q2.level)
                warning(['openGPC:QuadratureClass:minus:LevelError',...
                    'Higher level Quadrature subtracted from lower level one.']);
            end
            DifferenceQuadrature = QuadratureClass(Q1.type{1},[1 1]);
            DifferenceQuadrature.comment = ...
                {['(',Q1.comment{1},')-(',Q2.comment{1},')']};
            DifferenceQuadrature.level = Q1.level;
            R = sortrows(...
                [Q1.abscissas,Q1.weights;Q2.abscissas,-Q2.weights],1);
            R = collapseMatrix(R,1);
            DifferenceQuadrature.abscissas = R(:,1);
            DifferenceQuadrature.weights = R(:,2);
        end
        
        function SumQuadrature = plus(Q1,Q2)
            % returns the sum quadrature Q1+Q2
            if (strcmp(Q1.type{1},Q2.type{1})~=1)
                error(['openGPC:QuadratureClass:plus:QuadratureTypeMismatch',...
                    'Quadrature types do not match.']);
            end
            SumQuadrature = QuadratureClass(Q1.type{1},[1 1]);
            SumQuadrature.comment = ...
                {['(',Q1.comment{1},')+(',Q2.comment{1},')']};
            SumQuadrature.level = max(Q1.level,Q2.level);
            R = [Q1.abscissas, Q1.weights; Q2.abscissas, Q2.weights];
            R = collapseMatrix(sortrows(R,1),1);
            R(R(:,2)==0,:)=[];
            SumQuadrature.abscissas = R(:,1);
            SumQuadrature.weights = R(:,2);
        end
        
        function [QuadratureSet] = loadQuadratureSet(QuadratureSet,maxLevel)
            % loads all quadratures till maxLevel
            
            for j=1:maxLevel
                QuadratureSet(j,1) = loadQuadrature(QuadratureSet(j,1),j);
            end
        end
        
        function [QuadratureSet] = makeDifferenceQuadratureSet(...
                QuadratureSet,nExtraLevel)
            % returns the set of difference quadratures
            maxLevel = QuadratureSet(end,1).level;
            QuadratureSet(end,1) = loadQuadrature(...
                QuadratureSet(end,1),maxLevel+nExtraLevel);
            for j=maxLevel:-1:2
                QuadratureSet(j,1) = QuadratureSet(j,1) - QuadratureSet(j-1,1);
            end
        end
        
        function integral = integrate(Quadrature,responses)
            % performs the numerical integration using the given quadrature
            % weights and responses
            if length(Quadrature.weights)~=size(responses,1)
                error('openGPC:QuadratureClass:integrate:InconsistentSize',...
                    'Inconsistent quadrature weight and responses size.');
            elseif (size(Quadrature.weights,1)>size(Quadrature.weights,2))
                integral = Quadrature.weights'* responses;
            else
                integral = Quadrature.weights * responses;
            end
        end
        
    end
    
end