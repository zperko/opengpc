function y = herm(x,o)
%herm calculates the value of the oth Hermite polynomial at abscissas x:
% He_o(x)
% 1. INPUT:
%   - x: vector of abscissas (N x 1, double)
%   - o: vector of Hermite polynomial orders (N x 1, integer)
% 2. OUTPUT:
%   - y: matrix with the polynomial values at x for orders in o
% 3. Description:

%% COPYRIGHT:
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

N=length(x);

switch (max(o))
    case 0
        p=ones(N,1);
    case 1
        p=ones(N,2);
        p(:,2)=x;
    otherwise
        p=zeros(N,max(o)+1);
        p(:,1)=ones(N,1);
        p(:,2)=x;
        for i=3:max(o)+1
            p(:,i)=p(:,2).*p(:,i-1)-(i-2)*p(:,i-2);
        end
end

y = p(:,o+1);

end