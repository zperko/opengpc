function trimmedMI=hyperbolicTrimMI(MI,maxNorm,normType)
%hyperbolicTrimMI performs a trimming on the multi-index set based on
% the q-norm of the multi-indexes
% 1. INPUT:
%   - MI: input multi-index set
%   - maxNorm: the maximum allowed q-norm of the multi-indexes
%   - normType: the norm type, (0,1]
% 2. OUTPUT:
%   - MI_trimmed: the trimmed multi-index set satisfying Norm_q(MI)<+p
% 3. DESCRIPTION:

%% COPYRIGHT
% Zoltan Perko, TU Delft
% Date: 2018.11.15.

%% FUNCTION DEFINITION

trimmedMI=zeros(size(MI));
j=0;
for i=1:length(MI(:,1))
    if (sum(MI(i,:).^normType)^(1/normType)<=maxNorm)
        j=j+1;
        trimmedMI(j,:)=MI(i,:);
    elseif (sum(MI(i,:)>0)==1)
        % 1D polynomials have to be treated separately, as due to numerical
        % errors they might be cut accidentally
        if max(MI(i,:))<=maxNorm
            j=j+1;
            trimmedMI(j,:)=MI(i,:);
        end
    end
end
trimmedMI=trimmedMI(1:j,:);