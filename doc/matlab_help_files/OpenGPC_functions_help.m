%% OpenGPC_functions_help 
% OpenGPC function list
%
%% Functions
%
% <html>
% <p>Quadrature functions<a name="1"></a></p>
% </html>
%
% * <matlab:doc('gaussHermiteQuadrature') gaussHermiteQuadrature>: function
%   to calculate the abscissas and weights of an nPoints point
%   Gauss-Hermite quadrature
% * <matlab:doc('gaussLegendreQuadrature') gaussLegendreQuadrature>:
%   function to calculate the abscissas and weights of an nPoints point
%   Gauss-Legendre quadrature
% * <matlab:doc('gaussLaguerreQuadrature') gaussLaguerreQuadrature>:
%   function to calculate the abscissas and weights of an nPoints point
%   Gauss-Laguerre quadrature
%
% <html>
% <a href="#2">Test_Multiply:</a> Multiply two numbers
% </html>
%
% <html>
% <p>Second category<a name="2"></a></p>
% </html>
%
