%% OpenGPC Toolbox
% OpenGPC is an open source Generalized Polynomial Chaos Expansion toolbox
% for sensitivity analysis, uncertainty quantification, meta-modelling,
% optimization and other related purposes.
%
%% Syntax
%   GPC = OpenGPC(GPC_Settings) 
%   [GPC, IntegrationData] = OpenGPC(GPC_Settings) 
%
%% Description
% 
% |GPC = OpenGPC(GPC_Settings)| builds the Generalized Polynomial Chaos 
%   Expansion (GPC) for the specified model with the given settings and 
%   returns a GPC object containing the meta-model of the original system.
%
% |[GPC, IntegrationData] = OpenGPC(GPC_Settings)| also returns the raw
%   data used to construct the GPC meta-model.
%
%% Examples
%   
%   GPC_Settings = load(ExampleSettings.m);
%   GPC = OpenGPC(GPC_Settings);
