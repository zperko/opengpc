function generateDocumentation
% Documentation generator script for openGPC toolbox.

currentDirectory = pwd;

if (currentDirectory(end)==filesep)
    currentDirectory(end)='';
end

% Get the path to the root of the toolbox and the documentation folders
[toolboxRoot,~,~] = fileparts(currentDirectory);
htmlDirectory=[currentDirectory,filesep,'html'];
helpDirectory=[currentDirectory,filesep,'matlab_help_files'];

Options = struct('format','html','outputDir',htmlDirectory);

% publish help files to create htmls
documentFiles = dir([helpDirectory,filesep,'*_help.m']);
cd(helpDirectory);
for iFile = 1:numel(documentFiles)
    publish(documentFiles(iFile).name,Options);
end

% Build search database
toolboxPath = genpath(toolboxRoot);
addpath(toolboxPath);
cd(htmlDirectory);
builddocsearchdb(pwd);
rmpath(toolboxPath);

% Return to original directory
cd(currentDirectory);

end