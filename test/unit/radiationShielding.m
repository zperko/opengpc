function response = radiationShielding(input)
%radiationShielding is a simple one dimensional radiation shielding test
%problem
% INPUT:
%   - input: realization of the random physical properties (nSamples x
%   nInputs, double)
% OUTPUT:
%   - response: the value of the radiation after the shielding.
% TODO:
%   - Include description of the exact model
S=1*(1+input(:,1));
D1=.2*(1+input(:,2));
D2=.9*(1+input(:,3));
S1=.008*(1+input(:,4));
S2=.01*(1+input(:,5));
a=10*(1+input(:,6));
b=20*(1+input(:,7));
L1=(D1/S1)^.5*(1+input(:,8));
L2=(D2/S2)^.5*(1+input(:,9));

A2=(S.*L1.*L2./(2)).*(exp(a./L2))./(D2.*L1.*cosh(a./L1)+D1.*L2.*sinh(a./L1));
B2=0;
phi=A2.*exp(-b./L2)+B2.*exp(b./L1);
response = phi;