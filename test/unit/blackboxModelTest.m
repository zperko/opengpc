classdef blackboxModelTest < matlab.unittest.TestCase
    %blackboxModelTest runs through a number of analytical testcases
    %   Testcases include Rosenbrock function in 2D (1), Ishigami function
    %   in 3D (2), Sobol function in 8D (3), Continuous Genz function f_5
    %   in 10D (4), Morris function in 20D (5), analytical test function
    %   with noisy response (6), radiation shielding problem in 9D (7), a
    %   diffusion criticality problem (8), 1 effective group point kinetics
    %   problem in 6D (9), 2 group diffusion in slab with heat conducion
    %   and temperature feedback in 15D (10)
    
    properties (ClassSetupParameter)
        modelCase = {1,2,3,4,5,6,7,8,9,10};
        nInputs = {2,3,8,10,20,3,9,8,6,15};
        means = {zeros(1,2),zeros(1,3),0.5*ones(1,8),...
            0.5*ones(1,10),0.5*ones(1,20),zeros(1,3),...
            zeros(1,9),...
            zeros(1,8),...
            [1,0.094410382098,0.478e-6,0.00403,0.003646190457,6e-3],...
            zeros(1,15)};
        stds = {[1,1],[pi,pi,pi],0.5*ones(1,8),...
            0.5*ones(1,10),0.5*ones(1,20),ones(1,3),...
            [0.1,0.01,0.05,0.2,0.01,0.05,0.05,0.05,0.1],...
            [0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05],...
            0.05*[1,0.094410382098,0.478e-6,0.00403,0.003646190457,6e-3],...
            [0.05 0.05 0.05 0.05 0.05 0.02 0.02 0.02 0.05 0.05 0.05 0.05 0.1 0.1 0.05]};
        nResponses = {3,2,2,2,2,4,2,2,2,13};
        normType = {1,1,1,1,1,1,1,1,1,1};
        externalModel = {0,0,0,0,0,0,0,0,0,0};
        trim = {0,0,0,0,0,0,0,0,0,0};
    end
    
    properties (MethodSetupParameter)
        maxPolOrder = {2,3,4,5};%,6};
        maxGridLevel = {2,3,4,5};%,6};
    end
    
    properties (TestParameter)
        polType = {'hermite','legendre'};
        nExtraLevels = {0,1};
        cutoffValue = {0,1e-3};
        parallelMode = {'serial','matlabParallel','modelParallel'};
    end
    
    properties
        GPCSettings
        ModelDetails
    end
    
    %% Class setup with different distributions, extended levels, cutoffs
    % and parallelization modes
    methods (TestClassSetup, ParameterCombination = 'sequential')
        function ClassSetup(testCase,modelCase,nInputs,means,stds,nResponses,...
                normType,externalModel,trim)
            % Set GPCSettings
            [testCase.GPCSettings.variableNames{1:nInputs}] = deal('Variable');
            testCase.GPCSettings.means = means;
            testCase.GPCSettings.stds  = stds;
            testCase.GPCSettings.normType = normType;
            testCase.GPCSettings.trim = trim;
            % Set ModelDetails
            testCase.ModelDetails.nInputs = nInputs;
            testCase.ModelDetails.nResponses = nResponses;
            testCase.ModelDetails.modelCase = modelCase;
            testCase.ModelDetails.externalModel = externalModel;
            
            % Path settings
            originalPath = path;
            testCase.addTeardown(@path,originalPath);
            currentDirectory = pwd;
            addpath(genpath(currentDirectory));
        end
    end
    
    %% Method setup for different polynomial orders and grid levels
    methods (TestMethodSetup, ParameterCombination = 'sequential')
        function MethodSetup(testCase,maxPolOrder,maxGridLevel)
            % Set GPC settings
            testCase.GPCSettings.maxPolOrder = maxPolOrder;
            testCase.GPCSettings.maxGridLevel = maxGridLevel;
            
        end
    end
    
    methods (Test, ParameterCombination = 'exhaustive',TestTags = {'Coupling'})
        function testCouplingMethods(testCase,polType,nExtraLevels,cutoffValue,parallelMode)
            % Set GPCSettings
            [testCase.GPCSettings.polType{1:testCase.ModelDetails.nInputs}] = deal(polType);
            testCase.GPCSettings.cutoffValue = cutoffValue;
            testCase.GPCSettings.nExtraLevels = nExtraLevels;
            % Set ModelDetails
            testCase.ModelDetails.parallelMode = parallelMode;
            testCase.ModelDetails.externalRetryInterval = 15;
            testCase.ModelDetails.statusUpdateInterval = 60;
            if cutoffValue > 0
                testCase.ModelDetails.nResponses = testCase.ModelDetails.nResponses-1;
            end
            
            % Build GPC
            [GPC,IntegrationData] = openGPC(testCase.GPCSettings,testCase.ModelDetails);
            testCase.verifyEqual(length(GPC.coeffs(1,:)),testCase.ModelDetails.nResponses);
            testCase.verifyEqual(IntegrationData.ErrorScenarios.All.nResponses,...
                testCase.ModelDetails.nResponses);
        end
    end
    
end

