function [Phi,T,T_avg,B1,B2,Pow,k_eff,eta,p,epsilon,P_NL_thermal,P_NL_fast] = ...
    calculateSteadyStateSlab(Sigma_t,Sigma_f,Sigma_tr,khi,D,nu,Q,k,a,T_ref)
% TODO: documentation

[~,Sigma_2,Sigma_12,Sigma_21] = ...
    Get_Sigma(Sigma_t,Sigma_f,Sigma_tr,khi,nu,T_ref,T_ref,1,0);
T_avg=T_ref+1.0/Sigma_t(1,2)*...
    ((Sigma_12*Sigma_21-D(1)*(pi/(2*a))^4*D(2)-D(1)*(pi/(2*a))^2*Sigma_2)/((pi/(2*a))^2*D(2)+Sigma_2)-...
     Sigma_t(1,1)+khi(1)*nu(1)*Sigma_f(1)+Sigma_tr(1,1));
[Sigma_1,Sigma_2,Sigma_12,Sigma_21] = ...
    Get_Sigma(Sigma_t,Sigma_f,Sigma_tr,khi,nu,T_avg,T_ref,1,0);
[B1,B2] = Calculate_buckling(Sigma_1,Sigma_2,Sigma_12,Sigma_21,D);
Phi(1) = T_avg*B1^(3/2)*k*a/Q/(Sigma_f(1)+Sigma_f(2)*Sigma_12/(D(2)*B1+Sigma_2));
Phi(2) = T_avg*B1^(3/2)*k*a/Q/(Sigma_f(2)+Sigma_f(1)*(D(2)*B1+Sigma_2)/Sigma_12);
T=T_avg*B1^0.5*a;
Pow=2*T_avg*B1*k*a;
eta=nu(2)*Sigma_f(2)/(Sigma_t(2,1)-Sigma_tr(2,2));
P_NL_thermal=(Sigma_t(2,1)-Sigma_tr(2,2))/(Sigma_t(2,1)-Sigma_tr(2,2)+D(2)*B1);
p=Sigma_tr(1,2)/(Sigma_t(1,1)+Sigma_t(1,2)*(T_avg-T_ref)-Sigma_tr(1,1));
P_NL_fast = (Sigma_t(1,1)+Sigma_t(1,2)*(T_avg-T_ref)-Sigma_tr(1,1))/...
    (Sigma_t(1,1)+Sigma_t(1,2)*(T_avg-T_ref)-Sigma_tr(1,1)+D(1)*B1);
epsilon = 1+(Sigma_t(2,1)-Sigma_tr(2,2)+D(2)*B1)*nu(1)*Sigma_f(1)...
    /nu(2)/Sigma_f(2)/Sigma_tr(1,2);
k_eff = eta*p*epsilon*P_NL_fast*P_NL_thermal;

end

function [Sigma_1,Sigma_2,Sigma_12,Sigma_21]=Get_Sigma(Sigma_t,Sigma_f,Sigma_tr,khi,nu,T,T_ref,k_eff,l)

Sigma_1=Sigma_t(1,1)+Sigma_t(1,2)*(T-T_ref)-Sigma_tr(1,1)-(1-l)/k_eff*khi(1)*nu(1)*Sigma_f(1);
Sigma_2=Sigma_t(2,1)+Sigma_t(2,2)*(T-T_ref)-Sigma_tr(2,2)-(1-l)/k_eff*khi(2)*nu(2)*Sigma_f(2);
Sigma_12=Sigma_tr(1,2)+(1-l)/k_eff*khi(2)*nu(1)*Sigma_f(1);
Sigma_21=Sigma_tr(2,1)+(1-l)/k_eff*khi(1)*nu(2)*Sigma_f(2);
end

function [B1,B2]=Calculate_buckling(Sigma_1,Sigma_2,Sigma_12,Sigma_21,D)

B1=(-(D(1)*Sigma_2+D(2)*Sigma_1)+((D(1)*Sigma_2-D(2)*Sigma_1)^2+4*D(1)*D(2)*Sigma_12*Sigma_21)^0.5)...
    /(2*D(1)*D(2));
B2=(-(D(1)*Sigma_2+D(2)*Sigma_1)-((D(1)*Sigma_2-D(2)*Sigma_1)^2+4*D(1)*D(2)*Sigma_12*Sigma_21)^0.5)...
    /(2*D(1)*D(2));
end