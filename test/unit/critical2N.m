function [keff, phi1, phi2]=critical2N(input)
%critical2N is a 2/3 dimensional neutron diffusion problem
% USE
% INPUT:
%   - input: realization of the random physical properties (nSamples x
%   nInputs, double)
% OUTPUT:
%   - keff: The effective multiplication factor
%   - phi1: Neutron flux in group 1
%   - phi2: Neutron flux in group 2
% DESCRIPTION:
% TODO:
%   - clean up and check

L1=60;
L2=120;
L3=60;

%Stacey-Favorite LWR
%diff l(cm)
D11=1.695497;
D12=1.695497;
D13=1.695497;
D21=0.4097145;
D22=0.4097145;
D23=0.4097145;
%rem, scatt, fiss xsect (cm)
ss1=0.01641351;
ss2=0.01641351;
ss3=0.01641351;

nusf11=0.01949058*(1+input(3));
nusf12=0.01949058*(1+input(1));
nusf13=0.01949058*(1+input(3));
nusf21=0.497857*(1+input(4));
nusf22=0.497857*(1+input(2));
nusf23=0.497857*(1+input(4));

B21=0.01090;
B22=0.01200;
B23=0.01090;

sc11=0.01375321*(1+input(7));
sc12=0.01375321*(1+input(5));
sc13=0.01375321*(1+input(7));
sc21=0.2613659*(1+input(8));
sc22=0.2613659*(1+input(6));
sc23=0.2613659*(1+input(8));

sr11=sc11+D11*B21+ss1;
sr12=sc12+D12*B22+ss2;
sr13=sc13+D13*B23+ss3;
sr21=sc21+D21*B21;
sr22=sc22+D22*B22;
sr23=sc23+D23*B23;

D(1,1)=D11;
D(1,2)=D12;
D(1,3)=D13;
D(2,1)=D21;
D(2,2)=D22;
D(2,3)=D23;
sr(1,1)=sr11;
sr(1,2)=sr12;
sr(1,3)=sr13;
sr(2,1)=sr21;
sr(2,2)=sr22;
sr(2,3)=sr23;
sc(1,1)=sc11;
sc(1,2)=sc12;
sc(1,3)=sc13;
sc(2,1)=sc21;
sc(2,2)=sc22;
sc(2,3)=sc23;
ss(1)=ss1;
ss(2)=ss2;
ss(3)=ss3;
nusf(1,1)=nusf11;
nusf(1,2)=nusf12;
nusf(1,3)=nusf13;
nusf(2,1)=nusf21;
nusf(2,2)=nusf22;
nusf(2,3)=nusf23;


dx=1;

x1=dx:dx:L1-dx;
x2=dx:dx:L2-dx;
x3=dx:dx:L3-dx;
x=[x1,x2+x1(length(x1))+dx,x3+x2(length(x2))+x1(length(x1))+2*dx];
l1=length(x1);
l2=length(x2);
l3=length(x3);
I1=eye(l1);
I2=eye(l2);
I3=eye(l3);
Z11=zeros(l1,l1);
Z12=zeros(l1,l2);
Z13=zeros(l1,l3);
Z21=zeros(l2,l1);
Z22=zeros(l2,l2);
Z23=zeros(l2,l3);
Z31=zeros(l3,l1);
Z32=zeros(l3,l2);
Z33=zeros(l3,l3);
D11=D(1,1);
D12=D(1,2);
D13=D(1,3);
D21=D(2,1);
D22=D(2,2);
D23=D(2,3);
sr11=sr(1,1);
sr12=sr(1,2);
sr13=sr(1,3);
sr21=sr(2,1);
sr22=sr(2,2);
sr23=sr(2,3);
ss1=ss(1);
ss2=ss(2);
ss3=ss(3);
nusf11=nusf(1,1);
nusf12=nusf(1,2);
nusf13=nusf(1,3);
nusf21=nusf(2,1);
nusf22=nusf(2,2);
nusf23=nusf(2,3);

%%%%%%%%%%%%%%%%

A11=-diag(2*D11/dx^2*ones(l1,1),0)+diag(D11/dx^2*ones(l1-1,1),-1)+diag(D11/dx^2*ones(l1-1,1),1);
A11=A11-sr11*I1;
A12=-diag(2*D12/dx^2*ones(l2,1),0)+diag(D12/dx^2*ones(l2-1,1),-1)+diag(D12/dx^2*ones(l2-1,1),1);
A12=A12-sr12*I2;
A13=-diag(2*D13/dx^2*ones(l3,1),0)+diag(D13/dx^2*ones(l3-1,1),-1)+diag(D13/dx^2*ones(l3-1,1),1);
A13=A13-sr13*I3;

a1=D11/(D11+D12);
b1=D12/(D11+D12);
c1=D12/(D12+D13);
d1=D13/(D12+D13);

%first bound
A11(l1,l1)=A11(l1,l1)+D11*a1/dx^2;
BC1_12=Z12*0;
BC1_12(l1,1)=D11*b1/dx^2;

A12(1,1)=A12(1,1)+D12*b1/dx^2;
BC1_21=Z21*0;
BC1_21(1,l1)=D12*a1/dx^2;
%second bound

A12(l2,l2)=A12(l2,l2)+D12*c1/dx^2;
BC1_23=Z23*0;
BC1_23(l2,1)=D12*d1/dx^2;

A13(1,1)=A13(1,1)+D13*d1/dx^2;
BC1_32=Z32*0;
BC1_32(1,l2)=D13*c1/dx^2;


A21=-diag(2*D21/dx^2*ones(l1,1),0)+diag(D21/dx^2*ones(l1-1,1),-1)+diag(D21/dx^2*ones(l1-1,1),1);
A21=A21-sr21*I1;
A22=-diag(2*D22/dx^2*ones(l2,1),0)+diag(D22/dx^2*ones(l2-1,1),-1)+diag(D22/dx^2*ones(l2-1,1),1);
A22=A22-sr22*I2;
A23=-diag(2*D23/dx^2*ones(l3,1),0)+diag(D23/dx^2*ones(l3-1,1),-1)+diag(D23/dx^2*ones(l3-1,1),1);
A23=A23-sr23*I3;

B21s=ss1*I1;
B22s=ss2*I2;
B23s=ss3*I3;

a2=D21/(D21+D22);
b2=D22/(D21+D22);
c2=D22/(D22+D23);
d2=D23/(D22+D23);

%first bound
A21(l1,l1)=A21(l1,l1)+D21*a2/dx^2;
BC2_12=Z12*0;
BC2_12(l1,1)=D21*b2/dx^2;

A22(1,1)=A22(1,1)+D22*b2/dx^2;
BC2_21=Z21*0;
BC2_21(1,l1)=D22*a2/dx^2;
%second bound

A22(l2,l2)=A22(l2,l2)+D22*c2/dx^2;
BC2_23=Z23*0;
BC2_23(l2,1)=D22*d2/dx^2;

A23(1,1)=A23(1,1)+D23*d2/dx^2;
BC2_32=Z32*0;
BC2_32(1,l2)=D23*c2/dx^2;



L1=-[A11,BC1_12,Z13;BC1_21,A12,BC1_23;Z31,BC1_32,A13];
L2=-[A21,BC2_12,Z13;BC2_21,A22,BC2_23;Z31,BC2_32,A23];
DS=[B21s,Z12,Z13;Z21,B22s,Z23;Z31,Z32,B23s];
F1=[nusf11*I1,Z12,Z13;Z21,nusf12*I2,Z23;Z31,Z32,nusf13*I3];
F2=[nusf21*I1,Z12,Z13;Z21,nusf22*I2,Z23;Z31,Z32,nusf23*I3];

[keff, phi1, phi2]=critic_coupled(1e-8,x,L1,L2,F1,F2,DS);
keff=1e5*(keff-1)/keff;
Norm=trapz(x,phi1)/x(length(x));
phi1=(1/Norm)*phi1;
phi2=(1/Norm)*phi2;

end

function [keff, phi1, phi2]=critic_coupled(tol,x,L1,L2,F1,F2,DS)
eps=1;
S=zeros(1,length(L1))+1;
p1=S*0;
p2=p1*0;
k=1.99;

% Power iteration probably
while abs(eps)>tol
   p1=L1\(S'/k);
   p2=L2\(DS*p1);
   Sn=F1*p1+F2*p2;
   kn=trapz(x,Sn)/((1/k)*trapz(x,S));
   eps=(kn-k)/k;
   k=kn;
   S=Sn';
end
keff=k;
phi1=p1;
phi2=p2;

end