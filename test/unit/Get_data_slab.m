function [Sigma_t,Sigma_f,Sigma_tr,khi,D,nu,Q,k,Pow,a,gamma_I,gamma_Xe,lambda_I,lambda_Xe,sigma_Xe,T_ref]=...
         Get_data_slab(Perturbation)

%Neutronic parameters
Sigma_t =  [0.0400 0.000010;
            0.1390 0.00000];
Sigma_f =  [0.0050*(Perturbation/100+1);
            0.0500];
Sigma_tr = [0.0075 0.0200;
            0.0000 0.0150];
khi =[1.0 0.0]; %0.9 0.1
D = [1.25 0.5];
nu=[3 2.5];
T_ref=450;

%Thermal-hydraulic parameters
Q=200e6*1.602e-19;  %J
Pow=100;            %W
k=1;                %Wcm^-1K^-1
a=50;               %cm

%FP parameters
gamma_I=0.06386;
gamma_Xe=0.00228;
lambda_I=2.09e-5;     %s^-1
lambda_Xe=2.88e-5;    %s^-1
sigma_Xe(1)=2.275e-18;  %cm^2 increased 10 times compared to original
sigma_Xe(2)=2.0475e-17; %cm^2 increased 10 times compared to original