samplingMethod = 'slice'; %rejection
% Analytical function
uLimits = [0 10];
Phi = @(t)([t;-10*exp(-100*(t-4.25).^2)+sin(6*t)-2*sin(t)+2;zeros(1,length(t))]);
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
figure
t=0:0.01:10;
plot(t,-10*exp(-100*(t-4.25).^2)+sin(6*t)-2*sin(t)+2);
hold on
scatter(Points(:,1),Points(:,2));

% Analytical function in 2D
uLimits = [0 10; -5 5];
Phi = @(t)([t(1,:);(-10*exp(-100*(t(1,:)-4.25).^2)+sin(6*t(1,:))-2*sin(t(1,:))+2).*t(2,:);t(2,:)]);
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
t=linspace(0,10,1e3);
z=linspace(-5,5,100);
[tGrid,zGrid]=meshgrid(t,z);
yGrid = (-10*exp(-100*(tGrid-4.25).^2)+sin(6*tGrid)-2*sin(tGrid)+2).*zGrid;
figure
surf(tGrid,zGrid,yGrid)
hold on
scatter3(Points(:,1),Points(:,3),Points(:,2));

% Viviani curve
uLimits = [0 4*pi];
Phi = @(t)([1+cos(t);sin(t);2*sin(t/2)]);
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
figure
t=uLimits(1):0.01:uLimits(2);
plot3(1+cos(t),sin(t),2*sin(t/2));
hold on
scatter3(Points(:,1),Points(:,2),Points(:,3));

% Torus
uLimits = [0 2*pi; 0 2*pi];
Phi = @(u)([(3+cos(u(1,:))).*cos(u(2,:)); (3+cos(u(1,:))).*sin(u(2,:)); sin(u(1,:))]);
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
u = linspace(uLimits(1,1),uLimits(1,2),1e3);
v = linspace(uLimits(2,1),uLimits(2,2),1e3);
[uGrid,vGrid] = meshgrid(u,v);
figure
surf((3+cos(uGrid)).*cos(vGrid),(3+cos(uGrid)).*sin(vGrid),sin(uGrid));
hold on
scatter3(Points(:,1),Points(:,2),Points(:,3));

% Klein bottle
uLimits = [-pi/2 pi/2;0 2*pi];
Phi = @(u)([-2/15*cos(u(1,:)).*(3*cos(u(2,:))+5*sin(u(1,:)).*cos(u(2,:)).*cos(u(1,:))-...
    30*sin(u(1,:))-60*sin(u(1,:)).*cos(u(2,:)).^6+90*sin(u(1,:)).*cos(u(2,:)).^4);...
    -1/15*sin(u(1,:)).*(80*cos(u(2,:)).*cos(u(1,:)).^7.*sin(u(1,:))+48*cos(u(2,:)).*cos(u(1,:)).^6-...
    80*cos(u(2,:)).*cos(u(1,:)).^5.*sin(u(1,:))-48*cos(u(2,:)).*cos(u(1,:)).^4-...
    5*cos(u(2,:)).*cos(u(1,:)).^3.*sin(u(1,:))-3*cos(u(2,:)).*cos(u(1,:)).^2+5*sin(u(1,:)).*cos(u(2,:)).*cos(u(1,:))+...
    3*cos(u(2,:))-60*sin(u(1,:)));...
    2/15*sin(u(2,:)).*(3+5*sin(u(1,:)).*cos(u(1,:)))]);
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
u = linspace(uLimits(1,1),uLimits(1,2),1e3);
v = linspace(uLimits(2,1),uLimits(2,2),1e3);
[uGrid,vGrid] = meshgrid(u,v);
figure
PointsRef = Phi([uGrid(:)';vGrid(:)'])';
surf(reshape(PointsRef(:,1),size(uGrid)),reshape(PointsRef(:,2),size(vGrid)),...
    reshape(PointsRef(:,3),size(uGrid)));
hold on
scatter3(Points(:,1),Points(:,2),Points(:,3));

% Sphere
uLimits = [0 pi; 0 2*pi];
a = [1;1;1];
Phi = @(u)([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            sqrt(sum(([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            (a*ones(1,length(u(1,:))))).^2)));
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
u = linspace(uLimits(1,1),uLimits(1,2),1e3);
v = linspace(uLimits(2,1),uLimits(2,2),1e3);
[uGrid,vGrid] = meshgrid(u,v);
figure
PointsRef = Phi([uGrid(:)';vGrid(:)'])';
surf(reshape(PointsRef(:,1),size(uGrid)),reshape(PointsRef(:,2),size(vGrid)),...
    reshape(PointsRef(:,3),size(uGrid)));
hold on
scatter3(Points(:,1),Points(:,2),Points(:,3));

% Ellipsoid
uLimits = [0 pi; 0 2*pi];
a = [1;1;10];
Phi = @(u)([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            sqrt(sum(([reshape(prod(reshape(sin(u),[length(u(:,1)) 1 length(u(1,:))]).^triu(...
            ones(length(u(:,1))),1),1),[length(u(1,:)) length(u(:,1)) 1])'.*cos(u); prod(sin(u),1)]./...
            (a*ones(1,length(u(1,:))))).^2)));
Points = uniformlySampleSurface(Phi,uLimits,1000,samplingMethod);
u = linspace(uLimits(1,1),uLimits(1,2),1e3);
v = linspace(uLimits(2,1),uLimits(2,2),1e3);
[uGrid,vGrid] = meshgrid(u,v);
figure
PointsRef = Phi([uGrid(:)';vGrid(:)'])';
surf(reshape(PointsRef(:,1),size(uGrid)),reshape(PointsRef(:,2),size(vGrid)),...
    reshape(PointsRef(:,3),size(uGrid)));
hold on
scatter3(Points(:,1),Points(:,2),Points(:,3));

